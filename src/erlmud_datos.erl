%%%-------------------------------------------------------------------
%%% @author Notxor <notxor@nueva-actitud.org>
%%% @copyright (C) 2020, Notxor
%%% @doc
%%% Gestor de datos de la Aplicación
%%% @end
%%% Created :  6 Jul 2020 by Notxor <notxor@nueva-actitud.org>
%%%-------------------------------------------------------------------
-module(erlmud_datos).

-behaviour(gen_server).

%% API
-export([start_link/0]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3, format_status/2]).

-define(SERVER, ?MODULE).

%% Valor para el servidor de datos [parado |ocupado | listo | esperando]
-record(state, {estado=listo, contador_entidades=0}).

%% %% Definición de las estructuras de datos del programa
%% -include("include/registros.hrl").  % En teoría están incluidos en «data_mundi.hrl»

%% Definición de los datos del «mundo» para las pruebas.
-include("include/data_mundi.hrl").

%% Definición de sinónimos de acciones.
-include("include/sinonimos.hrl").

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Levanta el servidor de datos.
%% @end
%%--------------------------------------------------------------------
%% -spec start_link() -> {ok, Pid :: pid()} |
%%           {error, Error :: {already_started, pid()}} |
%%           {error, Error :: term()} |
%%           ignore.
start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Inicia el servidor iniciando los datos que gestionará.
%% @end
%%--------------------------------------------------------------------
%% -spec init(Args :: term()) -> {ok, State :: term()} |
%%           {ok, State :: term(), Timeout :: timeout()} |
%%           {ok, State :: term(), hibernate} |
%%           {stop, Reason :: term()} |
%%           ignore.
init([]) ->
    process_flag(trap_exit, true),
    %% Crear las tablas de datos «entidades» y «jugadores»
    ets:new(entidades, [set, named_table, protected,
                        {keypos, #entidad.uuid},
                        {heir, self(), "Datos Entidades"}]),
    ets:new(pjs,       [set, named_table, protected,
                        {keypos, #pjs.uuid},
                        {heir, self(), "Datos Jugadores"}]),
    ets:new(objetos,   [set, named_table, protected,
                        {keypos, #obj.uuid},
                        {heir, self(), "Datos Objetos"}]),
    ets:new(lugares,   [set, named_table, protected,
                        {keypos, #lugar.uuid},
                        {heir, self(), "Datos Lugares"}]),
    ets:new(cierres,   [set, named_table, protected,
                        {keypos, #cierre.uuid},
                        {heir, self(), "Datos Cierres"}]),
    %% Cargar los datos desde "include/data_mundi".
    ets:insert(entidades, ?DATA_MUNDI),
    ets:insert(pjs, ?PERSONAJES),
    ets:insert(objetos, ?OBJETOS),
    ets:insert(lugares, ?LUGARES),
    ets:insert(cierres, ?CIERRES),
    {ok, #state{estado=listo,contador_entidades=ets:info(entidades,size)}}.

%%--------------------------------------------------------------------
%% @doc
%% Handling call messages
%% @end
%%--------------------------------------------------------------------
%% -spec handle_call(Request :: term(), From :: {pid(), term()}, State :: term()) ->
%%           {reply, Reply :: term(), NewState :: term()} |
%%           {reply, Reply :: term(), NewState :: term(), Timeout :: timeout()} |
%%           {reply, Reply :: term(), NewState :: term(), hibernate} |
%%           {noreply, NewState :: term()} |
%%           {noreply, NewState :: term(), Timeout :: timeout()} |
%%           {noreply, NewState :: term(), hibernate} |
%%           {stop, Reason :: term(), Reply :: term(), NewState :: term()} |
%%           {stop, Reason :: term(), NewState :: term()}.
handle_call({mover_entidad, Datos}, _From, State) ->
    Reply = mover_entidad(Datos),
    {reply, Reply, State};
handle_call(nuevo_uuid, _From, State) ->
    Reply = State#state.contador_entidades,
    New_State = State#state{contador_entidades=State#state.contador_entidades+1},
    {reply, Reply, New_State};
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

%%--------------------------------------------------------------------
%% @doc
%% Handling cast messages
%% @end
%%--------------------------------------------------------------------
%% -spec handle_cast(Request :: term(), State :: term()) ->
%%           {noreply, NewState :: term()} |
%%           {noreply, NewState :: term(), Timeout :: timeout()} |
%%           {noreply, NewState :: term(), hibernate} |
%%           {stop, Reason :: term(), NewState :: term()}.
handle_cast({actualizar_lugar, Lugar}, State) ->
    ets:insert(lugares, Lugar),
    {noreply, State};
handle_cast({actualizar_objeto, Objeto}, State) ->
    ets:insert(objetos, Objeto),
    {noreply, State};
handle_cast({nuevo_personaje, PJ}, State) ->
    Entidad = #entidad{uuid=PJ#pjs.uuid,tipo=?PERSONAJE,nombre=PJ#pjs.nombre,parent=?UUID_LOCALIDAD_MUNDO},
    %% Se inserta un registro de Personaje y otro de Entidad.
    ets:insert(pjs,PJ),
    ets:insert(entidades, Entidad),
    {noreply, State};
handle_cast({actualizar_jugador, PJ}, State) ->
    %% El parámetro PJ es un registro pjs completo
    ets:insert(pjs, PJ),
    {noreply, State};
handle_cast({liberar_jugador, UUID}, State) ->
    PJs = ets:lookup(pjs, UUID),
    case PJs of
        [] -> {noreply, State};
        [H] ->
            ets:insert(pjs, H#pjs{en_juego=?LIBRE}),
            {noreply, State}
    end;
handle_cast({actualizar_entidad, Entidad}, State) ->
    ets:insert(entidades, Entidad),
    {noreply, State};
handle_cast({actualizar_cierre, Cierre}, State) ->
    ets:insert(cierres, Cierre),
    {noreply, State};
handle_cast({bloquear_jugador, UUID}, State) ->
    PJs = ets:lookup(pjs, UUID),
    case PJs of
        [] -> {noreply, State};
        [H] ->
            ets:insert(pjs, H#pjs{en_juego=?OCUPADO}),
            {noreply, State}
    end;
handle_cast(_Request, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @doc
%% Handling all non call/cast messages
%% @end
%%--------------------------------------------------------------------
%% -spec handle_info(Info :: timeout() | term(), State :: term()) ->
%%           {noreply, NewState :: term()} |
%%           {noreply, NewState :: term(), Timeout :: timeout()} |
%%           {noreply, NewState :: term(), hibernate} |
%%           {stop, Reason :: normal | term(), NewState :: term()}.
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%% @end
%%--------------------------------------------------------------------
%% -spec terminate(Reason :: normal | shutdown | {shutdown, term()} | term(),
%%                 State :: term()) -> any().
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @doc
%% Convert process state when code is changed
%% @end
%%--------------------------------------------------------------------
%% -spec code_change(OldVsn :: term() | {down, term()},
%%                   State :: term(),
%%                   Extra :: term()) -> {ok, NewState :: term()} |
%%           {error, Reason :: term()}.
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%--------------------------------------------------------------------
%% @doc
%% This function is called for changing the form and appearance
%% of gen_server status when it is returned from sys:get_status/1,2
%% or when it appears in termination error logs.
%% @end
%%--------------------------------------------------------------------
%% -spec format_status(Opt :: normal | terminate,
%%                     Status :: list()) -> Status :: term().
format_status(_Opt, Status) ->
    Status.

%%%===================================================================
%%% Internal functions
%%%===================================================================

%%-------------------------------------------------------------------
%% @doc
%% Mueve UUID_Entidad, desde la entidad con UUID_Origen a la
%% UUID_Destino, en la tabla de «entidades» que representa el mundo.
%% Supone que el llamante ha comprobado que el movimiento es posible
%% y coherente con el estado actual y las reglas del mundo (o que
%% quiere saltárserlas a conciencia).
%% Introduce las nuevas entidades en la tabla.
%% @end
%%-------------------------------------------------------------------
mover_entidad(Datos) ->
    [UUID_Entidad, UUID_Origen, UUID_Destino] = Datos,
    Entidad = hd(ets:lookup(entidades, UUID_Entidad)),
    Origen  = hd(ets:lookup(entidades, UUID_Origen)),
    Destino = hd(ets:lookup(entidades, UUID_Destino)),
    New_Children_O = lists:delete(UUID_Entidad, Origen#entidad.children),
    New_Children_D = lists:append(Destino#entidad.children, [UUID_Entidad]),
    ets:insert(entidades, Entidad#entidad{parent=UUID_Destino}),
    ets:insert(entidades, Origen#entidad{children=New_Children_O}),
    ets:insert(entidades, Destino#entidad{children=New_Children_D}),
    ok.
