%%%-------------------------------------------------------------------
%%% @author Notxor <notxor@nueva-actitud.org>
%%% @copyright (C) 2020, Notxor
%%% @doc
%%% Gestor cliente de la Aplicación.
%%% @end
%%% Created :  6 Jul 2020 by Notxor <notxor@nueva-actitud.org>
%%%-------------------------------------------------------------------
-module(erlmud_cliente).

-behaviour(gen_server).

%% API
-export([start_link/1]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3, format_status/2]).

-include("include/registros.hrl").
-include("include/sinonimos.hrl").

-define(SERVER, ?MODULE).

-record(state, {socket, lsocket, estado, registro}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%% @end
%%--------------------------------------------------------------------
%% -spec start_link() -> {ok, Pid :: pid()} |
%%           {error, Error :: {already_started, pid()}} |
%%           {error, Error :: term()} |
%%           ignore.
start_link(LSocket) ->
    gen_server:start_link(?MODULE, LSocket, []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Initializes the server
%% @end
%%--------------------------------------------------------------------
%% -spec init(Args :: term()) -> {ok, State :: term()} |
%%           {ok, State :: term(), Timeout :: timeout()} |
%%           {ok, State :: term(), hibernate} |
%%           {stop, Reason :: term()} |
%%           ignore.
init(LSocket) ->
    process_flag(trap_exit,true),
    gen_server:cast(self(), {accept,LSocket}),
    {ok, #state{lsocket=LSocket, estado=sin_pj}}.

%%--------------------------------------------------------------------
%% @doc
%% Handling call messages
%% @end
%%--------------------------------------------------------------------
%% -spec handle_call(Request :: term(), From :: {pid(), term()}, State :: term()) ->
%%           {reply, Reply :: term(), NewState :: term()} |
%%           {reply, Reply :: term(), NewState :: term(), Timeout :: timeout()} |
%%           {reply, Reply :: term(), NewState :: term(), hibernate} |
%%           {noreply, NewState :: term()} |
%%           {noreply, NewState :: term(), Timeout :: timeout()} |
%%           {noreply, NewState :: term(), hibernate} |
%%           {stop, Reason :: term(), Reply :: term(), NewState :: term()} |
%%           {stop, Reason :: term(), NewState :: term()}.
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

%%--------------------------------------------------------------------
%% @doc
%% Handling cast messages
%% @end
%%--------------------------------------------------------------------
%% -spec handle_cast(Request :: term(), State :: term()) ->
%%           {noreply, NewState :: term()} |
%%           {noreply, NewState :: term(), Timeout :: timeout()} |
%%           {noreply, NewState :: term(), hibernate} |
%%           {stop, Reason :: term(), NewState :: term()}.
handle_cast({accept, LSocket}, State) ->
    {ok, Socket} = gen_tcp:accept(LSocket),
    erlmud_sup:start_cliente(),
    %% Informar al servidor que tiene un nuevo cliente.
    Socket_Servidor = #socket{socket=Socket,pid=self()},
    gen_server:cast(erlmud_servidor, {add_cliente, Socket_Servidor}),
    gen_tcp:send(Socket, "Introduce el nombre de tu personaje o teclea 'crear' (sin las comillas) para crear uno nuevo: "),
    {noreply, State#state{socket=Socket}};
handle_cast({informar_evento, Mensaje}, State) ->
    %% la cadena de escape retrocede 30 posiciones en la línea actual del cursor
    escribir_mensaje("\e[30D" ++ Mensaje, State),
    {noreply, State};
handle_cast({dormir, Tiempo}, State) ->
    %% Después de transcurrido el Tiempo se envía un mensaje «despertar».
    timer:send_after(Tiempo, despertar),
    {noreply, State};
handle_cast(_Request, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @doc
%% Handling all non call/cast messages
%% @end
%%--------------------------------------------------------------------
%% -spec handle_info(Info :: timeout() | term(), State :: term()) ->
%%           {noreply, NewState :: term()} |
%%           {noreply, NewState :: term(), Timeout :: timeout()} |
%%           {noreply, NewState :: term(), hibernate} |
%%           {stop, Reason :: normal | term(), NewState :: term()}.
handle_info({tcp, Socket, Linea}, State) ->
    %% limpiar la entrada convertida en unicode.
    Cadena = utiles:limpiar_entrada(unicode:characters_to_list(list_to_binary(Linea))),
    %% Resultado debería ser una cadena para mostrarla y New_State el nuevo
    %% estado si se ha producido algún cambio debido a alguna acción del jugador.
    {Resultado, New_State} =
        case State#state.estado of
            sin_pj  ->
                asignar_pj_a_socket(Socket, Cadena, State);
            poner_nombre ->
                %% En este paso se ha pedido un nombre. Si Cadena es un nombre no
                %% utilizado por otro personaje se avanza en la creación del
                %% personaje y se pregunta por la Raza.
                poner_nombre(Cadena, State);
            establecer_sexo ->
                %% En este paso se ha preguntado por el sexo del personaje. Debe ser
                %% «Masculino» o «Femenino» simbolizados ambos por las letras «M» y «F»
                %% respectivamente.
                establecer_sexo(Cadena, State);
            establecer_raza ->
                %% En este paso se ha preguntado por la raza y vamos a establecerla
                establecer_raza(Cadena, State);
            establecer_profesion ->
                %% En este paso se ha preguntado por la profesión y procedemos a establecerla
                establecer_profesion(Cadena, State);
            establecer_descripcion ->
                %% Muestra la descripción del personaje por pantalla y pregunta su aceptación
                establecer_descripcion(Cadena, State);
            aceptar_personaje ->
                %% Se envían los datos del personaje a erlmud_datos para que cree los registros oportunos
                aceptar_personaje(Cadena,State);
            recargar ->
                %% Se recargan los datos de nuevo
                PJ = State#state.registro,
                asignar_pj_a_socket(Socket, PJ#pjs.nombre, State);
            normal ->
                analizar_comando(Cadena, State);
            durmiendo ->
                {"\nEstás durmiendo... espera un ratito más.", State};
            _ ->
                {"No sé qué decir.", State}
        end,
    case is_list(Resultado) of
        true -> escribir_mensaje(Resultado, New_State);
        false -> ""
    end,
    {noreply, New_State};
handle_info({tcp_closed, _Socket}, State) ->
    terminate(normal, State),
    {noreply, State};
handle_info(despertar, State) ->
    %% Para despertar hay que estar durmiendo
    if
        State#state.estado == durmiendo ->
            New_State = State#state{estado=normal},
            escribir_mensaje("\n...te despiertas con energías renovadas", New_State),
            {noreply, New_State};
        true ->
            {noreply, State}
    end;
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%% @end
%%--------------------------------------------------------------------
%% -spec terminate(Reason :: normal | shutdown | {shutdown, term()} | term(),
%%                 State :: term()) -> any().
terminate(normal, State) ->
    Pj = State#state.registro,
    gen_server:cast(erlmud_datos, {liberar_jugador, Pj#pjs.uuid}),
    gen_server:cast(erlmud_servidor, {del_cliente, Pj#pjs.uuid}),
    ok;
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @doc
%% Convert process state when code is changed
%% @end
%%--------------------------------------------------------------------
%% -spec code_change(OldVsn :: term() | {down, term()},
%%                   State :: term(),
%%                   Extra :: term()) -> {ok, NewState :: term()} |
%%           {error, Reason :: term()}.
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%--------------------------------------------------------------------
%% @doc
%% This function is called for changing the form and appearance
%% of gen_server status when it is returned from sys:get_status/1,2
%% or when it appears in termination error logs.
%% @end
%%--------------------------------------------------------------------
%% -spec format_status(Opt :: normal | terminate,
%%                     Status :: list()) -> Status :: term().
format_status(_Opt, Status) ->
    Status.

%%%===================================================================
%%% Internal functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Si la respuesta guardada en Cadena es afirmativa, se llama al
%% proceso erlmud_datos para guardar el personaje generado durante el
%% todo el proceso. Si es negativa se cierra el socket.
%% @end
%%--------------------------------------------------------------------
aceptar_personaje(Cadena, State) ->
    PJ = State#state.registro,
    R = string:tokens(utiles:quitar_tildes(string:lowercase(Cadena)), ", "),
    case R of
        [H] ->
            if
                H == "s" orelse
                H == "si" ->
                    %% Guardar el personaje
                    UUID_PJ = gen_server:call(erlmud_datos, nuevo_uuid),
                    New_Pj = PJ#pjs{uuid = UUID_PJ,estado=?DE_PIE,en_juego=?LIBRE},
                    %% Creamos el personaje con los datos establecidos
                    gen_server:cast(erlmud_datos, {nuevo_personaje, New_Pj}),
                    Mensaje = "Se han creado los datos del personaje correctamente. Ahora puedes disfrutar plenamente de este mundo virtual (pulsa ENTER).",
                    New_State = State#state{estado=recargar,registro=New_Pj};
                true ->
                    Mensaje = "No se ha completado bien el proceso. Inténtalo de nuevo.",
                    New_State = State#state{estado=sin_pj}
            end;
        _ ->
            Mensaje = "No se ha completado bien el proceso. Inténtalo de nuevo.",
            New_State = State#state{estado=sin_pj}
    end,
    {Mensaje, New_State}.

%%--------------------------------------------------------------------
%% @doc
%% Durante el proceso de creación de un Personaje nuevo muestra un
%% resumen de las principales características del mismo y pide
%% confirmación para generar el personaje con esos parámetros.
%% @end
%%--------------------------------------------------------------------
establecer_descripcion(_Cadena, State) ->
    PJ = State#state.registro,
    {Anio_Actual,_,_,_} = gen_server:call(erlmud_sol, dime_fecha),
    {Anio_Nacimiento,_,_} = PJ#pjs.nacimiento,
    Edad = Anio_Actual - Anio_Nacimiento,
    Desc =
        case PJ#pjs.formato of
            true ->
                utiles:formato(negrita, "Nombre: ") ++ PJ#pjs.nombre_propio ++ "\n"
                    ++ utiles:formato(negrita, "Género: ") ++ maps:get(PJ#pjs.gg, ?GENERO) ++ "\n"
                    ++ utiles:formato(negrita, "Edad: ")  ++ io_lib:format("~p~n", [Edad])
                    ++ utiles:descripcion_raza(true,PJ#pjs.raza) ++ "\n"
                    ++ utiles:formato(negrita, "Altura: ") ++ io_lib:format("~p",[PJ#pjs.altura]) ++ "\n"
                    ++ utiles:formato(negrita, "Peso: ") ++ io_lib:format("~p~n", [PJ#pjs.peso]);
            false ->
                "Nombre: " ++ PJ#pjs.nombre_propio ++ "\n"
                    ++ "Género: " ++ maps:get(PJ#pjs.gg, ?GENERO) ++ "\n"
                    ++ "Edad: "  ++ io_lib:format("~p~n", [Edad])
                    ++ utiles:descripcion_raza(false,PJ#pjs.raza) ++ "\n"
                    ++ "Altura: " ++ io_lib:format("~p",[PJ#pjs.altura]) ++ "\n"
                    ++ "Peso: " ++ io_lib:format("~p~n", [PJ#pjs.peso])
        end,
    Respuesta = Desc ++ "\n\nSe ha creado un personaje con los datos anteriores. ¿Estás conforme con el personaje creado? (Sí/No): ",
    New_Pj = PJ#pjs{desc=Desc},
    New_State = State#state{estado=aceptar_personaje,registro=New_Pj},
    {Respuesta, New_State}.

%%--------------------------------------------------------------------
%% @doc
%% Durante el proceso de creación de un Personaje nuevo, se llama a
%% esta función para establecer los valores que dependen de la
%% profesión.
%%
%% Devuelve una lista de valores entre 1 y 100 de todos los valores de
%% las características. Las /especiales/ son aquellas que vienen
%% marcadas como principal o secundaria, según las tablas de profesión.
%% En ese caso, se obliga a que el valor esté por encima del 75,
%% haciendo variable sólo una cuarta parte del valor.
%% @end
%%--------------------------------------------------------------------
establecer_valores_profesion(Profesion) ->
    Lista_Valores = [?FUE,?AGI,?INT,?CON,?NUM,?MEC,?ESP,?SOC,?COO],
    {Principal_Prof,Secundaria_Prof} = maps:get(Profesion,?TABLA_PROFESIONES),
    lists:map(
      fun(X) ->
              if
                  X == Principal_Prof orelse
                  X == Secundaria_Prof ->
                      (utiles:dado(100)+300) div 400;
                  true ->
                      utiles:dado(100)
              end
      end,
      Lista_Valores).

%%--------------------------------------------------------------------
%% @doc
%% Durante el proceso de creación de un Personaje nuevo, se llama a
%% esta función para establecer la profesión del mismo.
%%
%% Si la cadena llega vacía o contiene una profesión no válida se
%% establecerá la profesión aleatoriamente.
%% @end
%%--------------------------------------------------------------------
establecer_profesion(Cadena, State) ->
    PJ = State#state.registro,
    Tokens = string:tokens(utiles:quitar_tildes(string:lowercase(Cadena)), ", "),
    Profesiones = [string:trim(X) || X <- Tokens, X /= "la", X /= "el", X /= "y"],
    case Profesiones of
        [] ->
            %% No proporciona profesión. Se establecerá una al azar.
            Profesion = utiles:dado(maps:size(?PROFESIONES)),
            Lista_Valores = establecer_valores_profesion(Profesion),
            New_Pj = PJ#pjs{
                       fue = lists:nth(1,Lista_Valores),
                       agi = lists:nth(2,Lista_Valores),
                       int = lists:nth(3,Lista_Valores),
                       con = lists:nth(4,Lista_Valores),
                       num = lists:nth(5,Lista_Valores),
                       mec = lists:nth(6,Lista_Valores),
                       esp = lists:nth(7,Lista_Valores),
                       soc = lists:nth(8,Lista_Valores),
                       coo = lists:nth(9,Lista_Valores),
                       profesion = Profesion},
            New_State = State#state{estado=establecer_descripcion,registro=New_Pj},
            {"Se ha establecido la profesión a «"
             ++ maps:get(Profesion,?PROFESIONES) ++ "».",New_State};
        [H] ->
            %% Se ha proporcionado una profesión, si es válida se asigna y si no
            %% mostrará un mensaje de error
            Profesion = maps:get(H,?PROFESION_VALOR,none),
            case Profesion of
                none ->
                    {"La profesión introducida no es válida. Por favor, introduce un nombre de profesión válido o déjalo en blanco para que decida el programa:",State};
                _ ->
                    Lista_Valores = establecer_valores_profesion(Profesion),
                    New_Pj = PJ#pjs{
                               fue = lists:nth(1,Lista_Valores),
                               agi = lists:nth(2,Lista_Valores),
                               int = lists:nth(3,Lista_Valores),
                               con = lists:nth(4,Lista_Valores),
                               num = lists:nth(5,Lista_Valores),
                               mec = lists:nth(6,Lista_Valores),
                               esp = lists:nth(7,Lista_Valores),
                               soc = lists:nth(8,Lista_Valores),
                               coo = lists:nth(9,Lista_Valores),
                               profesion = Profesion},
                    New_State = State#state{estado=establecer_descripcion,registro=New_Pj},
                    {"Se ha establecido la profesion a «"
                     ++ maps:get(Profesion, ?PROFESIONES) ++ "».",New_State}
            end;
        _ ->
            {"No entiendo qué profesión quieres para el personaje.\nPor favor, introduce un nombre de profesión válido o déjalo en blanco para que decida el programa:",State}
    end.

%%--------------------------------------------------------------------
%% @doc
%% Dada una Raza, devuelve una tupla con los valores diferenciados para
%% dicha raza: Edad, Altura y Peso.
%%
%% La edad está expresada en una /tupla/ que expresa el año, el mes y
%% el día de nacimiento.
%%
%% La altura es un entero que expresa centímetros.
%%
%% El peso es un enterio que expresa kilos.
%% @end
%%--------------------------------------------------------------------
establecer_valores_raza(Raza,State) ->
    PJ = State#state.registro,
    Genero = PJ#pjs.gg,
    G = if
            Genero == ?MASCULINO -> "M";
            true -> "F"
        end,
    {Valor_Raza, _} = hd(Raza),
    R = if
            Valor_Raza == ?ELFO -> "E";
            Valor_Raza == ?ORCO -> "O";
            Valor_Raza == ?HOMBRE -> "H";
            true -> "D"
        end,
    %% El índice del diccionario ?TABLA_RAZA son dos letras para Raza y Género
    Indice = R ++ G,
    Valores = maps:get(Indice, ?TABLA_RAZA, none),
    if
        Valores /= none ->
            %% Esto quiere decir que ha encontrado algún valor en la tabla.
            {Edad_min, Edad_max,_,_,Altura_min,Altura_max,Peso_min,Peso_max} = Valores;
        true ->
            {Edad_min,Edad_max,Altura_min,Altura_max,Peso_min,Peso_max} ={16,180,90,195,20,200}
    end,
    {Anio_Actual,_,_,_} = gen_server:call(erlmud_sol, dime_fecha),
    %% El día de la fecha de nacimiento al azar convertido a mes y día
    Dia_Nacimiento = utiles:dado(365),
    Mes = 1 + Dia_Nacimiento div 30,
    Dia = 1 + Dia_Nacimiento rem 30,
    Anio_Nacimiento = Anio_Actual - utiles:distribucion_valores(utiles:dado(100),1,100,Edad_min,Edad_max),
    Dado_Altura = utiles:dado(100),
    Altura = utiles:distribucion_valores(Dado_Altura,1,100,Altura_min,Altura_max),
    %% El peso se debe calcular también en función de la altura también.
    Dado_Peso = Dado_Altura + 9 - utiles:dado(19), % +- 8%
    Peso = utiles:distribucion_valores(Dado_Peso,-9,109,Peso_min,Peso_max),
    {{Anio_Nacimiento,Mes,Dia}, Altura, Peso}.

%%--------------------------------------------------------------------
%% @doc
%% Durante el proceso de creación de un Personaje nuevo, se llama a
%% esta función para establecer la mezcla de razas.
%%
%% Si no hay parámetro se realiza una asignación al azar.
%% @end
%%--------------------------------------------------------------------
dos_razas() ->
    Raza1_Azar = utiles:dado(4),
    Raza2_Azar = utiles:dado(4),
    if
        Raza1_Azar == Raza2_Azar ->
            %% Si las dos razas son la misma repetir, hasta que sean distintas.
            dos_razas();
        true ->
            dos_razas([Raza1_Azar,Raza2_Azar])
    end.
dos_razas([H,T]) ->
    Valor = (utiles:dado(100)+100) div 2,
    [{H, Valor},{T,(100-Valor)}].

%%--------------------------------------------------------------------
%% @doc
%% Durante el proceso de creación de un Personaje nuevo, se llama a
%% esta función para establecer la raza del mismo.
%%
%% Si la cadena llega vacía o contiene una raza no válida se establecerá
%% la raza aleatoriamente.
%% @end
%%--------------------------------------------------------------------
establecer_raza(Cadena,State) ->
    PJ = State#state.registro,
    Tokens = string:tokens(utiles:quitar_tildes(string:lowercase(Cadena)), ", "),
    Razas = [string:trim(X) || X <- Tokens, X /= "la", X /= "el", X /= "y", X /= "con"],
    case Razas of
        [] ->
            %% No proporciona raza. Se establecerá al azar.
            %% Un 25% de probabilidad de que el personaje sea mezcla de 2 razas.
            Num_Razas = (utiles:dado(4)+1) div 3,
            Raza = case Num_Razas of
                       1 ->
                           [{utiles:dado(?NUM_RAZAS_PJ), 100}];
                       _ ->
                           dos_razas()
                   end,
            %% Establecer los valores por Raza: edad, altura y peso
            {Nacimiento, Altura, Peso} = establecer_valores_raza(Raza, State),
            New_Pj = PJ#pjs{raza = Raza, nacimiento=Nacimiento, altura=Altura, peso=Peso},
            New_State = State#state{estado=establecer_profesion,registro=New_Pj},
            {"Estableciendo «raza» al azar -- " ++ utiles:descripcion_raza(false,Raza)
             ++ "\nElige una profesión para el personaje, si lo dejas en blanco se elegirá una al azar:", New_State};
        [H] ->
            %% Proporciona sólo una raza.
            Raza_Tipo = maps:get(H,?RAZA_VALUE,none),
            case Raza_Tipo of
                none ->
                    {io_lib:format("«~p» no es una raza válida. Introduce el nombre de una raza válido o déjalo en blanco para que decida el programa:",[H]), State};
                _ ->
                    Raza = [{Raza_Tipo,100}],
                    %% Establecer los valores por Raza: edad, altura y peso
                    {Nacimiento, Altura, Peso} = establecer_valores_raza(Raza, State),
                    New_Pj = PJ#pjs{raza = Raza, nacimiento=Nacimiento, altura=Altura, peso=Peso},
                    New_State = State#state{estado=establecer_profesion,registro=New_Pj},
                    {"Raza establecida -- " ++ utiles:descripcion_raza(false,Raza)
                     ++ "\nElige una profesión para el personaje, si lo dejas en blanco el programa elegirá una al azar:" ,New_State}
            end;
        [H,T] ->
            %% Se proporcionan dos razas.
            R1_Tipo = maps:get(H,?RAZA_VALUE,none),
            R2_Tipo = maps:get(T,?RAZA_VALUE,none),
            if
                R1_Tipo /= none andalso
                R2_Tipo /= none ->
                    Raza = dos_razas([R1_Tipo,R2_Tipo]),
                    %% Establecer los valores por Raza: edad, altura y peso
                    {Nacimiento, Altura, Peso} = establecer_valores_raza(Raza, State),
                    New_Pj = PJ#pjs{raza = Raza, nacimiento=Nacimiento, altura=Altura, peso=Peso},
                    New_State = State#state{estado=establecer_profesion,registro=New_Pj},
                    {"Raza establecida -- " ++ utiles:descripcion_raza(false,Raza)
                     ++ "\nElige una profesión para el personaje, si lo dejas en blanco el programa elegirá una al azar:",New_State};
                true ->
                    {"O «" ++ H ++ "» o «" ++ T ++ "» no es un nombre de raza correcto. Introduce nombres de raza correctos o déjalo en blanco para que decida el programa:",State}
            end;
        _ ->
            {"Entre las razas debes elegir una (o dos para crear un personaje mestizo), o dejar que el sistema decida... Introduce nombres de raza correctos:",State}
    end.

%%--------------------------------------------------------------------
%% @doc
%% Durante el proceso de creación de un Personaje nuevo, se llama a
%% esta función para establecer el sexo del mismo.
%%
%% Si la cadena llega vacía o contiene sexo no válida no establecerá
%% aleatoriamente como en la raza, sino que se volverá a preguntar por
%% un valor «M» (masculino) o «F» (femenino).
%% @end
%%--------------------------------------------------------------------
establecer_sexo(Cadena,State) ->
    PJ = State#state.registro,
    Sexo = string:lowercase(Cadena),
    case Sexo of
        "m" ->
            Estado = State#state{estado=establecer_raza, registro=PJ#pjs{gg=?MASCULINO}},
            {"¿De qué raza quieres que sea el personaje?\nPuedes dejarlo en blanco y el programa decidirá al azar:",Estado};
        "f" ->
            Estado = State#state{estado=establecer_raza, registro=PJ#pjs{gg=?FEMENINO}},
            {"¿De qué raza quieres que sea el personaje?\nPuedes dejarlo en blanco y el programa decidirá al azar:",Estado};
        _ ->
            {"Debes elegir entre «M» y «F»; es sólo una convención para el personaje:",State}
    end.

%%--------------------------------------------------------------------
%% @doc
%% Durante el proceso de creación de un Personaje nuevo, se llama a
%% esta función para establecer el nombre del mismo.
%%
%% Si es un nombre ya en uso pregunta por otro nombre. Si el nombre no
%% está siendo utilizado, se establece el «nombre propio» a la Cadena
%% introducida, el «nombre» a una versión del «nombre propio» en
%% minúsculas, sin tildes y sin artículo; se avanza el estado a
%% «establecer_raza» y se envía la cadena preguntando por la «raza».
%% @end
%%--------------------------------------------------------------------
poner_nombre(Cadena,State) ->
    PJ = State#state.registro,
    Tokens = string:tokens(utiles:quitar_tildes(string:lowercase(Cadena)), " "),
    Nombre_Propuesto = hd([X || X <- Tokens, X /= "la", X /= "el"]),
    PJs = [X || X <- ets:tab2list(pjs), X#pjs.nombre==Nombre_Propuesto],
    case PJs of
        [] ->
            %% El nombre no existe y se utilizará. Se pasa al paso siguiente «establecer_raza»
            Estado = State#state{estado=establecer_sexo,registro=PJ#pjs{nombre_propio=Cadena, nombre=Nombre_Propuesto}},
            {"¿Qué género quieres que use el personaje? Masculino (M) o Femenino (F):", Estado};
        _ ->
            Estado = State#state{estado=poner_nombre,registro=PJ#pjs{nombre_propio=""}},
            {"Ese nombre ya está siendo utilizado por otro jugador, si ese personaje es tuyo avisa al administrador del sistema. Mientras puedes probar con otro personaje:",Estado}
    end.

%%--------------------------------------------------------------------
%% @doc
%% Escribe el Mensaje en el Socket. Le añade el prompt y lo convierte
%% a unicode binary
%% @end
%%--------------------------------------------------------------------
escribir_mensaje(Mensaje, State) ->
    Mensaje_Salida = utiles:hacer_contracciones(Mensaje),
    #pjs{formato=Formato} = State#state.registro,
    Respuesta =
        case State#state.estado of
            normal ->
                Mensaje_Salida ++ "\n" ++ prompt(State);
            saliendo ->
                Mensaje_Salida ++ " ¡Hasta pronto!\n";
            _ ->
                utiles:formatear(Formato,Mensaje_Salida)
        end,
    gen_tcp:send(State#state.socket, unicode:characters_to_binary(Respuesta)).

%%-------------------------------------------------------------------
%% @doc
%% Cuando se recibe un mensaje desde un Socket a través de
%% handle_call/3 si no ese Socket no está asociado a ningún jugador,
%% se llamará a esta función para asignar un nombre. Si falla cierra
%% el Socket y si no asigna el establece la relación entre Socket y
%% PJ.
%% @end
%%-------------------------------------------------------------------
asignar_pj_a_socket(Socket, Cadena, State) ->
    Hecho = if
        Cadena /= "" ->
            Tokens = string:tokens(utiles:quitar_tildes(string:lowercase(Cadena)), " "),
            %% Eliminamos artículos de los tokens y nos quedamos con la primera cadena
            Nombre = hd([X || X <- Tokens, X /= "la", X /= "el"]),
            PJs = [X || X <- ets:tab2list(pjs), X#pjs.nombre==Nombre],
            case PJs of
                [] ->
                    case Nombre of
                        "crear" ->
                            crear_personaje;
                        _ ->
                            no_pj
                    end;
                [H] ->
                    UUID_PJ = H#pjs.uuid,
                    case H#pjs.en_juego of
                        ?LIBRE ->
                            %% No hay PJ asignado al socket, así que se asigna el socket al jugador
                            Nuevo_Registro = #socket{socket=Socket, pid=self(), uuid=UUID_PJ, nombre=Nombre},
                            gen_server:cast(erlmud_servidor, {add_cliente, Nuevo_Registro}),
                            %% Se marca como activo el PJ poniendo el socket correspondiente
                            gen_server:cast(erlmud_datos, {bloquear_jugador, UUID_PJ}),
                            %% Se mueve la Entidad al «mundo»
                            Entidad = hd(ets:lookup(entidades, UUID_PJ)),
                            Datos_Movimiento = [UUID_PJ, Entidad#entidad.parent, Entidad#entidad.parent],
                            gen_server:cast(erlmud_servidor, {evento_ir, UUID_PJ, Entidad#entidad.parent, Entidad#entidad.parent}),
                            gen_server:call(erlmud_datos, {mover_entidad, Datos_Movimiento}),
                            {ok, H};
                        ?OCUPADO ->
                            ocupado
                    end
            end
    end,
    case Hecho of
        no_pj ->
            gen_tcp:send(Socket, "No existe un personaje con ese nombre. Escribe el nombre correctamente\no 'crear' para generar un personaje nuevo: "),
            {nil, State};
        crear_personaje ->
            PJ = #pjs{nombre_propio=""},
            New_State = State#state{estado=poner_nombre,registro=PJ},
            escribir_mensaje("Vamos a iniciar el proceso de generar un personaje nuevo, escribe el nombre que\nquieres que utilice tu personaje:",New_State),
            {nil, New_State};
        ocupado ->
            gen_tcp:send(Socket, "Ya existe un personaje con ese nombre.\nPor favor, escribe otro nombre: "),
            {nil, State};
        {ok,PJ} ->
            New_State = State#state{estado=normal,registro=PJ},
            {"Nos alegramos de verte por aquí.", New_State};
        _ ->
            gen_tcp:send(Socket, "Introduce un nombre de personaje: "),
            {nil, State}
    end.

%%--------------------------------------------------------------------
%% @doc
%% Esta función devuelve una cadena formando el prompt para el cliente.
%% @end
%%--------------------------------------------------------------------
prompt(State) ->
    Pj = State#state.registro,
    case Pj#pjs.formato of
        true ->
            utiles:formato(negrita, "<")
                ++ utiles:formato(amarillo,Pj#pjs.nombre_propio)
                ++ utiles:formato(negrita, ":")
                ++ utiles:formato(verde,io_lib:format("~p/~p", [Pj#pjs.pvE,Pj#pjs.pvR]))
                ++ utiles:formato(negrita, ":")
                ++ utiles:formato(amarillo,io_lib:format("~p", [Pj#pjs.energia]))
                ++ utiles:formato(negrita, "> ");
        false ->
            io_lib:format("<~s:~p/~p:~p> ", [Pj#pjs.nombre_propio,Pj#pjs.pvE,Pj#pjs.pvR,Pj#pjs.energia])
    end.

%%--------------------------------------------------------------------
%% @doc
%% Esta función responde a la acción de mirar desde la perspectiva del
%% UUID_PJ que recibe como parámetro. Si se recibe un segundo
%% parámetro indica la intención de mirar un Objeto determinado. Se
%% entiende que hasta aquí se ha comprobado si el objeto está al
%% alcance del sujeto.
%% @end
%%--------------------------------------------------------------------
mirar(State, []) ->
    Pj = State#state.registro,
    P = utiles:obtener_entidad_parent(Pj#pjs.uuid),
    Respuesta = utiles:situar_cursor_inicio_linea() ++ utiles:obtener_descripcion_lugar(Pj#pjs.uuid,P#entidad.uuid),
    {Respuesta,State};
mirar(State, Objetos) ->
    %% Se tiene en cuenta la entidad más significativa
    Obj_Dir = utiles:devolver_entidad_significativa(Objetos),
    %% Se tiene en cuenta si la salida debe ser con o sin formato
    #pjs{formato=Formato} = State#state.registro,
    Mensaje = utiles:obtener_descripcion_objeto(Formato,Obj_Dir#entidad.tipo, Obj_Dir),
    {Mensaje, State}.

%%--------------------------------------------------------------------
%% @doc
%% Mueve una entidad al alcance del UUID_PJ a su lista de «children».
%% @end
%%--------------------------------------------------------------------
coger(State, [H]) ->
    Obj_Dir = hd(ets:lookup(entidades, H)),
    Pj = State#state.registro,
    %% Comprobamos si el objeto es móvil
    Movible =
        case Obj_Dir#entidad.tipo of
            ?PERSONAJE ->
                {movil, ok};
            _ ->
                #obj{movil=Movil} = hd(ets:lookup(objetos, H)),
                {movil, Movil}
        end,
    if
        Movible /= {movil,ok} ->
            %% Si no es «cogible» muestra el mensaje de aviso.
            {_,Mensaje} = Movible,
            {Mensaje, State};
        Obj_Dir#entidad.parent /= Pj#pjs.uuid ->
            Datos_Movimiento =[Obj_Dir#entidad.uuid, Obj_Dir#entidad.parent, Pj#pjs.uuid],
            gen_server:call(erlmud_datos, {mover_entidad, Datos_Movimiento}),
            gen_server:cast(erlmud_servidor, {evento_pj, Pj#pjs.uuid, Pj#pjs.nombre_propio ++ " ha cogido " ++
                                                  utiles:nombre_con_articulo(indeterminado, Obj_Dir#entidad.uuid)}),
            {"Coges " ++ utiles:nombre_con_articulo(indeterminado, Obj_Dir#entidad.uuid) ++ ".", State};
        true ->
            {"Ya llevas " ++ utiles:nombre_con_articulo(indeterminado, Obj_Dir#entidad.uuid) ++ ".", State}
    end;
coger(State, [H,T]) ->
    %% Llegan dos objetos, miramos cuál es el contenedor
    %% Ya se podían coger objetos de contenedores abiertos: si el objeto estaba en un contenedor abierto
    %% entraba en el «alcance» del jugador
    Pj = State#state.registro,
    O1 = hd(ets:lookup(entidades, H)),
    O2 = hd(ets:lookup(entidades, T)),
    if
        O1#entidad.tipo == ?OBJETO_CONTENEDOR ->
            Obj_Dir = O2,
            Contenedor = O1;
        O1#entidad.tipo == ?OBJETO_TABLA ->
            Obj_Dir = O2,
            Contenedor = O1;
        true ->
            Obj_Dir = O1,
            Contenedor = O2
    end,
    %% TODO: Comprobar volumen de objeto y de contenedor
    if
        Obj_Dir#entidad.parent /= Contenedor#entidad.uuid ->
            %% Comprobar el objeto está en el contenedor
            {utiles:nombre_con_articulo(determinado, Obj_Dir#entidad.uuid) ++ " no está en " ++
                 utiles:nombre_con_articulo(determinado, Contenedor#entidad.uuid) ++ ".", State};
        Obj_Dir#entidad.parent /= Pj#pjs.uuid ->
            Datos_Movimiento = [Obj_Dir#entidad.uuid, Contenedor#entidad.uuid, Pj#pjs.uuid],
            gen_server:call(erlmud_datos, {mover_entidad, Datos_Movimiento}),
            gen_server:cast(erlmud_servidor, {evento_pj, Pj#pjs.uuid, Pj#pjs.nombre_propio ++ " ha cogido " ++
                                                  utiles:nombre_con_articulo(indeterminado, Obj_Dir#entidad.uuid) ++
                                                  " de " ++ utiles:nombre_con_articulo(determinado, Contenedor#entidad.uuid)}),
            {"Coges " ++ utiles:nombre_con_articulo(indeterminado, Obj_Dir#entidad.uuid) ++ " de "
             ++ utiles:nombre_con_articulo(determinado, Contenedor#entidad.uuid), State};
        true ->
            {"Ya llevas " ++ utiles:nombre_con_articulo(indeterminado, Obj_Dir#entidad.uuid) ++ ".", State}
    end;
coger(State, [H|T]) ->
    Objeto = utiles:devolver_entidad_significativa([H|T]),
    coger(State, [Objeto#entidad.uuid]);
coger(State, _) ->
    {"Hmmm, exactamente: ¿qué quieres coger?", State}.

%%--------------------------------------------------------------------
%% @doc
%% Mueve el objeto de la lista «children» del UUID_PJ a la lista de
%% «children» de la localidad donde se encuentra.
%% @end
%%--------------------------------------------------------------------
coger_todo(State) ->
    Pj = State#state.registro,
    #entidad{parent=Parent} = hd(ets:lookup(entidades,Pj#pjs.uuid)),
    Objetos = utiles:objetos_accesibles(Parent),
    Mensajes =lists:map(
                fun(X) ->
                        Esta_Inventariado = utiles:esta_en_inventario(Pj#pjs.uuid,X),
                        #entidad{tipo=Tipo} = hd(ets:lookup(entidades,X)),
                        case Esta_Inventariado of
                            false when Tipo >= ?OBJETO ->
                                {Mensaje, _} = coger(State,[X]),
                                Mensaje;
                            _ ->
                                ""
                        end
                end,
                Objetos),
    Respuesta = "Coges todo:\n" ++ lists:join("\n",Mensajes),
    {Respuesta, State}.

%%--------------------------------------------------------------------
%% @doc
%% Mueve el objeto de la lista «children» del UUID_PJ a la lista de
%% «children» de la localidad donde se encuentra.
%% @end
%%--------------------------------------------------------------------
dejar(State, [H]) ->
    %% llega un objeto y se deja en el parent.
    Obj_Dir = hd(ets:lookup(entidades, H)),
    Pj = State#state.registro,
    #entidad{parent=UUID_Parent} = hd(ets:lookup(entidades,Pj#pjs.uuid)),
    if
        Obj_Dir#entidad.parent == Pj#pjs.uuid ->
            Datos_Movimiento = [Obj_Dir#entidad.uuid, Pj#pjs.uuid, UUID_Parent],
            gen_server:call(erlmud_datos, {mover_entidad, Datos_Movimiento}),
            gen_server:cast(erlmud_servidor, {evento_pj, Pj#pjs.uuid, Pj#pjs.nombre_propio ++ " ha dejado " ++
                                                  utiles:nombre_con_articulo(indeterminado, Obj_Dir#entidad.uuid)}),
            {"Dejas " ++ utiles:nombre_con_articulo(indeterminado, Obj_Dir#entidad.uuid) ++ ".", State};
        true ->
            {"Hmmm. No entiendo qué quieres dejar.", State}
    end;
dejar(State, [H,T]) ->
    %% Llegan dos objetos, miramos si uno es un contenedor para dejar el objeto en él
    O1 = hd(ets:lookup(entidades, H)),
    O2 = hd(ets:lookup(entidades, T)),
    if
        O1#entidad.tipo == ?OBJETO_CONTENEDOR ->
            Contenedor = O1,
            Obj_Dir = O2;
        O1#entidad.tipo == ?OBJETO_TABLA ->
            Contenedor = O1,
            Obj_Dir = 02;
        true ->
            Contenedor = O2,
            Obj_Dir = O1
    end,
    %% Comprobamos que el objeto que llega como contenedor realmente lo es
    %% es posible que no haya ningún contenedor entre los dos objetos.
    Pj = State#state.registro,
    if
        Contenedor#entidad.tipo == ?OBJETO_CONTENEDOR ->
            Datos_Movimiento = [Obj_Dir#entidad.uuid, Pj#pjs.uuid, Contenedor#entidad.uuid],
            gen_server:call(erlmud_datos, {mover_entidad, Datos_Movimiento}),
            gen_server:cast(erlmud_servidor, {evento_pj, Pj#pjs.uuid, Pj#pjs.nombre_propio ++ " ha dejado " ++
                                                  utiles:nombre_con_articulo(indeterminado, Obj_Dir#entidad.uuid) ++ " en " ++
                                                  utiles:nombre_con_articulo(determinado, Contenedor#entidad.uuid)}),
            Mensaje = "Pones " ++ utiles:nombre_con_articulo(indeterminado, Obj_Dir#entidad.uuid) ++ " en "
                ++ utiles:nombre_con_articulo(determinado, Contenedor#entidad.uuid) ++ ".",
            {Mensaje, State};
        Contenedor#entidad.tipo == ?OBJETO_TABLA ->
            Datos_Movimiento = [Obj_Dir#entidad.uuid, Pj#pjs.uuid, Contenedor#entidad.uuid],
            gen_server:call(erlmud_datos, {mover_entidad, Datos_Movimiento}),
            gen_server:cast(erlmud_servidor, {evento_pj, Pj#pjs.uuid, Pj#pjs.nombre_propio ++ " ha dejado " ++
                                                  utiles:nombre_con_articulo(indeterminado, Obj_Dir#entidad.uuid) ++ " sobre " ++
                                                  utiles:nombre_con_articulo(determinado, Contenedor#entidad.uuid)}),
            Mensaje = "Dejas " ++ utiles:nombre_con_articulo(indeterminado, Obj_Dir#entidad.uuid) ++ " sobre "
                ++ utiles:nombre_con_articulo(determinado, Contenedor#entidad.uuid) ++ ".",
            {Mensaje, State};
        true ->
            {"Hmmm, exactamente: ¿qué quieres dejar?", State}
    end;
dejar(State, [H|T]) ->
    E = utiles:devolver_entidad_significativa([H|T]),
    dejar(State, [E#entidad.uuid]);
dejar(State, _) ->
    {"Hmmm, exactamente: ¿qué quieres dejar?", State}.

%%--------------------------------------------------------------------
%% @doc
%% Convierte las cadenas abreviadas de los puntos cardinales en sus
%% correspondientes nombres largos.
%% @end
%%--------------------------------------------------------------------
normalizar_puntos_cardinales(Tokens) ->
    lists:map(fun(T) ->
       if
           T == "n" -> "norte";
           T == "s" -> "sur";
           T == "e" -> "este";
           T == "o" -> "oeste";
           T == "ne" -> "noreste";
           T == "se" -> "sureste";
           T == "so" -> "suroeste";
           T == "no" -> "noroeste";
           true -> T
       end
    end, Tokens).

%%--------------------------------------------------------------------
%% @doc
%% Analiza Cadenas para realizar un movimiento del UUID_PJ a un lugar
%% accesible desde las «salidas» de la posición actual.
%% @end
%%--------------------------------------------------------------------
ir(State, Cadenas) ->
    Pj = State#state.registro,
    #entidad{parent=UUID_Parent} = hd(ets:lookup(entidades,Pj#pjs.uuid)),
    #entidad{tipo=Tipo_Parent} = hd(ets:lookup(entidades,UUID_Parent)),
    if
        Tipo_Parent == ?LUGAR ->
            #lugar{enlaces=Salidas} = hd(ets:lookup(lugares,UUID_Parent));
        Tipo_Parent == ?OBJETO_LUGAR ->
            #obj{enlaces=Salidas} = hd(ets:lookup(objetos,UUID_Parent));
        true ->
            Salidas = []
    end,
    Tokens = normalizar_puntos_cardinales([X || X <- Cadenas, X /= "ir", X /= "al", X /= "a"]),
    Lista_UUIDs = utiles:salidas_por_nombre(Salidas,hd(Tokens)),
    case Pj#pjs.estado of
        ?SENTADO ->
            {"No puedes ir a ningún sitio estando sentado, deberías levantarte primero.", State};
        ?TUMBADO ->
            {"No puedes ir a ningún sitio estando tumbado, deberías levantarte primero.", State};
        _ ->
            case Lista_UUIDs of
                [] ->
                    {"Hmmm, no entiendo dónde quieres ir.",State};
                [H|_] ->
                    {UUID_Destino, _, _, Desc, UUID_Cierre} = H,
                    Estado_Cierre = utiles:obtener_estado_cierre(UUID_Cierre),
                    case Estado_Cierre of
                        ?ABIERTO ->
                            Datos_Movimiento = [Pj#pjs.uuid, UUID_Parent, UUID_Destino],
                            gen_server:call(erlmud_datos, {mover_entidad, Datos_Movimiento}),
                            gen_server:cast(erlmud_servidor, {evento_ir, Pj#pjs.uuid, UUID_Parent, UUID_Destino}),
                            escribir_mensaje(Desc,State),
                            mirar(State,[]);
                        ?FORZADO ->
                            Datos_Movimiento = [Pj#pjs.uuid, UUID_Parent, UUID_Destino],
                            gen_server:call(erlmud_datos, {mover_entidad, Datos_Movimiento}),
                            gen_server:cast(erlmud_servidor, {evento_ir, Pj#pjs.uuid, UUID_Parent, UUID_Destino}),
                            escribir_mensaje(Desc,State),
                            mirar(State,[]);
                        ?LLAVE_CERRADO ->
                            {"Está cerrado con llave, deberías abrir antes.",State};
                        _ ->
                            {"No puedes ir por ahí, primero deberías abrir.",State}
                    end
            end
    end.

%%--------------------------------------------------------------------
%% @doc
%% Realiza la acción de decir, informando al sujeto y a los PJ
%% presentes.
%% @end
%%--------------------------------------------------------------------
decir(State, Comando) ->
    Pj = State#state.registro,
    Tokens = tl(string:tokens(Comando, " ")),
    Mensaje = string:join(Tokens, " "),          % Se rehace el mensaje habiendo quitado el comando
    gen_server:cast(erlmud_servidor, {evento_decir, Pj#pjs.uuid, Mensaje}),
    {"Dices: «" ++ Mensaje ++ "»", State}.

%%--------------------------------------------------------------------
%% @doc
%% Realiza la acción de gritar. El grito se notifica a toda la zona
%% establecida.
%% @end
%%--------------------------------------------------------------------
gritar(State, Comando) ->
    Pj = State#state.registro,
    Tokens = tl(string:tokens(Comando, " ")),
    Mensaje = string:join(Tokens, " "),
    gen_server:cast(erlmud_servidor, {evento_gritar, Pj#pjs.uuid, Mensaje}),
    case Mensaje of
        "" ->
            {"Gritas con todas tus fuerzas.",State};
        _ ->
            {"Gritas: «" ++ Mensaje ++ "»", State}
    end.

%%--------------------------------------------------------------------
%% @doc
%% Cambia el estado del PJ a sentado.
%% @end
%%--------------------------------------------------------------------
sentar(State,[]) ->
    Pj = State#state.registro,
    New_Pj = Pj#pjs{estado = ?SENTADO},
    New_State = State#state{registro=New_Pj},
    gen_server:cast(erlmud_datos, {actualizar_jugador, New_Pj}),
    gen_server:cast(erlmud_servidor, {evento_pj, Pj#pjs.uuid, Pj#pjs.nombre_propio ++ " se sienta en el suelo."}),
    {"Te sientas en el suelo", New_State};
sentar(State,[H]) ->
    Asiento = hd(ets:lookup(entidades,H)),
    Pj = State#state.registro,
    case Asiento#entidad.tipo of
        ?OBJETO_TABLA ->
            New_Pj = Pj#pjs{estado = ?SENTADO},
            New_State = State#state{registro=New_Pj},
            gen_server:call(erlmud_datos, {mover_entidad, [Pj#pjs.uuid, Asiento#entidad.parent, Asiento#entidad.uuid]}),
            gen_server:cast(erlmud_datos, {actualizar_jugador, New_Pj}),
            gen_server:cast(erlmud_servidor, {evento_pj, Pj#pjs.nombre_propio ++ " se sienta en " ++ utiles:nombre_con_articulo(indeterminado, Asiento#entidad.uuid)}),
            {"Te sientas en " ++ utiles:nombre_con_articulo(indeterminado, Asiento#entidad.uuid), New_State};
        _ ->
            {"Hmmm, no sé dónde te quieres sentar", State}
    end.

%%--------------------------------------------------------------------
%% @doc
%% Cambia el estado del PJ al estado normal.
%% @end
%%--------------------------------------------------------------------
levantar(State) ->
    Pj = State#state.registro,
    #entidad{parent=UUID_Parent} = hd(ets:lookup(entidades, Pj#pjs.uuid)),
    Parent = hd(ets:lookup(entidades, UUID_Parent)),
    New_Pj = Pj#pjs{estado = ?DE_PIE},
    New_State = State#state{registro=New_Pj},
    case Parent#entidad.tipo of
        ?OBJETO_TABLA ->
            Preter_Parent = hd(ets:lookup(entidades, Parent#entidad.parent)),
            gen_server:cast(erlmud_datos, {actualizar_jugador, New_Pj}),
            gen_server:call(erlmud_datos, {mover_entidad, [Pj#pjs.uuid, Parent#entidad.uuid, Preter_Parent#entidad.uuid]}),
            gen_server:cast(erlmud_servidor, {evento_pj, Pj#pjs.uuid, Pj#pjs.nombre_propio ++ " se pone en pie."}),
            {"Te levantas de " ++ utiles:nombre_con_articulo(determinado,Parent#entidad.uuid), New_State};
        _ ->
            gen_server:cast(erlmud_datos, {actualizar_jugador, New_Pj}),
            gen_server:cast(erlmud_servidor, {evento_pj, Pj#pjs.uuid, Pj#pjs.nombre_propio ++ " se pone en pie."}),
            {"Te pones en pie", New_State}
    end.

%%--------------------------------------------------------------------
%% @doc
%% Cambia el estado del PJ a tumbado
%% @end
%%--------------------------------------------------------------------
tumbar(State,[]) ->
    Pj = State#state.registro,
    New_Pj = Pj#pjs{estado = ?TUMBADO},
    New_State = State#state{registro=New_Pj},
    gen_server:cast(erlmud_datos, {actualizar_jugador, New_Pj}),
    gen_server:cast(erlmud_servidor, {evento_pj, Pj#pjs.uuid, Pj#pjs.nombre_propio ++ " se tumba en el suelo."}),
    {"Te echas en el suelo", New_State};
tumbar(State,[H]) ->
    Asiento = hd(ets:lookup(entidades,H)),
    Pj = State#state.registro,
    case Asiento#entidad.tipo of
        ?OBJETO_TABLA ->
            New_Pj = Pj#pjs{estado = ?TUMBADO},
            New_State = State#state{registro=New_Pj},
            gen_server:call(erlmud_datos, {Pj#pjs.uuid, Asiento#entidad.parent, Asiento#entidad.uuid}),
            gen_server:cast(erlmud_servidor, {evento_pj, Pj#pjs.nombre_propio ++ " se sienta en " ++ utiles:nombre_con_articulo(indeterminado, Asiento#entidad.uuid)}),
            {"Te sientas en " ++ utiles:nombre_con_articulo(indeterminado, Asiento#entidad.uuid), New_State};
        _ ->
            {"Hmmm, no sé dónde te quieres tumbar.", State}
    end.

%%--------------------------------------------------------------------
%% @doc
%% Cambia el estado del PJ a durmiendo. Durante el tiempo que esté en
%% este estado no puede realizar ninguna acción. Este estado se guarda
%% en el registro del proceso cliente. De esta forma el PJ conserva su
%% estado anterior a dormirse y la acción de «dormir» limita las
%% acciones que puede realizar el jugador en el bucle de llamadas TCP.
%% @end
%%--------------------------------------------------------------------
dormir(State) ->
    Pj = State#state.registro,
    Estado = Pj#pjs.estado,
    if
        Estado == ?DE_PIE orelse Estado == undefined ->
            {"No es buena idea dormir de pie.", State};
        true ->
            New_Pj = Pj#pjs{estado=durmiendo},
            New_State = State#state{registro=New_Pj},
            gen_server:cast(self(), {dormir, 150000}),
            {"Te duermes tranquilamente...", New_State}
    end.

%%--------------------------------------------------------------------
%% @doc
%% Marca como abierto un camino. Apoyándose en las funciones de
%% erlmud_datos que modifican la tabla de caminos.
%% @end
%%--------------------------------------------------------------------
abrir_paso(UUID_Cierre) ->
    Cierre = hd(ets:lookup(cierres, UUID_Cierre)),
    case Cierre#cierre.estado of
        ?LLAVE_CERRADO ->
            "Hmmm, necesitas una llave para abrir.";
        ?LLAVE_ABIERTO ->
            Nuevo_Cierre = Cierre#cierre{estado=?ABIERTO},
            gen_server:cast(erlmud_datos, {actualizar_cierre, Nuevo_Cierre}),
            "";
        ?CERRADO ->
            Nuevo_Cierre = Cierre#cierre{estado=?ABIERTO},
            gen_server:cast(erlmud_datos, {actualizar_cierre, Nuevo_Cierre}),
            "";
        _ ->
            "Hmmm, parece que ya estaba abierto."
    end.

abrir_paso(UUID_Pj,UUID_Cierre,Objetos)->
    Cierre = hd(ets:lookup(cierres, UUID_Cierre)),
    case Objetos of
        [H] ->
            #entidad{tipo=Tipo_objeto} = hd(ets:lookup(entidades,H)),
            Esta_Inventariado = utiles:esta_en_inventario(UUID_Pj,H),
            case Tipo_objeto of
                ?OBJETO_LUGAR ->
                    %% En este caso llega sólo el objeto enlazado por el Cierre
                    Mensaje = abrir_paso(UUID_Cierre),
                    if Mensaje == "" ->
                            Respuesta = io_lib:format("Abres ~s.",[utiles:nombre_con_articulo(determinado,H)]);
                       true ->
                            Respuesta = Mensaje
                    end,
                    Respuesta;
                ?OBJETO when Cierre#cierre.llave == H andalso
                             Cierre#cierre.estado == ?LLAVE_CERRADO andalso
                             Esta_Inventariado == true ->
                    %% Es la llave, está en el inventario y el cierre está cerrado
                    Nuevo_Cierre = Cierre#cierre{estado=?LLAVE_ABIERTO},
                    gen_server:cast(erlmud_datos,{actualizar_cierre,Nuevo_Cierre}),
                    "";
                ?OBJETO when Cierre#cierre.llave == H andalso
                             (Cierre#cierre.estado == ?LLAVE_ABIERTO orelse
                              Cierre#cierre.estado == ?ABIERTO) ->
                    "Hmmm, parece que ya estaba abierto.";
                _ ->
                    "Hmmm, no entiendo qué quieres abrir. ¿Tienes la llave?"
            end;
        [H,T] ->
            %% Llegan dos objetos uno debe ser el Objeto Lugar enlazado y otro la llave
            H_en_Inventario = utiles:esta_en_inventario(UUID_Pj,H),
            T_en_Inventario = utiles:esta_en_inventario(UUID_Pj,T),
            case Cierre#cierre.llave of
                H when Cierre#cierre.estado == ?LLAVE_CERRADO andalso
                       H_en_Inventario == true ->
                    Nuevo_Cierre = Cierre#cierre{estado=?LLAVE_ABIERTO},
                    gen_server:cast(erlmud_datos,{actualizar_cierre,Nuevo_Cierre}),
                    "";
                H when Cierre#cierre.estado == ?LLAVE_CERRADO andalso
                       H_en_Inventario == false ->
                    "Hmmm, deberías tener la llave antes.";
                H ->
                    "Hmmm, parece que ya estaba abierto.";
                T when Cierre#cierre.estado == ?LLAVE_CERRADO andalso
                       T_en_Inventario == true ->
                    Nuevo_Cierre = Cierre#cierre{estado=?LLAVE_ABIERTO},
                    gen_server:cast(erlmud_datos,{actualizar_cierre,Nuevo_Cierre}),
                    "";
                T when Cierre#cierre.estado == ?LLAVE_CERRADO andalso
                       T_en_Inventario == false ->
                    "Hmmm, deberías tener la llave antes.";
                T ->
                    "Hmmm, parece que ya estaba abierto.";
                _ ->
                    "Hmmm, no entiendo qué quieres abrir. ¿Tienes la llave?"
            end
    end.

%%--------------------------------------------------------------------
%% @doc
%% Marca como abierto un objeto de tipo contenedor y envía los mensajes
%% de aviso de apertura y modifica los datos. Si se pasan dos objetos
%% entiende que uno es el contenedor y otro la llave.
%% @end
%%--------------------------------------------------------------------
abrir_objeto(State,[H,T]) ->
    Pj = State#state.registro,
    %% Llegan dos objetos. ¿Cuál es el contenedor y cuál la llave?
    #entidad{tipo=Tipo_H} = hd(ets:lookup(entidades,H)),
    #entidad{tipo=Tipo_T} = hd(ets:lookup(entidades,T)),
    case Tipo_H of
        ?OBJETO_CONTENEDOR ->
            %% Si el objeto contenedor es H, T debe ser llave
            Contenedor = hd(ets:lookup(objetos,H)),
            Llave = hd(ets:lookup(objetos,T));
        _ when Tipo_T == ?OBJETO_CONTENEDOR ->
            Contenedor = hd(ets:lookup(objetos,T)),
            Llave = hd(ets:lookup(objetos,H));
        _ ->
            %% Si no es un camino, ni hay contenedor, ¿qué quiere abrir?
            Contenedor = nil,
            Llave = nil
    end,
    if
        Contenedor /= nil ->
            %% ¿Es la llave correcta?
            {llave,UUID_Llave} = lists:keyfind(llave,1,Contenedor#obj.enlaces),
            case Contenedor#obj.estado of
                ?LLAVE_CERRADO when UUID_Llave == Llave#obj.uuid ->
                    New_Objeto = Contenedor#obj{estado=?LLAVE_ABIERTO},
                    Aviso = io_lib:format("~s ha abierto la cerradura de ~s",[Pj#pjs.nombre_propio,utiles:nombre_con_articulo(determinado,Contenedor#obj.uuid)]),
                    gen_server:cast(erlmud_servidor, {evento_pj,Pj#pjs.uuid,Aviso}),
                    gen_server:cast(erlmud_datos, {actualizar_objeto,New_Objeto}),
                    io_lib:format("Abres la cerradura de ~s con ~s", [utiles:nombre_con_articulo(determinado,Contenedor#obj.uuid),utiles:nombre_con_articulo(determinado,Llave#obj.uuid)]);
                ?LLAVE_CERRADO ->
                    "Hmmm, ¿seguro que esa es la llave?";
                _ ->
                    io_lib:format("Hmmm, parece que ~s ya estaba abiert~s", [utiles:nombre_con_articulo(determinado,Llave#obj.uuid),utiles:fin_genero(Llave#obj.uuid)])
            end;
        true ->
            "Hmmm, no sé qué quieres abrir."
    end;
abrir_objeto(State,[H]) ->
    Pj = State#state.registro,
    %% Comprobar primero qué tipo de entidad es
    #entidad{tipo=Tipo_Objeto} = hd(ets:lookup(entidades, H)),
    case Tipo_Objeto of
        ?OBJETO_CONTENEDOR ->
            Objeto = hd(ets:lookup(objetos,H)),
            case Objeto#obj.estado of
                ?CERRADO ->
                    New_Objeto = Objeto#obj{estado=?ABIERTO},
                    Aviso = io_lib:format("~s ha abierto ~s",[Pj#pjs.nombre_propio,utiles:nombre_con_articulo(determinado,H)]),
                    gen_server:cast(erlmud_servidor, {evento_pj,Pj#pjs.uuid,Aviso}),
                    gen_server:cast(erlmud_datos, {actualizar_objeto,New_Objeto}),
                    io_lib:format("Abres ~s", [utiles:nombre_con_articulo(determinado,H)]);
                ?LLAVE_ABIERTO ->
                    New_Objeto = Objeto#obj{estado=?ABIERTO},
                    Aviso = io_lib:format("~s ha abierto ~s",[Pj#pjs.nombre_propio,utiles:nombre_con_articulo(determinado,H)]),
                    gen_server:cast(erlmud_servidor, {evento_pj,Pj#pjs.uuid,Aviso}),
                    gen_server:cast(erlmud_datos, {actualizar_objeto,New_Objeto}),
                    io_lib:format("Abres ~s", [utiles:nombre_con_articulo(determinado,H)]);
                ?ABIERTO ->
                    io_lib:format("Hmmm, parece que ~s ya estaba abiert~s",[utiles:nombre_con_articulo(determinado,H),utiles:fin_genero(H)]);
                ?LLAVE_CERRADO ->
                    "Hmmm, está cerrado con llave.";
                _ ->
                    "Hmmm, no sé qué quieres abrir."
            end;
        _ ->
            "Hmmm, aún no he implementado que se pueda abrir eso."
    end;
abrir_objeto(_State,[]) ->
    "Hmmm, no comprendo qué quieres abrir.".

%%--------------------------------------------------------------------
%% @doc
%% Esta función marca un objeto o una ruta como abierta para hacer
%% accesible el contenido o el paso.
%%
%% Se apoya en dos funciones externas abrir_objeto/2 y abrir_paso/2,3.
%% @end
%%--------------------------------------------------------------------
abrir(State, Cadenas) ->
    Pj = State#state.registro,
    #entidad{parent=UUID_Parent} = hd(ets:lookup(entidades,Pj#pjs.uuid)),
    Cadenas_Limpias = [X || X <- Cadenas, X /= "abrir"],
    Objetos_Scope = utiles:objetos_accesibles(UUID_Parent) ++
        utiles:objetos_accesibles(Pj#pjs.uuid),
    Objetos = utiles:cadenas_a_UUIDs(Objetos_Scope,Cadenas_Limpias),
    #entidad{tipo=Tipo_Parent} = hd(ets:lookup(entidades,UUID_Parent)),
    case Tipo_Parent of
        ?LUGAR when Cadenas_Limpias /= [] ->
            Parent = hd(ets:lookup(lugares,UUID_Parent)),
            Salidas = Parent#lugar.enlaces,
            Caminos = utiles:salidas_por_nombre(Salidas,hd(Cadenas_Limpias));
        ?OBJETO_LUGAR when Cadenas_Limpias /= [] ->
            Parent = hd(ets:lookup(objetos,UUID_Parent)),
            Salidas = Parent#obj.enlaces,
            Caminos = utiles:salidas_por_nombre(Salidas,hd(Cadenas_Limpias));
        _ ->
            Caminos = []
    end,
    case Caminos of
        [] ->
            %% No ha encontrado un camino apropiado, comprobar los objetos
            Mensaje = abrir_objeto(State,Objetos),
            {Mensaje,State};
        [H] when length(Objetos) > 0 ->
            %% Se ha encontrado un camino practicable y hay objetos relacionados
            {_,Nombres,_,_,Cierre} = H,
            Mensaje = abrir_paso(Pj#pjs.uuid,Cierre,Objetos),
            Respuesta =
                if Mensaje == "" ->
                        io_lib:format("Abres la cerradura de ~s", [hd(Nombres)]);
                   true ->
                        Mensaje
                end,
            gen_server:cast(erlmud_servidor, {evento_pj, Pj#pjs.uuid,io_lib:format("~s ha abierto ~s.", [Pj#pjs.nombre_propio,hd(Nombres)])}),
            {Respuesta,State};
        [H] ->
            %% Se ha encontrado un camino y no hay objetos relacionados
            {_,Nombres,_,_,Cierre} = H,
            Mensaje = abrir_paso(Cierre),
            Respuesta =
                if Mensaje == "" ->
                        io_lib:format("Abres ~s", [hd(Nombres)]);
                   true ->
                        Mensaje
                end,
            gen_server:cast(erlmud_servidor, {evento_pj, Pj#pjs.uuid,io_lib:format("~s ha abierto ~s.", [Pj#pjs.nombre_propio,hd(Nombres)])}),
            {Respuesta,State};
        _ ->
            {"Hmmm, no comprendo qué quieres abrir.", State}
    end.

%%--------------------------------------------------------------------
%% @doc
%% Marca como cerrado un camino. Apoyándose en las funciones de
%% erlmud_datos que modifican la tabla de caminos.
%% @end
%%--------------------------------------------------------------------
cerrar_paso(UUID_Cierre) ->
    Cierre = hd(ets:lookup(cierres, UUID_Cierre)),
    case Cierre#cierre.estado of
        ?ABIERTO ->
            Nuevo_Cierre = Cierre#cierre{estado=?CERRADO},
            gen_server:cast(erlmud_datos, {actualizar_cierre, Nuevo_Cierre}),
            "";
        ?CERRADO ->
            "Hmmm, parece que ya estaba cerrado.";
        _ ->
            "Hmmm, parece que ya estaba cerrado."
    end.

cerrar_paso(UUID_Pj,UUID_Cierre,Objetos)->
    Cierre = hd(ets:lookup(cierres, UUID_Cierre)),
    case Objetos of
        [H] ->
            #entidad{tipo=Tipo_objeto} = hd(ets:lookup(entidades,H)),
            Esta_Inventariado = utiles:esta_en_inventario(UUID_Pj,H),
            case Tipo_objeto of
                ?OBJETO_LUGAR ->
                    %% En este caso llega sólo el objeto enlazado por el Cierre
                    Mensaje = cerrar_paso(UUID_Cierre),
                    if Mensaje == "" ->
                            Respuesta = io_lib:format("Cierras ~s.",[utiles:nombre_con_articulo(determinado,H)]);
                       true ->
                            Respuesta = Mensaje
                    end,
                    Respuesta;
                ?OBJETO when Cierre#cierre.llave == H andalso
                             Cierre#cierre.estado == ?LLAVE_CERRADO ->
                    "Hmmm, parece que ya estaba cerrado.";
                ?OBJETO when Cierre#cierre.llave == H andalso
                             Esta_Inventariado == true andalso
                             Cierre#cierre.estado /= ?LLAVE_CERRADO ->
                    %% Es la llave, está en el inventario y el cierre está abierto
                    Nuevo_Cierre = Cierre#cierre{estado=?LLAVE_CERRADO},
                    gen_server:cast(erlmud_datos,{actualizar_cierre,Nuevo_Cierre}),
                    "";
                _ ->
                    "Hmmm, no entiendo qué quieres cerrar. ¿Tienes la llave?"
            end;
        [H,T] ->
            %% Llegan dos objetos uno debe ser el Objeto Lugar enlazado y otro la llave
            case Cierre#cierre.llave of
                H when Cierre#cierre.estado == ?LLAVE_ABIERTO orelse
                       Cierre#cierre.estado == ?ABIERTO ->
                    Nuevo_Cierre = Cierre#cierre{estado=?LLAVE_CERRADO},
                    gen_server:cast(erlmud_datos,{actualizar_cierre,Nuevo_Cierre}),
                    "";
                H ->
                    "Hmmm, parece que ya estaba cerrado.";
                T when Cierre#cierre.estado == ?LLAVE_ABIERTO orelse
                       Cierre#cierre.estado == ?ABIERTO ->
                    Nuevo_Cierre = Cierre#cierre{estado=?LLAVE_CERRADO},
                    gen_server:cast(erlmud_datos,{actualizar_cierre,Nuevo_Cierre}),
                    "";
                T ->
                    "Hmmm, parece que ya estaba cerrado.";
                _ ->
                    "Hmmm, no entiendo qué quieres cerrar. ¿Tienes la llave?"
            end
    end.

%%--------------------------------------------------------------------
%% @doc
%% Marca como cerrado un objeto de tipo contenedor y envía los mensajes
%% de aviso de cierre y modifica los datos. Si se pasan dos objetos
%% entiende que uno es el contenedor y otro la llave.
%% @end
%%--------------------------------------------------------------------
cerrar_objeto(State,[H,T]) ->
    Pj = State#state.registro,
    %% Llegan dos objetos. ¿Cuál es el contenedor y cuál la llave?
    #entidad{tipo=Tipo_H} = hd(ets:lookup(entidades,H)),
    #entidad{tipo=Tipo_T} = hd(ets:lookup(entidades,T)),
    case Tipo_H of
        ?OBJETO_CONTENEDOR ->
            %% Si el objeto contenedor es H, T debe ser llave
            Contenedor = hd(ets:lookup(objetos,H)),
            Llave = hd(ets:lookup(objetos,T));
        _ when Tipo_T == ?OBJETO_CONTENEDOR ->
            Contenedor = hd(ets:lookup(objetos,T)),
            Llave = hd(ets:lookup(objetos,H));
        _ ->
            %% Si no es un camino, ni hay contenedor, ¿qué quiere cerrar?
            Contenedor = nil,
            Llave = nil
    end,
    if
        Contenedor /= nil ->
            %% ¿Es la llave correcta?
            {llave,UUID_Llave} = lists:keyfind(llave,1,Contenedor#obj.enlaces),
            case Contenedor#obj.estado of
                ?LLAVE_ABIERTO when UUID_Llave == Llave#obj.uuid ->
                    New_Objeto = Contenedor#obj{estado=?LLAVE_CERRADO},
                    Aviso = io_lib:format("~s ha cerradp la cerradura de ~s",[Pj#pjs.nombre_propio,utiles:nombre_con_articulo(determinado,Contenedor#obj.uuid)]),
                    gen_server:cast(erlmud_servidor, {evento_pj,Pj#pjs.uuid,Aviso}),
                    gen_server:cast(erlmud_datos, {actualizar_objeto,New_Objeto}),
                    io_lib:format("Cierras la cerradura de ~s con ~s", [utiles:nombre_con_articulo(determinado,Contenedor#obj.uuid),utiles:nombre_con_articulo(determinado,Llave#obj.uuid)]);
                ?ABIERTO when UUID_Llave == Llave#obj.uuid ->
                    New_Objeto = Contenedor#obj{estado=?LLAVE_CERRADO},
                    Aviso = io_lib:format("~s ha cerradp la cerradura de ~s",[Pj#pjs.nombre_propio,utiles:nombre_con_articulo(determinado,Contenedor#obj.uuid)]),
                    gen_server:cast(erlmud_servidor, {evento_pj,Pj#pjs.uuid,Aviso}),
                    gen_server:cast(erlmud_datos, {actualizar_objeto,New_Objeto}),
                    io_lib:format("Cierras la cerradura de ~s con ~s", [utiles:nombre_con_articulo(determinado,Contenedor#obj.uuid),utiles:nombre_con_articulo(determinado,Llave#obj.uuid)]);
                ?LLAVE_ABIERTO ->
                    "Hmmm, ¿seguro que esa es la llave?";
                ?ABIERTO ->
                    "Hmmm, ¿seguro que esa es la llave?";
                _ ->
                    io_lib:format("Hmmm, parece que ~s ya estaba cerrad~s", [utiles:nombre_con_articulo(determinado,Llave#obj.uuid),utiles:fin_genero(Llave#obj.uuid)])
            end;
        true ->
            "Hmmm, no sé qué quieres cerrar."
    end;
cerrar_objeto(State,[H]) ->
    Pj = State#state.registro,
    %% Comprobar primero qué tipo de entidad es
    #entidad{tipo=Tipo_Objeto} = hd(ets:lookup(entidades, H)),
    case Tipo_Objeto of
        ?OBJETO_CONTENEDOR ->
            Objeto = hd(ets:lookup(objetos,H)),
            case Objeto#obj.estado of
                ?ABIERTO ->
                    New_Objeto = Objeto#obj{estado=?CERRADO},
                    Aviso = io_lib:format("~s ha cerrado ~s",[Pj#pjs.nombre_propio,utiles:nombre_con_articulo(determinado,H)]),
                    gen_server:cast(erlmud_servidor, {evento_pj,Pj#pjs.uuid,Aviso}),
                    gen_server:cast(erlmud_datos, {actualizar_objeto,New_Objeto}),
                    io_lib:format("Cierras ~s", [utiles:nombre_con_articulo(determinado,H)]);
                ?CERRADO ->
                    io_lib:format("Hmmm, parece que ~s ya estaba cerrad~s",[utiles:nombre_con_articulo(determinado,H),utiles:fin_genero(H)]);
                _ ->
                    "Hmmm, no sé qué quieres cerrar."
            end;
        _ ->
            "Hmmm, aún no he implementado que se pueda cerrar eso."
    end;
cerrar_objeto(_State,[]) ->
    "Hmmm, no comprendo qué quieres cerrar.".

%%--------------------------------------------------------------------
%% @doc
%% Esta función marca un objeto o una ruta como cerrada para hacer
%% impedir el paso o el acceso al contenido, según el caso.
%%
%% Se apoya en las funciones cerrar_objeto/2 y cerrar_paso/1,3
%% @end
%%--------------------------------------------------------------------
cerrar(State, Cadenas) ->
    Pj = State#state.registro,
    #entidad{parent=UUID_Parent} = hd(ets:lookup(entidades,Pj#pjs.uuid)),
    Cadenas_Limpias = [X || X <- Cadenas, X /= "cerrar"],
    Objetos_Scope = utiles:objetos_accesibles(UUID_Parent) ++
        utiles:objetos_accesibles(Pj#pjs.uuid),
    Objetos = utiles:cadenas_a_UUIDs(Objetos_Scope, Cadenas_Limpias),
    #entidad{tipo=Tipo_Parent} = hd(ets:lookup(entidades,UUID_Parent)),
    case Tipo_Parent of
        ?LUGAR when Cadenas_Limpias /= [] ->
            Parent = hd(ets:lookup(lugares, UUID_Parent)),
            Salidas = Parent#lugar.enlaces,
            Caminos = utiles:salidas_por_nombre(Salidas,hd(Cadenas_Limpias));
        ?OBJETO_LUGAR when Cadenas_Limpias /= [] ->
            Parent = hd(ets:lookup(objetos,UUID_Parent)),
            Salidas = Parent#obj.enlaces,
            Caminos = utiles:salidas_por_nombre(Salidas,hd(Cadenas_Limpias));
        _ ->
            Caminos = []
    end,
    case Caminos of
        [] ->
            %% No ha encontrado un camino apropiado, comprobar los objetos
            Mensaje = cerrar_objeto(State,Objetos),
            {Mensaje,State};
        [H] when length(Objetos) > 0 ->
            %% Se ha encontrado un camino practicable y hay objetos relacionados
            {_,Nombres,_,_,Cierre} = H,
            Mensaje = cerrar_paso(Pj#pjs.uuid,Cierre,Objetos),
            Respuesta =
                if Mensaje == "" ->
                        io_lib:format("Cierras la cerradura de ~s", [hd(Nombres)]);
                   true ->
                        Mensaje
                end,
            gen_server:cast(erlmud_servidor,{evento_pj,Pj#pjs.uuid,io_lib:format("~s ha cerrado la cerradura de ~s.",[Pj#pjs.nombre_propio,hd(Nombres)])}),
            {Respuesta,State};
        [H] ->
            {_, Nombres,_,_,Cierre} = H,
            Mensaje = cerrar_paso(Cierre),
            Respuesta =
                if Mensaje == "" ->
                        io_lib:format("Cierras ~s", [hd(Nombres)]);
                   true ->
                        Mensaje
                end,
            gen_server:cast(erlmud_servidor, {evento_pj, Pj#pjs.uuid,io_lib:format("~s ha cerrado ~s.", [Pj#pjs.nombre_propio,hd(Nombres)])}),
            {Respuesta,State};
        _ ->
            {"Hmmm, no comprendo qué quieres cerrar.",State}
    end.

%%--------------------------------------------------------------------
%% @doc
%% Cierra la conexión del cliente con el mundo.
%% @end
%%--------------------------------------------------------------------
abandonar(State) ->
    terminate(normal, State),
    Mensaje = "Viajas hacia las etéreas salas de la Nada, abandonando este mundo.",
    escribir_mensaje(Mensaje,State#state{estado=saliendo}),
    gen_tcp:close(State#state.socket),
    {Mensaje, State}.

%%--------------------------------------------------------------------
%% @doc
%% Traspasa un Objeto a otro jugador. Se comprueba, cuál de los Objetos
%% que llegan a esta función es el «personaje» y cuál es el Objeto,
%% de esta manera puede atender comandos como «dar objeto a personaje»
%% o «dar a personaje objeto»
%% @end
%%--------------------------------------------------------------------
dar(State, [H]) ->
    Objeto = hd(ets:lookup(entidades, H)),
    case Objeto#entidad.tipo of
        ?PERSONAJE ->
            {"Hmmm, no sé qué quieres dar a " ++ Objeto#entidad.nombre,State};
        _ ->
            {"Hmmm, no sé a quién quieres dar " ++ utiles:nombre_con_articulo(indeterminado, H),State}
    end;
dar(State, [H,T]) ->
    Pj = State#state.registro,
    %% Llegan dos UUIDs una es del objeto y otra del personaje.
    Personaje = utiles:devolver_personaje_significativo([H,T]),
    O = utiles:devolver_objeto_significativo([H,T]),
    Objeto = hd(ets:lookup(entidades,O#obj.uuid)),
    Datos_Movimiento = [Objeto#entidad.uuid, Objeto#entidad.parent, Personaje#pjs.uuid],
    if
        Objeto#entidad.parent /= Pj#pjs.uuid ->
            Mensaje = Pj#pjs.nombre_propio ++ " coge "
                ++ utiles:nombre_con_articulo(indeterminado, Objeto#entidad.uuid)
                ++ " y se " ++ utiles:acusativo(Objeto#entidad.uuid)
                ++ " da a " ++ Personaje#pjs.nombre_propio,
            gen_server:call(erlmud_datos, {mover_entidad, Datos_Movimiento}),
            gen_server:cast(erlmud_servidor, {evento_pj, Pj#pjs.uuid, Mensaje}),
            {"Das " ++ utiles:nombre_con_articulo(indeterminado, Objeto#entidad.uuid) ++ " a " ++ Personaje#pjs.nombre_propio,State};
        true ->
            gen_server:call(erlmud_datos, {mover_entidad, Datos_Movimiento}),
            gen_server:cast(erlmud_servidor, {evento_dar,Pj#pjs.uuid,Personaje#pjs.uuid,Objeto#entidad.uuid}),
            {"Das " ++ utiles:nombre_con_articulo(indeterminado, Objeto#entidad.uuid) ++ " a " ++ Personaje#pjs.nombre_propio,State}
    end;
dar(State, _) ->
    {"Hmmm, no comprendo qué quieres dar y a quién",State}.

%%--------------------------------------------------------------------
%% @doc
%% Esta función consulta la fecha del MUD. El servidor devuelve un valor
%% {Año,Mes,Día}.
%% @end
%%--------------------------------------------------------------------
fecha(State) ->
    {Anio, Mes, Dia, Semana} = gen_server:call(erlmud_sol, dime_fecha),
    Dia_Semana = utiles:nombre_dia_semana(Semana),
    Respuesta = io_lib:format("Hoy es ~s, ~p del ~p del ~p.", [Dia_Semana,Dia,Mes,Anio]),
    {Respuesta,State}.

%%--------------------------------------------------------------------
%% @doc
%% Esta función consulta la hora del MUD. El servidor devuelve un valor
%% {Hora,Minuto}.
%% @end
%%--------------------------------------------------------------------
hora(State) ->
    Reply = gen_server:call(erlmud_sol, dime_hora),
    {Hora, Minuto} = Reply,
    Respuesta =
        if
            Hora == 1 ->
                io_lib:format("Es la ~p y ~p minutos.", [Hora,Minuto]);
            true ->
                io_lib:format("Son las ~p y ~p minutos.", [Hora,Minuto])
        end,
    {Respuesta, State}.

%%--------------------------------------------------------------------
%% @doc
%% Llama a la correspondiente función de la librería para realizar la
%% acción de encendido.
%% @end
%%--------------------------------------------------------------------
encender(State, Objetos) ->
    Mensaje = utiles:encender(State#state.registro, Objetos),
    {Mensaje, State}.

%%--------------------------------------------------------------------
%% @doc
%% Llama a la correspondiente función de la librería para realizar la
%% acción de apagado.
%% @end
%%--------------------------------------------------------------------
apagar(State, Objetos) ->
    Mensaje = utiles:apagar(State#state.registro, Objetos),
    {Mensaje, State}.

%%--------------------------------------------------------------------
%% @doc
%% Esta función recibe las cadena introducidas por el jugador y espera
%% encontrar «formato» como primera cadena y como otra cadena «si» o
%% «no», para activar o desactivar el formateo de cadenas en consola.
%% @end
%%--------------------------------------------------------------------
conf_formato(State, [H,T]) ->
    Pj = State#state.registro,
    case H of
        "formato" when T == "si" ->
            Mensaje = "Activado el formateo de texto.",
            New_Pj = Pj#pjs{formato=true},
            New_State = State#state{registro=New_Pj},
            gen_server:cast(erlmud_datos, {actualizar_jugador,New_Pj}),
            {Mensaje,New_State};
        "formato" when T == "no" ->
            Mensaje = "Desactivado el formateo de texto.",
            New_Pj = Pj#pjs{formato=false},
            New_State = State#state{registro=New_Pj},
            gen_server:cast(erlmud_datos, {actualizar_jugador,New_Pj}),
            {Mensaje,New_State};
        _ ->
            {"Hmmm, no sé muy bien qué quieres hacer.", State}
    end;
conf_formato(State,[_]) ->
    {"Hmmm, no sé muy bien qué quieres hacer.", State}.

%%--------------------------------------------------------------------
%% @doc
%% Dado un objeto V se comprueba que es vestuario y que está en el
%% inventario del jugador. Si es así y no está marcado el estado como
%% vestido, lo modifica vistiéndolo.
%% @end
%%--------------------------------------------------------------------
vestir(State,[V]) ->
    Objeto = hd(ets:lookup(objetos,V)),
    Pj = State#state.registro,
    En_Inventario = utiles:esta_en_inventario(Pj#pjs.uuid,V),
    case Objeto#obj.tipo of
        ?OBJETO_VESTIBLE when
              En_Inventario == true andalso
              Objeto#obj.estado == ?NO_VESTIDO ->
            gen_server:cast(erlmud_datos, {actualizar_objeto,Objeto#obj{estado=?VESTIDO}}),
            {"Te pones " ++ utiles:nombre_con_articulo(determinado,V) ++ ".",State};
        ?OBJETO_VESTIBLE when En_Inventario == false ->
            Desc = io_lib:format("Hmmm, primero deberías tener ~s.", [utiles:nombre_con_articulo(indeterminado,V)]),
            {Desc,State};
        ?OBJETO_VESTIBLE when
              En_Inventario == true andalso
              Objeto#obj.estado == ?VESTIDO ->
            Desc = io_lib:format("Hmmm, ya llevas puest~s ~s.", [utiles:fin_genero(V),utiles:nombre_con_articulo(determinado,V)]),
            {Desc,State};
        _ ->
            {"Hmmm, no puedes ponerte eso.",State}
    end;
vestir(State,[]) ->
    {"Hmmm, no sé qué quieres ponerte.",State}.

%%--------------------------------------------------------------------
%% @doc
%% Dado un objeto V se comprueba que es vestuario y que está marcado
%% el estado como vestido, si es así lo modifica para desvestirlo.
%% @end
%%--------------------------------------------------------------------
desvestir(State,[V]) ->
    Objeto = hd(ets:lookup(objetos,V)),
    case Objeto#obj.tipo of
        ?OBJETO_VESTIBLE when Objeto#obj.estado == ?VESTIDO ->
            gen_server:cast(erlmud_datos, {actualizar_objeto,Objeto#obj{estado=?NO_VESTIDO}}),
            {"Te quitas " ++ utiles:nombre_con_articulo(determinado,V) ++ ".",State};
        ?OBJETO_VESTIBLE when Objeto#obj.estado == ?NO_VESTIDO ->
            {"Hmmm, antes de quitárte algo deberías tenerlo puesto.",State};
        _ ->
            {"Hmmm, no sé qué quieres quitarte.",State}
    end;
desvestir(State,[]) ->
    {"Hmmm, no sé qué quieres quitarte.",State}.

%%--------------------------------------------------------------------
%% @doc
%% Esta función recibe la cadena introducida por el cliente para
%% analizarla. Devuelve siempre una cadena sin salto de línea al final.
%% @end
%%--------------------------------------------------------------------
analizar_comando([], State) ->
    {"",State};
analizar_comando(Comando,State) ->
    Pj = State#state.registro,
    #entidad{parent=UUID_Parent} = hd(ets:lookup(entidades,Pj#pjs.uuid)),
    Minusculas = utiles:quitar_tildes(string:lowercase(Comando)),
    Tokens = string:tokens(Minusculas, " "),
    Tokens_Limpios = [X || X <- Tokens, X /= "el", X /= "la", X/="con", X/="de", X/="del"],
    %% Comprobar si el comando tiene un sinónimo.
    Verbo = maps:get(hd(Tokens_Limpios), ?SINONIMOS, hd(Tokens_Limpios)),
    %% Tener en cuenta los objetos del lugar y los del jugador
    Objetos_Scope = utiles:objetos_accesibles(UUID_Parent) ++
        utiles:objetos_accesibles(Pj#pjs.uuid),
    Objetos = utiles:cadenas_a_UUIDs(Objetos_Scope, tl(Tokens_Limpios)),
    case Verbo of
        "xyzzy" ->
            io_lib:format("Conexiones: ~p", [ets:tab2list(sockets)]);
        "hora" ->
            hora(State);
        "fecha" ->
            fecha(State);
        "mirar" ->
            mirar(State, Objetos);
        "mirarme" ->
            mirar(State, [Pj#pjs.uuid]);
        "salidas" ->
            {utiles:escribir_salidas(Pj, UUID_Parent),State};
        "coger" ->
            Todo = lists:member("todo", Tokens_Limpios),
            case Todo of
                true when Objetos == [] ->
                    coger_todo(State);
                _ ->
                    coger(State, Objetos)
            end;
        "dejar" ->
            dejar(State, Objetos);
        "inventario" ->
            {utiles:escribir_inventario(Pj),State};
        "ir" ->
            ir(State, Tokens_Limpios);
        "decir" ->
            decir(State, Comando);
        "gritar" ->
            gritar(State, Comando);
        "sentar" ->
            sentar(State,Objetos);
        "levantar" ->
            levantar(State);
        "tumbar" ->
            tumbar(State,Objetos);
        "dormir" ->
            dormir(State);
        "abrir" ->
            abrir(State, Tokens_Limpios);
        "cerrar" ->
            cerrar(State, Tokens_Limpios);
        "abandonar" ->
            abandonar(State);
        "dar" ->
            dar(State, Objetos);
        "encender" ->
            encender(State, Objetos);
        "apagar" ->
            apagar(State, Objetos);
        "formato" ->
            conf_formato(State, Tokens_Limpios);
        "vestir" ->
            vestir(State, Objetos);
        "desvestir" ->
            desvestir(State, Objetos);
        _ ->
            {"Hmmm, no comprendo qué quieres hacer.", State}
    end.
