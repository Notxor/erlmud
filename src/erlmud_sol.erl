%%%-------------------------------------------------------------------
%%% @author Notxor <notxor@nueva-actitud.org>
%%% @copyright (C) 2020, Notxor
%%% @doc
%%% Proceso para la gestión del tiempo.
%%% @end
%%% Created : 17 Sep 2020 by Notxor <notxor@nueva-actitud.org>
%%%-------------------------------------------------------------------
-module(erlmud_sol).

-behaviour(gen_server).

%% API
-export([start_link/0]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3, format_status/2]).

-define(SERVER, ?MODULE).

-record(state, {
                 minuto = 0
               , hora = 0
               , dia = 1
               , mes = 1
               , ano = 111
               , era = 111
               , dia_semana = 0
               }).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%% @end
%%--------------------------------------------------------------------
%% -spec start_link() -> {ok, Pid :: pid()} |
%%           {error, Error :: {already_started, pid()}} |
%%           {error, Error :: term()} |
%%           ignore.
start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%% @end
%%--------------------------------------------------------------------
%% -spec init(Args :: term()) -> {ok, State :: term()} |
%%           {ok, State :: term(), Timeout :: timeout()} |
%%           {ok, State :: term(), hibernate} |
%%           {stop, Reason :: term()} |
%%           ignore.
init([]) ->
    process_flag(trap_exit, true),
    State = #state{},
    timer:send_after(15000, un_minuto),   % 15 segundos equivalen a un minuto
    {ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%% @end
%%--------------------------------------------------------------------
%% -spec handle_call(Request :: term(), From :: {pid(), term()}, State :: term()) ->
%%           {reply, Reply :: term(), NewState :: term()} |
%%           {reply, Reply :: term(), NewState :: term(), Timeout :: timeout()} |
%%           {reply, Reply :: term(), NewState :: term(), hibernate} |
%%           {noreply, NewState :: term()} |
%%           {noreply, NewState :: term(), Timeout :: timeout()} |
%%           {noreply, NewState :: term(), hibernate} |
%%           {stop, Reason :: term(), Reply :: term(), NewState :: term()} |
%%           {stop, Reason :: term(), NewState :: term()}.
handle_call(dime_hora, _From, State) ->
    Reply = {State#state.hora, State#state.minuto},
    {reply, Reply, State};
handle_call(dime_fecha, _From, State) ->
    Reply = {State#state.ano,State#state.mes,State#state.dia,State#state.dia_semana},
    {reply, Reply, State};
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%% @end
%%--------------------------------------------------------------------
%% -spec handle_cast(Request :: term(), State :: term()) ->
%%           {noreply, NewState :: term()} |
%%           {noreply, NewState :: term(), Timeout :: timeout()} |
%%           {noreply, NewState :: term(), hibernate} |
%%           {stop, Reason :: term(), NewState :: term()}.
handle_cast(_Request, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%% @end
%%--------------------------------------------------------------------
%% -spec handle_info(Info :: timeout() | term(), State :: term()) ->
%%           {noreply, NewState :: term()} |
%%           {noreply, NewState :: term(), Timeout :: timeout()} |
%%           {noreply, NewState :: term(), hibernate} |
%%           {stop, Reason :: normal | term(), NewState :: term()}.
handle_info(un_minuto, State) ->
    New_State = sumar_minuto(State),
    {noreply, New_State};
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%% @end
%%--------------------------------------------------------------------
%% -spec terminate(Reason :: normal | shutdown | {shutdown, term()} | term(),
%%                 State :: term()) -> any().
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%% @end
%%--------------------------------------------------------------------
%% -spec code_change(OldVsn :: term() | {down, term()},
%%                   State :: term(),
%%                   Extra :: term()) -> {ok, NewState :: term()} |
%%           {error, Reason :: term()}.
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called for changing the form and appearance
%% of gen_server status when it is returned from sys:get_status/1,2
%% or when it appears in termination error logs.
%% @end
%%--------------------------------------------------------------------
%% -spec format_status(Opt :: normal | terminate,
%%                     Status :: list()) -> Status :: term().
format_status(_Opt, Status) ->
    Status.

%%%===================================================================
%%% Internal functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Serie de funciones que se encadenan para establecer los tiempos
%% correctos empezando por el minuto hasta llegar a la /Era/.
%% @end
%%--------------------------------------------------------------------
sumar_era(State) ->
    %% ¿Estará funcionando 889 años del juego el servidor? Eso son
    %% mas de 222 años de funcionamiento real... pero el cambio de era
    %% estar: está.
    Era = State#state.era + 1,
    State#state{era=Era}.

sumar_ano(State) ->
    Ano = State#state.ano + 1,
    New_State =
        if
            Ano == 1001 ->
                Estado = State#state{ano=0},
                sumar_era(Estado);
            true ->
                State#state{ano=Ano}
        end,
    New_State.

sumar_mes(State) ->
    Mes = State#state.mes + 1,
    New_State =
        if
            Mes == 14 ->
                %% el mes 13 son los cinco días de ajuste del calendario.
                Estado = State#state{mes=1},
                sumar_ano(Estado);
            true ->
                State#state{mes=Mes}
        end,
    New_State.

sumar_dia_semana(State) ->
    Dia = State#state.dia_semana + 1,
    New_State =
        case Dia of
            7 ->
                State#state{dia_semana=0};
            _ ->
                State#state{dia_semana=Dia}
        end,
    New_State.

sumar_dia(State) ->
    Dia = State#state.dia + 1,
    New_State =
        case Dia of
            6 when State#state.mes == 13 ->
                %% Estamos en los cinco días de ajuste del año y hay que pasar mes también
                %% y todos los años comienzan en domingo (Taera).
                Estado = State#state{dia=1, dia_semana=0},
                sumar_mes(Estado);
            31 ->
                Estado = sumar_dia_semana(State#state{dia=1}),
                sumar_mes(Estado);
            true ->
                sumar_dia_semana(State#state{dia=Dia})
        end,
    New_State.

sumar_hora(State) ->
    Hora = State#state.hora + 1,
    New_State =
        if
            Hora == 24 ->
                Estado = State#state{hora=0},
                sumar_dia(Estado);
            true ->
                State#state{hora=Hora}
        end,
    %% Comprueba los eventos de amanecer y anochecer.
    if
        Hora == 7 ->
            %% Enviar mensaje de que amanece
            gen_server:cast(erlmud_servidor, {evento_global, "El sol comienza a asomarse a los cielos: amanece en Tero."});
        Hora == 20 ->
            %% Enviar mensaje de que anochece
            gen_server:cast(erlmud_servidor, {evento_global, "El sol vuelve a sus cuarteles nocturnos: anochece en Tero."});
        true ->
            ok
    end,
    New_State.

sumar_minuto(State) ->
    Minuto = State#state.minuto + 1,
    New_State =
        if
            Minuto == 60 ->
                Estado = State#state{minuto=0},
                sumar_hora(Estado);
            true ->
                State#state{minuto=Minuto}
        end,
    timer:send_after(15000, un_minuto),    % Iniciar el contador de otro minuto.
    New_State.
