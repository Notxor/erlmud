%%%-------------------------------------------------------------------
%%% @author Notxor <notxor@nueva-actitud.org>
%%% @copyright (C) 2020, Notxor
%%% @doc
%%% Supervisor de más alto nivel del proyecto
%%% @end
%%% Created :  6 Jul 2020 by Notxor <notxor@nueva-actitud.org>
%%%-------------------------------------------------------------------
-module(erlmud_supersup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the supervisor
%% @end
%%--------------------------------------------------------------------
%% -spec start_link() -> {ok, Pid :: pid()} |
%%           {error, {already_started, Pid :: pid()}} |
%%           {error, {shutdown, term()}} |
%%           {error, term()} |
%%           ignore.
start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Whenever a supervisor is started using supervisor:start_link/[2,3],
%% this function is called by the new process to find out about
%% restart strategy, maximum restart intensity, and child
%% specifications.
%% @end
%%--------------------------------------------------------------------
%% -spec init(Args :: term()) ->
%%           {ok, {SupFlags :: supervisor:sup_flags(),
%%                 [ChildSpec :: supervisor:child_spec()]}} |
%%           ignore.
init([]) ->
    SupFlags = #{strategy => one_for_one,
                 intensity => 1,
                 period => 5},

    Datos    = #{id => erlmud_datos,
                 start => {erlmud_datos, start_link, []},
                 restart => permanent,
                 shutdown => 5000,
                 modules => [erlmud_datos]},

    Servidor = #{id => erlmud_servidor,
                 start => {erlmud_servidor, start_link, []},
                 restart => permanent,
                 shutdown => 5000,
                 type => worker,
                 modules => [erlmud_servidor]},

    Superv   = #{id => erlmud_sup,
                 start => {erlmud_sup, start_link, []},
                 restart => permanent,
                 shutdown => 5000,
                 type => supervisor,
                 modules => [erlmud_sup]},

    Cosmosup = #{id => erlmud_cosmosup,
                 start => {erlmud_cosmosup, start_link, []},
                 restart => permanent,
                 shutdown => 5000,
                 type => supervisor,
                 modules => [erlmud_cosmosup]},

    {ok, {SupFlags, [Datos, Servidor, Cosmosup, Superv]}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
