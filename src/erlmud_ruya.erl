%%%-------------------------------------------------------------------
%%% @author Notxor <notxor@nueva-actitud.org>
%%% @copyright (C) 2020, Notxor
%%% @doc
%%% Proceso para la gestión del tiempo.
%%% @end
%%% Created : 17 Sep 2020 by Notxor <notxor@nueva-actitud.org>
%%%-------------------------------------------------------------------
-module(erlmud_ruya).

-behaviour(gen_server).

%% API
-export([start_link/0]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3, format_status/2]).

-define(SERVER, ?MODULE).

-record(state, {
                 minuto = 0
               , hora = 0
               , dia = 1
               , fase = llena
               }).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%% @end
%%--------------------------------------------------------------------
%% -spec start_link() -> {ok, Pid :: pid()} |
%%           {error, Error :: {already_started, pid()}} |
%%           {error, Error :: term()} |
%%           ignore.
start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%% @end
%%--------------------------------------------------------------------
%% -spec init(Args :: term()) -> {ok, State :: term()} |
%%           {ok, State :: term(), Timeout :: timeout()} |
%%           {ok, State :: term(), hibernate} |
%%           {stop, Reason :: term()} |
%%           ignore.
init([]) ->
    process_flag(trap_exit, true),
    State = #state{},
    timer:send_after(13000, un_minuto),   % 14 segundos equivalen a un minuto lunar
    {ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%% @end
%%--------------------------------------------------------------------
%% -spec handle_call(Request :: term(), From :: {pid(), term()}, State :: term()) ->
%%           {reply, Reply :: term(), NewState :: term()} |
%%           {reply, Reply :: term(), NewState :: term(), Timeout :: timeout()} |
%%           {reply, Reply :: term(), NewState :: term(), hibernate} |
%%           {noreply, NewState :: term()} |
%%           {noreply, NewState :: term(), Timeout :: timeout()} |
%%           {noreply, NewState :: term(), hibernate} |
%%           {stop, Reason :: term(), Reply :: term(), NewState :: term()} |
%%           {stop, Reason :: term(), NewState :: term()}.
handle_call({dime_fase}, _From, State) ->
    {reply, State#state.fase, State};
handle_call({esta_visible}, _From,State) ->
    Reply =
        if
            (State#state.hora =< 6 andalso State#state.hora >= 18)
            orelse State#state.fase == nueva ->
                false;
            true ->
                true
        end,
    {reply, Reply, State};
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%% @end
%%--------------------------------------------------------------------
%% -spec handle_cast(Request :: term(), State :: term()) ->
%%           {noreply, NewState :: term()} |
%%           {noreply, NewState :: term(), Timeout :: timeout()} |
%%           {noreply, NewState :: term(), hibernate} |
%%           {stop, Reason :: term(), NewState :: term()}.
handle_cast(_Request, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%% @end
%%--------------------------------------------------------------------
%% -spec handle_info(Info :: timeout() | term(), State :: term()) ->
%%           {noreply, NewState :: term()} |
%%           {noreply, NewState :: term(), Timeout :: timeout()} |
%%           {noreply, NewState :: term(), hibernate} |
%%           {stop, Reason :: normal | term(), NewState :: term()}.
handle_info(un_minuto, State) ->
    New_State = sumar_minuto(State),
    {noreply, New_State};
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%% @end
%%--------------------------------------------------------------------
%% -spec terminate(Reason :: normal | shutdown | {shutdown, term()} | term(),
%%                 State :: term()) -> any().
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%% @end
%%--------------------------------------------------------------------
%% -spec code_change(OldVsn :: term() | {down, term()},
%%                   State :: term(),
%%                   Extra :: term()) -> {ok, NewState :: term()} |
%%           {error, Reason :: term()}.
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called for changing the form and appearance
%% of gen_server status when it is returned from sys:get_status/1,2
%% or when it appears in termination error logs.
%% @end
%%--------------------------------------------------------------------
%% -spec format_status(Opt :: normal | terminate,
%%                     Status :: list()) -> Status :: term().
format_status(_Opt, Status) ->
    Status.

%%%===================================================================
%%% Internal functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Serie de funciones que se encadenan para establecer los tiempos
%% correctos empezando por el minuto hasta llegar a la fase de la
%% luna.
%% @end
%%--------------------------------------------------------------------
cambiar_fase(State) ->
    case State#state.fase of
        nueva ->
            State#state{fase=creciente};
        creciente ->
            State#state{fase=cuarto_creciente};
        cuarto_creciente ->
            State#state{fase=gibosa_creciente};
        gibosa_creciente ->
            State#state{fase=llena};
        llena ->
            State#state{fase=gibosa_menguante};
        gibosa_menguante ->
            State#state{fase=cuarto_menguante};
        cuarto_menguante ->
            State#state{fase=menguante};
        menguante ->
            State#state{fase=nueva};
        _ ->
            State#state{fase=nueva}
    end.

sumar_dia(State) ->
    Dia = State#state.dia + 1,
    New_State =
        case Dia of
            29 ->
                %% Cambiar a luna nueva
                Estado = State#state{dia=1},
                cambiar_fase(Estado);
            4 ->
                %% Cambiar a luna creciente
                Estado = State#state{dia=Dia},
                cambiar_fase(Estado);
            8 ->
                %% Cambiar a cuarto creciente
                Estado = State#state{dia=Dia},
                cambiar_fase(Estado);
            12 ->
                %% Cambiar a gibosa creciente
                Estado = State#state{dia=Dia},
                cambiar_fase(Estado);
            15 ->
                %% Cambiar a luna llena
                Estado = State#state{dia=Dia},
                cambiar_fase(Estado);
            18 ->
                %% Cambiar a gibosa menguante
                Estado = State#state{dia=Dia},
                cambiar_fase(Estado);
            22 ->
                %% Cambiar a cuarto menguante
                Estado = State#state{dia=Dia},
                cambiar_fase(Estado);
            25 ->
                %% Cambiar a menguante
                Estado = State#state{dia=Dia},
                cambiar_fase(Estado);
            _ ->
                State#state{dia=Dia}
        end,
    New_State.

sumar_hora(State) ->
    Hora = State#state.hora + 1,
    New_State =
        if Hora == 24 ->
                Estado = State#state{hora=0},  % Comienza la cuenta de horas de nuevo
                sumar_dia(Estado);
           true ->
                State#state{hora=Hora}
        end,
    case Hora of
            6 ->
                %% Enviar mensaje de que la luna se esconde
                gen_server:cast(erlmud_servidor, {evento_global, "Ruya, la luna roja de los orcos se esconde tras el horizonte."});
            18 ->
                %% Enviar mensaje de que la luna sale
                gen_server:cast(erlmud_servidor, {evento_global, "Ruya, la luna roja de los orcos comienza a asomarse en el horizonte."});
            _ ->
                ok
        end,
    New_State.

sumar_minuto(State) ->
    Minuto = State#state.minuto + 1,
    Estado =
        if
            Minuto == 60 ->
                New_State = State#state{minuto=0},
                sumar_hora(New_State);
            true ->
                State#state{minuto=Minuto}
        end,
    timer:send_after(13000, un_minuto),     % Comenzar otro minuto lunar
    Estado.
