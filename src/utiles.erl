-module(utiles).

-export([objetos_accesibles/1
       , cadenas_a_UUIDs/2
       , uuids_a_nombres/1
       , formato/2
       , centrar/1
       , formatear/2
       , nombre_con_articulo/2
       , acusativo/1
       , quitar_tildes/1
       , salidas_por_nombre/2
       , fin_genero/1
       , hacer_contracciones/1
       , dado/1
       , descripcion_raza/2
       , distribucion_valores/5
       , obtener_entidad_parent/1
       , obtener_nombre_descripcion_lugar/2
       , obtener_nombre_salidas/2
       , obtener_descripcion_lugar/2
       , obtener_objetos_contenidos/1
       , situar_cursor_inicio_linea/0
       , obtener_estado_cierre/1
       , esta_en_inventario/2
       , obtener_id_zona/1
       , nombre_dia_semana/1
       , obtener_indumentaria/1
       , obtener_descripcion_vestido/1
       , obtener_descripcion_objeto/3
       , separar_objetos_ropa/1
       , limpiar_entrada/1
       , escribir_salidas/2
       , escribir_inventario/1
       , encender/2
       , apagar/2
       , contar_elementos/1
       , devolver_entidad_significativa/1
       , devolver_objeto_significativo/1
       , devolver_personaje_significativo/1]).

-include("include/registros.hrl").

-define(ANCHO_TEXTO, 70).

%%--------------------------------------------------------------------
%% @doc
%% Devuelve una lista de los UUIDs de los objetos que son visibles o
%% accesibles desde UUID_Lugar, entendiendo que son parte de su
%% inventario.  Si lo que se pretende es obtener una lista de objetos
%% accesibles para un PJ, hay que proporcionar como inicio el UUID de
%% su #entidad.parent
%% @end
%%--------------------------------------------------------------------
objetos_accesibles(UUID) ->
    Objeto = hd(ets:lookup(entidades, UUID)),
    Lista = [Objeto#entidad.uuid] ++ lists:map(
        fun(UUID_Child) ->
                #entidad{tipo=Tipo,estado=Se_Ve} = hd(ets:lookup(entidades, UUID_Child)),
                if
                    Se_Ve == ?VISIBLE ->
                        case Tipo of
                            ?OBJETO_TABLA ->
                                objetos_accesibles(UUID_Child);
                            ?OBJETO_CONTENEDOR ->
                                %% Si es un contenedor comprobamos el estado
                                #obj{estado=Estado} = hd(ets:lookup(objetos, UUID_Child)),
                                if
                                    Estado == ?ABIERTO ->
                                        objetos_accesibles(UUID_Child);
                                    true ->
                                        UUID_Child
                                end;
                            _ ->
                                UUID_Child
                        end;
                    true ->
                        []
                end
        end,
        Objeto#entidad.children),
    lists:flatten(Lista).

%%--------------------------------------------------------------------
%% @doc
%% Devuelve una lista de salidas cuyo Nombre está en la lista de nombres
%% de la salida, por si hay varias.
%% @end
%%--------------------------------------------------------------------
salidas_por_nombre(Salidas, Nombre) ->
    lists:filtermap(
      fun(S) ->
              {_,Nombres,_,_,_} = S,
              lists:member(Nombre, Nombres)
      end, Salidas).

%%--------------------------------------------------------------------
%% @doc
%% Analiza la lista de Cadenas buscando si cada cadena corresponde a
%% una determinada entidad del mundo. Si encuentra que un nombre entre
%% los objetos accesibles, Objetos_Scope, coincide con una cadena
%% añade su UUID a la lista.
%% @end
%%--------------------------------------------------------------------
cadenas_a_UUIDs(Objetos_Scope, Cadenas) ->
    cadenas_a_UUIDs(Objetos_Scope, [], Cadenas).
cadenas_a_UUIDs(Objetos, UUIDs, Cadenas) ->
    case Cadenas of
        [] ->
            UUIDs;
        [H|T] ->
            Nombre_Buscado = string:lowercase(H),
            UUIDs_Encontrados = UUIDs ++ lists:filtermap(
                fun(E) ->
                        #entidad{nombre=Nombre,sinonimos=Sinonimos} = hd(ets:lookup(entidades, E)),
                        %% Quitar los artículos o desarticulando el nombre.
                        Nombre_Minus = [X || X <-
                                                 string:tokens(string:lowercase(Nombre), " "),
                                                 X /= "el",
                                                 X /= "la"],
                        lists:member(Nombre_Buscado, Nombre_Minus) orelse
                            lists:member(Nombre_Buscado,Sinonimos)
                end,
                Objetos),
            % Iteramos en todas las cadenas de la cola (Tail -> T).
            cadenas_a_UUIDs(Objetos, UUIDs_Encontrados, T)
    end.

%%--------------------------------------------------------------------
%% @doc
%% Recibe una lista de UUIDs y devuelve una lista de nombres que
%% corresponden a esos UUIDs. La lista se ordena alfabéticamente.
%% @end
%%--------------------------------------------------------------------
uuids_a_nombres(UUIDs) ->
    Nombres = lists:map(
                fun(E) ->
                        #entidad{tipo=Tipo} = hd(ets:lookup(entidades, E)),
                        case Tipo of
                            ?PERSONAJE ->
                                #pjs{nombre_propio=Nombre} = hd(ets:lookup(pjs, E)),
                                Nombre;
                            _ ->
                                #entidad{nombre=Nombre} = hd(ets:lookup(entidades,E)),
                                Nombre
                        end
                end,
                UUIDs),
    lists:sort(Nombres).

%%--------------------------------------------------------------------
%% @doc
%% Recibe el Tipo de artículo: determinado | indeterminado y genera una
%% salida adecuando el Tipo y el género con el nombre de la entidad
%% referenciada por UUID.
%% @end
%%--------------------------------------------------------------------
nombre_con_articulo(Tipo, UUID) ->
    #entidad{tipo=Tipo_Entidad} = hd(ets:lookup(entidades, UUID)),
    if
        Tipo_Entidad == ?LUGAR ->
            Numero = 1,
            #lugar{gg=Genero, nombre_propio=Nombre} = hd(ets:lookup(lugares,UUID));
        Tipo_Entidad == ?PERSONAJE ->
            Numero = 1,
            #pjs{nombre_propio=Nombre} = hd(ets:lookup(pjs, UUID)),
            Genero = ?NEUTRO;
        true ->
            #obj{gg=Genero, numero=Numero, nombre_propio=Nombre} = hd(ets:lookup(objetos, UUID))
    end,
    case Tipo of
        determinado ->
            case Genero of
                ?MASCULINO when
                      Numero == 1 ->
                    "el " ++ Nombre;
                ?MASCULINO when
                      Numero > 1 ->
                    "los ";
                ?FEMENINO when
                      Numero == 1 ->
                    "la " ++ Nombre;
                ?FEMENINO when
                      Numero > 1 ->
                    "las " ++ Nombre;
                _ ->
                    Nombre
            end;
        indeterminado ->
            case Genero of
                ?MASCULINO when
                      Numero == 1 ->
                    "un " ++ Nombre;
                ?MASCULINO when
                      Numero > 1 ->
                    "unos " ++ Nombre;
                ?FEMENINO when
                      Numero == 1 ->
                    "una " ++ Nombre;
                ?FEMENINO when
                      Numero > 1 ->
                    "unas " ++ Nombre;
                _ ->
                    Nombre
            end
    end.

%%--------------------------------------------------------------------
%% @doc
%% Devuelve el «acusativo» equivalente a un objeto dado según su género,
%% de esta manera se pueden formar frases como «personaje coge objeto
%% y se /lo/ da a personaje».
%% @end
%%--------------------------------------------------------------------
acusativo(UUID) ->
    #entidad{tipo=Tipo} = hd(ets:lookup(entidades, UUID)),
    case Tipo of
        ?PERSONAJE ->
            #pjs{gg=Genero} = hd(ets:lookup(personajes,UUID));
        _ ->
            #obj{gg=Genero} = hd(ets:lookup(objetos,UUID))
    end,
    case Genero of
        ?MASCULINO ->
            "lo";
        ?FEMENINO ->
            "la"
    end.

%%--------------------------------------------------------------------
%% @doc
%% Devuelve la terminación de una palabra según el género.
%%
%% Este es el caso general en que los masculinos acaban en $o y los
%% femeninos en $a. Cuando aparezcan las excepciones veremos cómo se
%% pueden gestionar.
%% @end
%%--------------------------------------------------------------------
fin_genero(UUID) ->
    #entidad{tipo=Tipo} = hd(ets:lookup(entidades,UUID)),
    case Tipo of
        ?PERSONAJE ->
            #pjs{gg=Genero} = hd(ets:lookup(pjs,UUID));
        _ ->
            #obj{gg=Genero} = hd(ets:lookup(objetos,UUID))
    end,
    case Genero of
        ?MASCULINO ->
            "o";
        ?FEMENINO ->
            "a"
    end.

%%--------------------------------------------------------------------
%% @doc
%% Recibe una cadena y le pone el código de negrita para consola delante
%% detrás coloca el código de texto normal para delimitar la salida.
%% @end
%%--------------------------------------------------------------------
formato(Formato, Cadena) ->
    case Formato of
        negrita ->
            "\e[1m" ++ Cadena ++ "\e[0m";
        cursiva ->
            "\e[3m" ++ Cadena ++ "\e[0m";
        amarillo ->
            "\e[93m" ++ Cadena ++ "\e[0m";
        verde ->
            "\e[92m" ++ Cadena ++ "\e[0m";
        rojo ->
            "\e[91m" ++ Cadena ++ "\e[0m";
        _ ->
            Cadena
    end.

%%--------------------------------------------------------------------
%% @doc
%% Devuelve la cadena de escape que mueve el cursor 70 posiciones a la
%% izquierda, si las hay o se queda en la primera columna de la linea.
%% @end
%%--------------------------------------------------------------------
situar_cursor_inicio_linea() ->
    "\e[70D".

%%--------------------------------------------------------------------
%% @doc
%% Devuelve una Cadena centrada en el ancho del texto.
%% @end
%%--------------------------------------------------------------------
centrar(Cadena) ->
    string:pad(Cadena, ?ANCHO_TEXTO, both).

%%--------------------------------------------------------------------
%% @doc
%% Recoge una cadena e inicia un bucle para, palabra por palabra,
%% analizar el largo de la línea para ajustarla al ancho de texto
%% establecido. ajustar_parrafos/1 se apoya en la función auxiliar
%% ajustar_parrafos/3 para arrastar el contador del ancho y la cadena
%% generada con el formato.
%% @end
%%--------------------------------------------------------------------
ajustar_parrafos(Cadena) ->
    Tokens = string:tokens(Cadena, "\n\r\t "),
    ajustar_parrafos(Tokens, 0, "").

ajustar_parrafos(Palabras, Contador, Cadena) ->
    case Palabras of
        [] ->
            Cadena;
        [H|T] ->
            Longitud = Contador + length(H) + 1,
            if
                Longitud < ?ANCHO_TEXTO ->
                    New_Cadena = Cadena ++ H ++ " ",
                    ajustar_parrafos(T, Longitud, New_Cadena);
                true ->
                    New_Cadena = Cadena ++ "\n",
                    ajustar_parrafos([H|T], 0, New_Cadena)
            end
    end.

%%--------------------------------------------------------------------
%% @doc
%% Formatea una cadena para ajustarla al ancho establecido utilizando
%% las funciones ajustar_parrafos/1 y ajustar_parrafos/3.
%%
%% Antes de enviar la cadena al análisis marca el santo de párrafo con
%% un carácter |. Después revierte el proceso para mantener los
%% párrafos.
%% @end
%%--------------------------------------------------------------------
formatear(true, Cadena) ->
    Parrafos = string:replace(Cadena, "\n\n", "|", all),
    Respuesta = lists:map(fun ajustar_parrafos/1, Parrafos),
    string:replace(Respuesta, " | ", "\n\n", all);
formatear(false, Cadena) ->
    Parrafos = string:replace(Cadena, "\n\n", "|", all),
    %% Convertimos los párrafos en listas de palabras
    Tokens = lists:map(fun(X) ->
                               string:tokens(X, "\n\r\t ")
                       end,
                       Parrafos),
    %% Juntamos las palabras de los párrafos en una sola lista de palabras
    Parrafos_juntos = string:join(Tokens, ""),
    %% Convertimos la lista de palabras en una cadena con espacios.
    Respuesta = string:join(Parrafos_juntos, " "),
    %% Rehacemos el formato de párrafos por el marcador «|»
    string:replace(Respuesta, " | ", "\n\n", all).

%%--------------------------------------------------------------------
%% @doc
%% Realiza las contracciones de artículos «del» y «al». Está pensado
%% para que modifique las apariciones generadas por el texto automático
%% las demás debería controlarlas el jugador y por eso no se esperan
%% signos de puntuación alrededor de «de el» o «a el».
%% @end
%%--------------------------------------------------------------------
hacer_contracciones(Cadena) ->
    Cadena_del = string:replace(Cadena, " de el ", " del ", all),
    Cadena_al = string:replace(Cadena_del, " a el ", " al " , all),
    Cadena_al.

%%--------------------------------------------------------------------
%% @doc
%% Elimina las tildes de una Cadena. Tiene en cuenta sólo las minúsculas
%% porque está pensada para eliminarlas de los comandos introducidos
%% por el jugador.
%% @end
%%--------------------------------------------------------------------
quitar_tildes(Cadena) ->
    lists:map(
      fun(X) ->
              case X of
                  $á -> $a;
                  $é -> $e;
                  $í -> $i;
                  $ó -> $o;
                  $ú -> $u;
                  _ -> X
              end
      end,
      Cadena).

%%--------------------------------------------------------------------
%% @doc
%% Devuelve un número pseudo-aleatorio Maximo >= X >= 1
%% @end
%%--------------------------------------------------------------------
dado(Maximo) ->
    rand:uniform(Maximo).

%%--------------------------------------------------------------------
%% @doc
%% Devuelve una cadena con una descripción legible de las Razas que
%% llegan como parámetro.
%% @end
%%--------------------------------------------------------------------
descripcion_raza(Formato,Razas) ->
    Cadenas = lists:map(
                fun({R,P}) ->
                        Raza = maps:get(R, ?RAZAS),
                        if
                            Raza == "inmortal" andalso
                            Formato == true ->
                                formato(amarillo, Raza);
                            Raza == "importal" andalso
                            Formato == false ->
                                Raza;
                            true ->
                                maps:get(R, ?RAZAS) ++ io_lib:format(" (~p%)", [P])
                        end
                end,
                Razas),
    case Cadenas of
        [] when Formato == true ->
            formato(negrita,"Raza ") ++ "no conocida";
        [H] when Formato == true ->
            formato(negrita,"Raza: ") ++ H;
        [] when Formato == false ->
            "Raza no conocida";
        [H] when Formato == false ->
            "Raza " ++ H;
        _ when Formato == true ->
            formato(negrita, "Raza: ") ++ string:join(Cadenas, ", ");
        _ ->
            "Raza: " ++ string:join(Cadenas, ", ")
    end.

%%--------------------------------------------------------------------
%% @doc
%% Para dos distribuciones de valores y dado un Valor que se
%% distribuye de V_Min a V_Max devuelve el valor correspondiente
%% distribuido entre Min y Max.
%%
%% Lo redondea a entero para utilización en características.
%% @end
%%--------------------------------------------------------------------
distribucion_valores(Valor, V_Min, V_Max, Min, Max) ->
    round(Min + (Max - Min) * (Valor - V_Min) / (V_Max - V_Min)).

%%--------------------------------------------------------------------
%% @doc
%% Dado el UUID de un personaje devuelve su entidad parent correcta
%% teniendo en cuenta que puede estar sentado o tumbado en otro objeto
%% y por tanto su parent real será el parent de su parent.
%%
%% Parece un trabalenguas, pero es lo que hace.
%% @end
%%--------------------------------------------------------------------
obtener_entidad_parent(UUID) ->
    #entidad{parent=P} = hd(ets:lookup(entidades,UUID)),
    %% Hay que tener en cuenta que el personaje puede estar sentado en un asiento
    %% o tumbado en algún lugar y por tanto su Parent sería el parent de ese
    %% elemento sobre el que está.
    #entidad{tipo=Tipo_Parent} = hd(ets:lookup(entidades,P)),
    case Tipo_Parent of
        ?OBJETO_TABLA ->
            #entidad{parent=UUID_PP} = hd(ets:lookup(entidades,P)),
            hd(ets:lookup(entidades,UUID_PP));
        _ ->
            hd(ets:lookup(entidades,P))
    end.

%%--------------------------------------------------------------------
%% @doc
%% Dado el UUID de un lugar devuelve el nombre y la descripción
%% teniendo en cuenta que el lugar puede ser un OBJETO_LUGAR
%% @end
%%--------------------------------------------------------------------
obtener_nombre_descripcion_lugar(Hay_Luz, UUID) ->
    Entidad_Lugar = hd(ets:lookup(entidades, UUID)),
    case Entidad_Lugar#entidad.tipo of
        ?OBJETO_LUGAR when Hay_Luz == false ->
            Lugar = hd(ets:lookup(objetos,UUID)),
            Nombre = Lugar#obj.nombre_propio,
            Desc = "En esta oscuridad no puedes ver nada.",
            {Nombre, Desc};
        ?OBJETO_LUGAR ->
            Lugar = hd(ets:lookup(objetos,UUID)),
            Value = lists:keysearch(dentro,1,Lugar#obj.desc),
            Desc =
                case Value of
                    {value, Tuple} ->
                        %% Obtener descripción del interior
                        {dentro, Descripcion} = Tuple,
                        Descripcion;
                    false ->
                        Lugar#obj.desc;
                    _ ->
                        nombre_con_articulo(indeterminado,Lugar#obj.uuid)
                end,
            Nombre = Lugar#obj.nombre_propio,
            {Nombre, Desc};
        ?LUGAR when Hay_Luz == false ->
            Lugar = hd(ets:lookup(lugares,UUID)),
            Nombre = Lugar#lugar.nombre_propio,
            Desc = "En esta oscuridad no puedes ver nada.",
            {Nombre, Desc};
        ?LUGAR ->
            Lugar = hd(ets:lookup(lugares,UUID)),
            Desc = Lugar#lugar.desc,
            Nombre = Lugar#lugar.nombre_propio,
            {Nombre, Desc};
        _ ->
            {Entidad_Lugar#entidad.nombre, ""}
    end.

%%--------------------------------------------------------------------
%% @doc
%% Esta función devuelve una cadena dependiendo del tipo de objeto que
%% se le pase en la entidad Obj_Dir y si el usuario tiene activado o
%% no el /formateo/ de texto.
%% @end
%%--------------------------------------------------------------------
obtener_descripcion_objeto(Formato, ?OBJETO, Obj_Dir) ->
    Objeto = hd(ets:lookup(objetos,Obj_Dir#entidad.uuid)),
    case Formato of
        true ->
            utiles:centrar(utiles:formato(negrita,Objeto#obj.nombre_propio))
                ++ "\n" ++ utiles:formatear(Formato, Objeto#obj.desc);
        false ->
            Objeto#obj.nombre_propio ++ "\n" ++ utiles:formatear(Formato, Objeto#obj.desc)
    end;
obtener_descripcion_objeto(Formato,?PERSONAJE, Obj_Dir) ->
    Personaje = hd(ets:lookup(pjs, Obj_Dir#entidad.uuid)),
    #entidad{tipo=Tipo_Parent} = hd(ets:lookup(entidades,Obj_Dir#entidad.parent)),
    Estado =
        case Personaje#pjs.estado of
            ?SENTADO ->
                case Tipo_Parent of
                    ?OBJETO_TABLA ->
                        "Sentad" ++ utiles:fin_genero(Personaje#pjs.uuid) ++
                            " en " ++ utiles:nombre_con_articulo(determinado,Obj_Dir#entidad.parent) ++ "\n";
                    _ ->
                        "Sentad" ++ utiles:fin_genero(Personaje#pjs.uuid) ++ " en el suelo.\n"
                end;
            ?TUMBADO ->
                case Tipo_Parent of
                    ?OBJETO_TABLA ->
                        "Tumbad" ++ utiles:fin_genero(Personaje#pjs.uuid) ++
                            " en " ++ utiles:nombre_con_articulo(determinado, Obj_Dir#entidad.parent) ++ "\n";
                    _ ->
                        "Tumbad" ++ utiles:fin_genero(Personaje#pjs.uuid) ++ " en el suelo.\n"
                end;
            _ ->
                ""
        end,
    Vestir = utiles:obtener_indumentaria(Personaje#pjs.uuid),
    case Formato of
        true ->
            utiles:centrar(utiles:formato(negrita,Personaje#pjs.nombre_propio))
                ++ "\n" ++ utiles:formatear(Formato, Personaje#pjs.desc) ++ "\n"
                ++ Estado ++ "\n"
                ++ utiles:formato(negrita, "Atuendo: ")
                ++ utiles:formatear(Formato, Vestir)
                ++ "\n" ++ utiles:descripcion_raza(Formato,Personaje#pjs.raza) ++ "\n"
                ++ io_lib:format("Estatura: ~pcms -- ", [Personaje#pjs.altura])
                ++ io_lib:format("Peso: ~pkg", [Personaje#pjs.peso]);
        false ->
            Personaje#pjs.nombre_propio ++ "\n"
                ++ utiles:formatear(Formato, Personaje#pjs.desc) ++ "\n"
                ++ Estado ++ "\n"
                ++ "Atuendo: " ++ Vestir
                ++ "\n" ++ utiles:descripcion_raza(Formato,Personaje#pjs.raza) ++ "\n"
                ++ io_lib:format("Estatura: ~pcms -- ", [Personaje#pjs.altura])
                ++ io_lib:format("Peso: ~pkg", [Personaje#pjs.peso])
    end;
obtener_descripcion_objeto(Formato, ?OBJETO_CONTENEDOR, Obj_Dir) ->
    Objeto = hd(ets:lookup(objetos,Obj_Dir#entidad.uuid)),
    Children = Obj_Dir#entidad.children,
    case Formato of
        true when Objeto#obj.estado == ?ABIERTO andalso
                  Children /= [] ->
            utiles:centrar(utiles:formato(negrita,Objeto#obj.nombre_propio))
                ++ "\n" ++ utiles:formatear(Formato,Objeto#obj.desc) ++
                "\n" ++ utiles:formato(negrita, "Dentro hay: ") ++
                lists:join(", ", [utiles:nombre_con_articulo(indeterminado,X) ||
                                     X <- Children]);
        false when Objeto#obj.estado == ?ABIERTO andalso
                   Children /= [] ->
            Objeto#obj.nombre_propio
                ++ "\n" ++ utiles:formatear(Formato,Objeto#obj.desc) ++
                "\n" ++ "Dentro hay: " ++
                lists:join(", ", [utiles:nombre_con_articulo(indeterminado,X) ||
                                     X <- Children]);
        _ ->
            Objeto#obj.nombre_propio ++ "\n" ++ utiles:formatear(Formato,Objeto#obj.desc)
    end;
obtener_descripcion_objeto(Formato,?OBJETO_TABLA, Obj_Dir) ->
    Objeto = hd(ets:lookup(objetos,Obj_Dir#entidad.uuid)),
    Children = Obj_Dir#entidad.children,
    case Formato of
        true when Children /= [] ->
            utiles:centrar(utiles:formato(negrita, Objeto#obj.nombre_propio))
                ++ "\n" ++ utiles:formatear(Formato,Objeto#obj.desc) ++
                "\n" ++ utiles:formato(negrita, "Sobre " ++ utiles:nombre_con_articulo(determinado, Obj_Dir#entidad.uuid) ++ " hay: ") ++
                lists:join(", ", [utiles:nombre_con_articulo(indeterminado,X) ||
                                         X <- Children]);
        true when Children == [] ->
            utiles:centrar(utiles:formato(negrita, Objeto#obj.nombre_propio))
                ++ "\n" ++ utiles:formatear(Formato,Objeto#obj.desc);
        false when Children /= []->
            Objeto#obj.nombre_propio ++ "\n\n" ++
                utiles:formatear(Formato,Objeto#obj.desc) ++ "\n" ++
                "Sobre " ++ utiles:nombre_con_articulo(determinado, Objeto#obj.uuid) ++ " hay: " ++
                lists:join(", ", [utiles:nombre_con_articulo(indeterminado,X) ||
                                     X <- Children]);
        false when Children == [] ->
            Objeto#obj.nombre_propio ++ "\n\n" ++
                utiles:formatear(Formato,Objeto#obj.desc) ++ "\n";
        _ -> ""
    end;
obtener_descripcion_objeto(_Formato, ?OBJETO_RELOJ, Obj_Dir) ->
    Objeto = hd(ets:lookup(objetos,Obj_Dir#entidad.uuid)),
    {Hora, Minuto} = gen_server:call(erlmud_sol, dime_hora),
    {Horas, Minutos} =
        case Objeto#obj.estado of
            {parado,H,M} ->
                {H,M};
            {adelanta,H,M} ->
                sumar_hora({Hora,Minuto}, {H,M});
            {atrasa,H,M} ->
                restar_hora({Hora,Minuto},{H,M});
            _ ->
                {Hora,Minuto}
        end,
    Respuesta = io_lib:format("Marca las ~p horas y ~p minutos.", [Horas,Minutos]),
    Objeto#obj.desc ++ "\n\n" ++ Respuesta;
obtener_descripcion_objeto(_Formato, ?OBJETO_CALENDARIO, Obj_Dir) ->
    Objeto = hd(ets:lookup(objetos,Obj_Dir#entidad.uuid)),
    {Anio, Mes, Dia, Semana} = gen_server:call(erlmud_sol, dime_fecha),
    Dia_Semana = utiles:nombre_dia_semana(Semana),
    Respuesta = io_lib:format("Hoy es ~s, ~p del ~p de ~p.", [Dia_Semana,Dia,Mes,Anio]),
    Objeto#obj.desc ++ "\n\n" ++ Respuesta;
obtener_descripcion_objeto(Formato, ?OBJETO_LUGAR, Obj_Dir) ->
    Objeto = hd(ets:lookup(objetos,Obj_Dir#entidad.uuid)),
    Value = lists:keysearch(fuera,1,Objeto#obj.desc),
    Desc =
        case Value of
            {value, Tuple} ->
                %% Obtener descripción del objeto desde fuera
                {fuera, Descripcion} = Tuple,
                Descripcion;
            false ->
                Objeto#obj.desc;
            _ ->
                utiles:nombre_con_articulo(indeterminado,Objeto#obj.uuid)
        end,
    case Formato of
        true ->
            utiles:centrar(utiles:formato(negrita,Objeto#obj.nombre))
                ++ "\n" ++ utiles:formatear(Formato,Desc);
        false ->
            Objeto#obj.nombre ++ "\n" ++ utiles:formatear(Formato,Desc)
    end;
obtener_descripcion_objeto(_Formato, ?OBJETO_LUZ, Obj_Dir) ->
    Objeto = hd(ets:lookup(objetos,Obj_Dir#entidad.uuid)),
    if Objeto#obj.estado == ?OFF ->
            "Apagad" ++ utiles:fin_genero(Objeto#obj.uuid) ++ ". ";
       true ->
            "Encendid" ++ utiles:fin_genero(Objeto#obj.uuid) ++ ". "
    end
        ++ Objeto#obj.desc;
obtener_descripcion_objeto(Formato, ?OBJETO_VESTIBLE, Obj_Dir) ->
    Desc = utiles:obtener_descripcion_vestido(Obj_Dir#entidad.uuid),
    utiles:formatear(Formato,Desc);
obtener_descripcion_objeto(_,_,_) ->
    "".

%%--------------------------------------------------------------------
%% @doc
%% Suma dos horas en formato {Hora,Minuto} devolviendo el resultado
%% en el mismo formato.
%% @end
%%--------------------------------------------------------------------
sumar_hora(H1, H2) ->
    {Hora1,Minuto1} = H1,
    {Hora2,Minuto2} = H2,
    %% Pasar todo a minutos
    Valor1 = Hora1 * 60 + Minuto1,
    Valor2 = Hora2 * 60 + Minuto2,
    %% Devolver el valor a horas y devolverlo
    Hora = (Valor1 + Valor2) div 60,
    Minuto = (Valor1 + Valor2) rem 60,
    if
        Hora >= 24 ->
            {Hora-24,Minuto};
        true ->
            {Hora,Minuto}
    end.

%%--------------------------------------------------------------------
%% @doc
%% Resta dos horas en formato {Hora,Minuto} devolviendo el resultado
%% en el mismo formato.
%% @end
%%--------------------------------------------------------------------
restar_hora(H1, H2) ->
    {Hora1,Minuto1} = H1,
    {Hora2,Minuto2} = H2,
    %% Pasar todo a minutos
    Valor1 = Hora1 * 60 + Minuto1,
    Valor2 = Hora2 * 60 + Minuto2,
    io:format("Valor1: ~p -- Valor2: ~p~n", [Valor1,Valor2]),
    %% Devolver el valor a horas y devolverlo
    Hora = (Valor1 - Valor2) div 60,
    Minuto = (Valor1 - Valor2) rem 60,
    io:format("Hora: ~p -- Minuto: ~p~n", [Hora,Minuto]),
    if
        Minuto < 0 andalso Hora == 0 ->
            {23, 60 + Minuto};
        Minuto < 0 andalso Hora > 0 ->
            {Hora - 1, 60 + Minuto};
        Minuto < 0 andalso Hora < 0 ->
            {24+Hora, 60 + Minuto};
        Minuto >= 0 andalso Hora < 0 ->
            {24-Hora, Minuto};
        true ->
            {Hora,Minuto}
    end.

%%--------------------------------------------------------------------
%% @doc
%% Dada una lista de uuids devuelve un tuple con dos listas separadas
%% de Personajes y Objetos.
%%
%% Ver la documentación de lists:partition/1 para ver cómo.
%% @end
%%--------------------------------------------------------------------
separar_objetos_personajes(Lista_UUIDs) ->
    lists:partition(
      fun(E) ->
              #entidad{tipo=Tipo} = hd(ets:lookup(entidades,E)),
              if
                  Tipo == ?PERSONAJE ->
                      true;
                  true -> false
              end
      end,
      Lista_UUIDs).

%%--------------------------------------------------------------------
%% @doc
%% Dada una lista de uuids que contiene sólo objetos, separa los mismos
%% en dos listas de {Objetos,Ropa}. Se cuenta como ropa sólo si está
%% vestida. Una prenda de vestir sin poner se cuenta como objeto y se
%% listará en inventario.
%%
%% Ver la documentación de lists:partition/1 para ver cómo.
%% @end
%%--------------------------------------------------------------------
separar_objetos_ropa(Lista_UUIDs) ->
    lists:partition(
      fun(E) ->
              #entidad{tipo=Tipo} = hd(ets:lookup(entidades,E)),
              if
                  Tipo == ?OBJETO_VESTIBLE ->
                      #obj{estado=Estado} = hd(ets:lookup(objetos,E)),
                      if Estado == ?VESTIDO ->
                              false;
                         true ->
                              true
                      end;
                  true -> true
              end
      end,
      Lista_UUIDs).

%%--------------------------------------------------------------------
%% @doc
%% Dado el uuid de un cierre devuelve su estado: ?ABIERTO, ?CERRADO,
%% ?LLAVE_ABIERTO, ?LLAVE_CERRADO o ?FORZADO.
%%
%% Si recibe un «nil» devuelve el estado ?ABIERTO que es el de por
%% defecto.
%% @end
%%--------------------------------------------------------------------
obtener_estado_cierre(UUID) ->
    if UUID /= nil ->
            #cierre{estado=Estado} = hd(ets:lookup(cierres,UUID));
       true ->
            Estado = ?ABIERTO
    end,
    Estado.

%%--------------------------------------------------------------------
%% @doc
%% Dado el uuid de un lugar devuelve una lista de cadenas formateadas
%% con los nombres de las salidas del lugar.
%%
%% Tiene en cuenta si el lugar también es un OBJETO_LUGAR
%% @end
%%--------------------------------------------------------------------
obtener_nombre_salidas(Formato, UUID_Lugar) ->
    #entidad{tipo=Tipo} = hd(ets:lookup(entidades, UUID_Lugar)),
    Salidas =
        case Tipo of
            ?OBJETO_LUGAR ->
                #obj{enlaces=Enlaces} = hd(ets:lookup(objetos,UUID_Lugar)),
                Enlaces;
            ?LUGAR ->
                #lugar{enlaces=Enlaces} = hd(ets:lookup(lugares,UUID_Lugar)),
                Enlaces;
            _ ->
                []
        end,
    Nombres_Salidas =
        lists:map(
          fun(S) ->
                  {_, Nombres, Estado, _, Cierre} = S,
                  Iluminacion = es_de_dia() orelse hay_luz(UUID_Lugar),
                  case Cierre of
                      nil when Formato == true ->
                          formato(verde, hd(Nombres));
                      nil when Formato == false ->
                          hd(Nombres);
                      _ when Estado == ?VISIBLE orelse
                             (Estado==?OCULTO_NOCHE andalso
                              Iluminacion==true) andalso
                             Formato == true ->
                          Estado_Cierre = obtener_estado_cierre(Cierre),
                          Nombre = hd(Nombres),   % Sólo tomamos el primer nombre
                          case Estado_Cierre of
                              ?ABIERTO ->
                                  formato(verde, Nombre);
                              ?CERRADO ->
                                  io_lib:format("[~s]",[formato(amarillo, Nombre)]);
                              ?LLAVE_ABIERTO ->
                                  io_lib:format("[~s]",[formato(amarillo, Nombre)]);
                              ?LLAVE_CERRADO ->
                                  io_lib:format("[~s]",[formato(rojo, Nombre)]);
                              _ ->
                                  formato(verde, Nombre)
                          end;
                      _ when Estado == ?VISIBLE orelse
                             (Estado==?OCULTO_NOCHE andalso
                              Iluminacion==true) andalso
                             Formato == false ->
                          Estado_Cierre = obtener_estado_cierre(Cierre),
                          Nombre = hd(Nombres),   % Sólo tomamos el primer nombre
                          case Estado_Cierre of
                              ?ABIERTO ->
                                  Nombre;
                              ?CERRADO ->
                                  io_lib:format("[~s]",[Nombre]);
                              ?LLAVE_ABIERTO ->
                                  io_lib:format("[~s]",[Nombre]);
                              ?LLAVE_CERRADO ->
                                  io_lib:format("[~s]",[Nombre]);
                              _ ->
                                  formato(verde, Nombre)
                          end;
                      _ -> ""
                  end
          end,
          Salidas),
    [X || X <- Nombres_Salidas, X /= ""].

%%--------------------------------------------------------------------
%% @doc
%% Dado un uuid obtiene los uuid's de todas las entidades que se
%% encuentran como children de esa entidad o de sus children
%% @end
%%--------------------------------------------------------------------
obtener_objetos_contenidos(UUID) ->
    Entidad = hd(ets:lookup(entidades, UUID)),
    Lista = [Entidad#entidad.uuid] ++
        lists:map(
          fun(UUID_Child) ->
                  #entidad{children=Children} = hd(ets:lookup(entidades,UUID_Child)),
                  if
                      length(Children) == 0 ->
                          UUID_Child;
                      true ->
                          obtener_objetos_contenidos(UUID_Child)
                  end
          end,
          Entidad#entidad.children),
    lists:flatten(Lista).

%%--------------------------------------------------------------------
%% @doc
%% Dado un uuid de un lugar devuelve una lista de objetos luminosos si
%% su estado es encendido.
%% @end
%%--------------------------------------------------------------------
obtener_objetos_luminosos(UUID_Lugar) ->
    Objetos_Accesibles = obtener_objetos_contenidos(UUID_Lugar),
    Lista = lists:filtermap(
              fun(X) ->
                      #entidad{tipo=Tipo} = hd(ets:lookup(entidades,X)),
                      case Tipo of
                          ?OBJETO_LUZ ->
                              #obj{estado=Estado} = hd(ets:lookup(objetos,X)),
                              if
                                  Estado == ?OFF ->
                                      false;
                                  true ->
                                      true
                              end;
                          _ ->
                              false
                      end
              end,
              Objetos_Accesibles),
    Lista.

hay_luz(UUID_Lugar) ->
    Luminarias = obtener_objetos_luminosos(UUID_Lugar),
    case Luminarias of
        [] ->  false;
        [_] -> true;
        _ ->   false
    end.

%%--------------------------------------------------------------------
%% @doc
%% Devuelve un booleano diciendo si es de día.
%% @end
%%--------------------------------------------------------------------
es_de_dia() ->
    {Hora, _Minuto} = gen_server:call(erlmud_sol, dime_hora),
    if
        Hora < 7 andalso Hora >= 0 ->
            false;                   % Horas de madrugada
        hora >= 20 andalso Hora < 24 ->
            false;                   % Horas de tarde
        Hora >= 7 andalso Hora < 20 ->
            true;                    % Horas de día
        true -> true                 % Y si hay duda, es de día
    end.

%%--------------------------------------------------------------------
%% @doc
%% Dado el uuid de un Personaje devuelve una cadena formateada con la
%% la descripción del lugar, su contenido separando objetos de personajes
%% y las salidas que tiene dicho lugar.
%% @end
%%--------------------------------------------------------------------
obtener_descripcion_lugar(UUID_PJ,UUID_Lugar) ->
    Pj = hd(ets:lookup(pjs,UUID_PJ)),
    L = hd(ets:lookup(entidades,UUID_Lugar)),
    %% Comprobar si hay objetos luminosos en el lugar
    Hay_Luz = L#entidad.estado == ?EXTERIOR orelse                  % Tiene visibilidad por ser exterior
        hay_luz(UUID_Lugar) orelse                                  % Hay un objeto luminoso dentro
        (L#entidad.estado == ?TECHADO andalso es_de_dia() == true), % bajo techo con vistas diurno
    {Nombre_Propio, Desc} = obtener_nombre_descripcion_lugar(Hay_Luz, UUID_Lugar),
    %% Separar los Personajes de los Objetos eliminando al Pj y su Parent de las listas
    Children = lists:delete(UUID_Lugar,lists:delete(UUID_PJ, objetos_accesibles(UUID_Lugar))),
    {Pjs, Objs} = separar_objetos_personajes(Children),
    {Cabecera_Pjs, Nombres_Pjs} =
        case Pjs of
            [] -> {"",""};
            [_] when Hay_Luz == false ->
                Gente = io_lib:format("No puedes ver, pero notas que ~s está aquí.", [uuids_a_nombres(Pjs)]),
                {"", Gente};
            [_] when Pj#pjs.formato == true ->
                {formato(negrita, "Está: "), uuids_a_nombres(Pjs)};
            [_] when Pj#pjs.formato == false ->
                {"Está: ", uuids_a_nombres(Pjs)};
            [_|_] when Hay_Luz == false ->
                Gente = io_lib:format("A tu alrededor sientes a ~s", [lists:join(", ", uuids_a_nombres(Pjs))]),
                {"", Gente};
            [_|_] when Pj#pjs.formato == true ->
                {formato(negrita, "Están: "), lists:join(", ", uuids_a_nombres(Pjs))};
            [_|_] when Pj#pjs.formato == false ->
                {"Están: ", lists:join(", ", uuids_a_nombres(Pjs))};
            _ -> {"",""}
        end,
    {Cabecera_Objs, Nombres_Objs} =
        case Objs of
            [] -> {"", ""};
            [_|_] when Hay_Luz == false ->
                {"Intuyes, más que ves, que hay ",lists:join(", ", [nombre_con_articulo(indeterminado,X) || X <- Objs])};
            [_|_] when Pj#pjs.formato == true ->
                {formato(negrita, "Hay: "), lists:join(", ", [nombre_con_articulo(indeterminado,X) || X <- Objs])};
            [_|_] when Pj#pjs.formato == false ->
                {"Hay: ", lists:join(", ", [nombre_con_articulo(indeterminado,X) || X <- Objs])};
            _ -> {"",""}
        end,
    Nombres_Salidas = obtener_nombre_salidas(Pj#pjs.formato,UUID_Lugar),
    Salidas =
        case Pj#pjs.formato of
            true when length(Nombres_Salidas) > 0 ->
                formato(negrita, "Salidas: ") ++ lists:join(" - ", Nombres_Salidas);
            true when length(Nombres_Salidas) == 0  ->
                formato(negrita, "No hay salida.");
            false when length(Nombres_Salidas) > 0 ->
                "Salidas: " ++ lists:join(" - ", Nombres_Salidas);
            false when length(Nombres_Salidas) == 0 ->
                "No hay salida."
        end,
    case Pj#pjs.formato of
        true ->
            centrar(formato(amarillo,Nombre_Propio)) ++ "\n\n"
                ++ formatear(true,Desc) ++ "\n"
                ++ "\n" ++ formatear(true,Cabecera_Objs ++ Nombres_Objs)
                ++ "\n" ++ formatear(true,Cabecera_Pjs ++ Nombres_Pjs)
                ++ "\n" ++ formatear(true,Salidas);
        false ->
            Nombre_Propio ++ "\n\n"
                ++ formatear(false,Desc) ++ "\n"
                ++ "\n" ++ formatear(false,Cabecera_Objs ++ Nombres_Objs)
                ++ "\n" ++ formatear(false,Cabecera_Pjs ++ Nombres_Pjs)
                ++ "\n" ++ formatear(false,Salidas)
    end.

%%--------------------------------------------------------------------
%% @doc
%% Dado el uuid de un Personaje y el uuid de un objeto devuelve un
%% valor booleano indicando si dicho objeto está en el inventario del
%% personaje.
%% @end
%%--------------------------------------------------------------------
esta_en_inventario(UUID_Pj,UUID_Objeto) ->
    #entidad{children=Inventario} = hd(ets:lookup(entidades,UUID_Pj)),
    Encontrado = lists:member(UUID_Objeto,Inventario),
    if Encontrado == false ->
            false;
       true ->
            true
    end.

%%--------------------------------------------------------------------
%% @doc
%% Dado el uuid de una localización, devuelve su Parent que indica la
%% la zona en la que está inscrita o su propio ID si es la raíz de
%% todas las entidades.
%% @end
%%--------------------------------------------------------------------
obtener_id_zona(UUID_Entidad) ->
    #entidad{parent=UUID_Parent} = hd(ets:lookup(entidades,UUID_Entidad)),
    case UUID_Parent of
        nil ->
            UUID_Entidad;
        _ ->
            UUID_Parent
    end.

%%--------------------------------------------------------------------
%% @doc
%% Dado el día de la semana en cifra, devuelve la cadena con el nombre
%% del día en cuestión.
%% @end
%%--------------------------------------------------------------------
nombre_dia_semana(Dia) ->
    case Dia of
        0 -> "Taera";
        1 -> "Taunu";
        2 -> "Tadu";
        3 -> "Tatri";
        4 -> "Takvar";
        5 -> "Takvin";
        6 -> "Tases";
        _ -> ""
    end.

%%--------------------------------------------------------------------
%% @doc
%% Dada una cadena quita todos los espacios en blanco, los saltos de
%% linea, los tabuladores y los retornos dejando una cadena simple
%% @end
%%--------------------------------------------------------------------
quitar_espacios(Cadena) ->
    Tokens = string:tokens(Cadena, "\n\r\t "),
    lists:join(" ", Tokens).

%%--------------------------------------------------------------------
%% @doc
%% Dado el uuid de un objeto que debe ser una prenda de vestir,
%% devuelve la descripción que corresponda según se esté vistiendo o
%% no, marcado por el campo «estado» del objeto.
%% @end
%%--------------------------------------------------------------------
obtener_descripcion_vestido(UUID) ->
    Objeto = hd(ets:lookup(objetos,UUID)),
    Value = lists:keysearch(Objeto#obj.estado,1,Objeto#obj.desc),
    case Value of
        {value, Tuple} ->
            {_,Descripcion} = Tuple,
            Descripcion;
        _ ->
            ""
    end.

%%--------------------------------------------------------------------
%% @doc
%% Dado el uuid de un personaje devuelve las descripciones de los
%% objetos vestibles que lleva puestos ignorando el resto. Si no
%% encuentra ningún objeto que esté en estado ?VESTIDO, devuelve la
%% cadena "Está completamente desnudo." ajustándola al género del
%% personaje.
%% @end
%%--------------------------------------------------------------------
obtener_indumentaria(UUID_Pj) ->
    #entidad{children=Children} = hd(ets:lookup(entidades,UUID_Pj)),
    Prendas =
        lists:map(
          fun(X) ->
                  #entidad{tipo=Tipo} = hd(ets:lookup(entidades,X)),
                  #obj{estado=Estado} = hd(ets:lookup(objetos,X)),
                  case Tipo of
                      ?OBJETO_VESTIBLE when Estado == ?VESTIDO ->
                          quitar_espacios(obtener_descripcion_vestido(X));
                      _ -> " "
                  end
          end,
          Children),
    Vestimenta = quitar_espacios(lists:join(" ",Prendas)),
    Desc = string:trim(Vestimenta),
    if
        Desc == [] ->
            "Está completamente desnud" ++ fin_genero(UUID_Pj) ++ ".";
        true ->
            Desc
    end.

%%--------------------------------------------------------------------
%% @doc
%% Esta función limpia la cadena por la derecha eliminando los chars
%% que podemos encontrar. Desde telnet puede llegar bien un \n\0 al
%% final o un \n\r, según la configuración. Con netcat sólo llega \n
%% @end
%%--------------------------------------------------------------------
limpiar_entrada(Linea) ->
    string:strip(string:strip(string:strip(Linea, right, $\0), right, $\n), right, $\r).

%%--------------------------------------------------------------------
%% @doc
%% Esta función devuelve una cadena con los nombres de las salidas para
%% una localidad concreta referenciada por UUID_Lugar.
%% @end
%%--------------------------------------------------------------------
escribir_salidas(Pj, UUID_Lugar) ->
    #pjs{formato=Formato} = Pj,
    Nombre_Salidas = obtener_nombre_salidas(Formato,UUID_Lugar),
    case Formato of
        true when length(Nombre_Salidas) > 0 ->
            utiles:formato(negrita, "Salidas: ") ++ lists:join(" - ", Nombre_Salidas);
        true when length(Nombre_Salidas) == 0 ->
            utiles:formato(negrita, "No hay salida.");
        false when length(Nombre_Salidas) > 0 ->
            "Salidas:" ++ lists:join(" - ", Nombre_Salidas);
        false when length(Nombre_Salidas) == 0 ->
            "No hay salida"
    end.

%%--------------------------------------------------------------------
%% @doc
%% Devuelve una cadena de los objetos que lleva el Pj. Separa los
%% objetos que se llevan, de los que se visten llamando a la función
%% utiles:separar_objetos_ropa/1 y muestra los mensajes apropiados
%% según esté activado el formato de texto o no.
%% @end
%%--------------------------------------------------------------------
escribir_inventario(Pj) ->
    #entidad{children=Children} = hd(ets:lookup(entidades, Pj#pjs.uuid)),
    {Objetos, Ropa} = separar_objetos_ropa(Children),
    case Pj#pjs.formato of
        true when length(Objetos) >= 1 andalso
                  length(Ropa) == 0 ->
            utiles:formato(negrita,"Inventario: ") ++ string:join([utiles:nombre_con_articulo(indeterminado,X) || X <- Objetos], ", ") ++ ".";
        true when length(Objetos) == 0 andalso
                  length(Ropa) == 0 ->
            utiles:formato(negrita,"Inventario: ") ++ "No llevas nada.";
        true when length(Objetos) >= 1 andalso
                  length(Ropa) >= 1 ->
            utiles:formato(negrita,"Inventario: ") ++ string:join([utiles:nombre_con_articulo(indeterminado,X) || X <- Objetos], ", ") ++ "\n"
                ++ utiles:formato(negrita, "Llevas puesto: ") ++ string:join([utiles:nombre_con_articulo(indeterminado,X) || X <- Ropa], ", ") ++ ".";
        true when length(Objetos) == 0 andalso
                  length(Ropa) >= 1 ->
            utiles:formato(negrita, "Inventario: ") ++ "No llevas nada\n"
                ++ utiles:formato(negrita, "Llevas puesto: ") ++ string:join([utiles:nombre_con_articulo(indeterminado,X) || X <- Ropa], ", ") ++ ".";
        false when length(Objetos) >= 1 andalso
                   length(Ropa) ->
            "Inventario: " ++ string:join([utiles:nombre_con_articulo(indeterminado,X) || X <- Children], ", ") ++ ".";
        false when length(Objetos) == 0 andalso
                   length(Ropa) ->
            "Inventario: No llevas nada.";
        false when length(Objetos) >= 1 andalso
                  length(Ropa) >= 1 ->
            "Inventario: " ++ string:join([utiles:nombre_con_articulo(indeterminado,X) || X <- Objetos], ", ") ++ "\n"
                ++ "Llevas puesto: " ++ string:join([utiles:nombre_con_articulo(indeterminado,X) || X <- Ropa], ", ") ++ ".";
        false when length(Objetos) == 0 andalso
                  length(Ropa) >= 1 ->
            "Inventario: " ++ "No llevas nada\n"
                ++ "Llevas puesto: " ++ string:join([utiles:nombre_con_articulo(indeterminado,X) || X <- Ropa], ", ") ++ "."
    end.

%%--------------------------------------------------------------------
%% @doc
%% Esta función cambia el estado de apagado a encendido de un objeto de
%% tipo ?OBJETO_LUZ
%% @end
%%--------------------------------------------------------------------
encender(_Pj, []) ->
    "Hmmm, no sé qué quieres encender.";
encender(Pj, Objetos) ->
    Objeto = hd(ets:lookup(objetos,hd(Objetos))),
    #pjs{uuid=UUID, nombre_propio=Nombre} = Pj,
    Mensaje =
        case Objeto#obj.tipo of
            ?OBJETO_LUZ when Objeto#obj.estado == ?OFF ->
                New_Objeto = Objeto#obj{estado = ?ON},
                Aviso_Evento = io_lib:format("~s ha encendido ~s.", [Nombre, utiles:nombre_con_articulo(indeterminado,Objeto#obj.uuid)]),
                gen_server:cast(erlmud_datos, {actualizar_objeto, New_Objeto}),
                gen_server:cast(erlmud_servidor, {evento_pj, UUID, Aviso_Evento}),
                "Enciendes " ++ utiles:nombre_con_articulo(determinado, Objeto#obj.uuid);
        ?OBJETO_LUZ when Objeto#obj.estado == ?ON ->
                "Hmmm, " ++ utiles:nombre_con_articulo(determinado, Objeto#obj.uuid)
                    ++ " ya estaba encendid" ++ utiles:fin_genero(Objeto#obj.uuid);
        _ ->
                "Hmmm, no parece que se pueda encender."
        end,
    Mensaje.

%%--------------------------------------------------------------------
%% @doc
%% Esta función cambia el estado de encendido a apagado de un objeto de
%% tipo ?OBJETO_LUZ
%% @end
%%--------------------------------------------------------------------
apagar(_Pj, []) ->
    "Hmmm, no sé qué quieres apagar.";
apagar(Pj, Objetos) ->
    Objeto = hd(ets:lookup(objetos,hd(Objetos))),
    #pjs{uuid=UUID, nombre_propio=Nombre} = Pj,
    Mensaje =
        case Objeto#obj.tipo of
            ?OBJETO_LUZ when Objeto#obj.estado == ?ON ->
                New_Objeto = Objeto#obj{estado = ?OFF},
                Aviso_Evento = io_lib:format("~s ha apagado ~s.", [Nombre, utiles:nombre_con_articulo(determinado, Objeto#obj.uuid)]),
                gen_server:cast(erlmud_datos, {actualizar_objeto, New_Objeto}),
                gen_server:cast(erlmud_servidor, {evento_pj, UUID, Aviso_Evento}),
                "Apagas " ++ utiles:nombre_con_articulo(determinado, Objeto#obj.uuid);
        ?OBJETO_LUZ when Objeto#obj.estado == ?OFF ->
                "Hmmm, " ++ utiles:nombre_con_articulo(determinado, Objeto#obj.uuid)
                    ++ " ya estaba apagad" ++ utiles:fin_genero(Objeto#obj.uuid);
        _ ->
                "Hmmm, no parece que se pueda apagar"
    end,
    Mensaje.

%%--------------------------------------------------------------------
%% @doc
%% Dada una Lista de elementos, cuenta el número de veces que aparece
%% y devuelve una lista de tuplas ordenada de mayor a menor número de
%% apariciones, donde cada una elemento de la tupla consta de dos
%% elementos, el primero es el elemento en la Lista original y el
%% segundo el número de veces que aparece en ella.
%% @end
%%--------------------------------------------------------------------
contar_elementos(Lista) ->
    %% Nos apoyamos en la función recursiva contar_elementos/2
    contar_elementos(Lista, #{}).

contar_elementos([H|T], X) ->
    case maps:is_key(H,X) of
        true ->
            %% Si el elemento H existe en X suma 1 a su valor
            contar_elementos(T, maps:put(H, maps:get(H,X) + 1, X));
        false ->
            %% Si el elemento H no existe en X lo crea con valor 1
            contar_elementos(T, maps:put(H, 1, X))
    end;
contar_elementos([],X) ->
    %% Si no hay más elementos convierte X en lista y la ordena
    R = maps:to_list(X),
    lists:sort(
      fun({_,N},{_,M}) ->
              N > M
      end,
      R).

%%--------------------------------------------------------------------
%% @doc
%% Dada una Lista de entidades devuelve el objeto entidad más
%% significativo del conjunto.
%%
%% Ojo que devuelve el registro completo y no sólo su uuid.
%% @end
%%--------------------------------------------------------------------
devolver_entidad_significativa(Lista) ->
    {E,_} = hd(contar_elementos(Lista)),
    hd(ets:lookup(entidades,E)).

%%--------------------------------------------------------------------
%% @doc
%% Dada una Lista de entidades separa los Objetos de los personajes y
%% devuelve el objeto más significativo.
%%
%% Ojo que devuelve el registro Obj y no sólo su uuid.
%% @end
%%--------------------------------------------------------------------
devolver_objeto_significativo(Lista) ->
    %% Descartamos los personajes de la lista
    {_, Objetos} = separar_objetos_personajes(Lista),
    %% Tomamos el que más se repita
    {Obj, _} = hd(contar_elementos(Objetos)),
    %% Devolvemos el objeto, no su id
    hd(ets:lookup(objetos, Obj)).

%%--------------------------------------------------------------------
%% @doc
%% Dada una Lista de entidades separa los Objetos de los Personajes y
%% devuelve el personaje más significativo.
%%
%% Ojo que devuelve el registro Pjs y no sólo su uuid.
%% @end
%%--------------------------------------------------------------------
devolver_personaje_significativo(Lista) ->
    %% Descartamos los objetos de la lista
    {Personajes,_} = separar_objetos_personajes(Lista),
    %% Tomamos el que más se repita
    {Pjs, _} = hd(contar_elementos(Personajes)),
    %% Devolvemos el personaje, no su id
    hd(ets:lookup(pjs, Pjs)).
