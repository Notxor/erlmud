%%%-------------------------------------------------------------------
%%% @author Notxor <notxor@oldulo>
%%% @copyright (C) 2020, Notxor
%%% @doc
%%% Supervisor para los elementos cosmológicos del programa
%%% @end
%%% Created : 17 Sep 2020 by Notxor <notxor@nueva-actitud.org>
%%%-------------------------------------------------------------------
-module(erlmud_cosmosup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the supervisor
%% @end
%%--------------------------------------------------------------------
%% -spec start_link() -> {ok, Pid :: pid()} |
%%           {error, {already_started, Pid :: pid()}} |
%%           {error, {shutdown, term()}} |
%%           {error, term()} |
%%           ignore.
start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a supervisor is started using supervisor:start_link/[2,3],
%% this function is called by the new process to find out about
%% restart strategy, maximum restart intensity, and child
%% specifications.
%% @end
%%--------------------------------------------------------------------
%% -spec init(Args :: term()) ->
%%           {ok, {SupFlags :: supervisor:sup_flags(),
%%                 [ChildSpec :: supervisor:child_spec()]}} |
%%           ignore.
init([]) ->

    SupFlags = #{strategy => one_for_one,
                 intensity => 1,
                 period => 5},

    Sol      = #{id => erlmud_sol,
                 start => {erlmud_sol, start_link, []},
                 restart => permanent,
                 shutdown => 5000,
                 type => worker,
                 modules => [erlmud_sol]},


    Blua     = #{id => erlmud_blua,
                 start => {erlmud_blua, start_link, []},
                 restart => permanent,
                 shutdown => 5000,
                 type => worker,
                 modules => [erlmud_blua]},


    Ruya     = #{id => erlmud_ruya,
                 start => {erlmud_ruya, start_link, []},
                 restart => permanent,
                 shutdown => 5000,
                 type => worker,
                 modules => [erlmud_ruya]},


    Blanka   = #{id => erlmud_blanka,
                 start => {erlmud_blanka, start_link, []},
                 restart => permanent,
                 shutdown => 5000,
                 type => worker,
                 modules => [erlmud_blanka]},

    Bruna    = #{id => erlmud_bruna,
                 start => {erlmud_bruna, start_link, []},
                 restart => permanent,
                 shutdown => 5000,
                 type => worker,
                 modules => [erlmud_bruna]},

    Meteo    = #{id => erlmud_meteo,
                 start => {erlmud_meteo, start_link, []},
                 restart => permanent,
                 shutdown => 5000,
                 type => worker,
                 modules => [erlmud_meteo]},

    {ok, {SupFlags, [Sol,Blua,Ruya,Blanka,Bruna,Meteo]}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
