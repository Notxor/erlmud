%%%-------------------------------------------------------------------
%%% @author Notxor <notxor@nueva-actitud.org>
%%% @copyright (C) 2020, Notxor
%%% @doc
%%% Servidor de la aplicación
%%% @end
%%% Created :  6 Jul 2020 by Notxor <notxor@nueva-actitud.org>
%%%-------------------------------------------------------------------
-module(erlmud_servidor).

-behaviour(gen_server).

%% API
-export([start_link/0]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3, format_status/2]).

-define(SERVER, ?MODULE).

%% Incluir los registros de la aplicación
-include("include/registros.hrl").

%% Registro para el estado del servidor.
-record(estado, {
                  puerto = ?DEFAULT_PORT
                , listen
                }).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%% @end
%%--------------------------------------------------------------------
%% -spec start_link() -> {ok, Pid :: pid()} |
%%           {error, Error :: {already_started, pid()}} |
%%           {error, Error :: term()} |
%%           ignore.
start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Initializes the server
%% @end
%%--------------------------------------------------------------------
%% -spec init(Args :: term()) -> {ok, State :: term()} |
%%           {ok, State :: term(), Timeout :: timeout()} |
%%           {ok, State :: term(), hibernate} |
%%           {stop, Reason :: term()} |
%%           ignore.
init([]) ->
    init([?DEFAULT_PORT]);
init([Port]) ->
    process_flag(trap_exit, true),
    Estado = #estado{puerto=Port},
    ets:new(sockets, [set, named_table, protected,
                      {keypos, #socket.uuid},
                      {heir, self(), "Servidor"}]),
    {ok, Estado}.

%%--------------------------------------------------------------------
%% @doc
%% Handling call messages
%% @end
%%--------------------------------------------------------------------
%% -spec handle_call(Request :: term(), From :: {pid(), term()}, State :: term()) ->
%%           {reply, Reply :: term(), NewState :: term()} |
%%           {reply, Reply :: term(), NewState :: term(), Timeout :: timeout()} |
%%           {reply, Reply :: term(), NewState :: term(), hibernate} |
%%           {noreply, NewState :: term()} |
%%           {noreply, NewState :: term(), Timeout :: timeout()} |
%%           {noreply, NewState :: term(), hibernate} |
%%           {stop, Reason :: term(), Reply :: term(), NewState :: term()} |
%%           {stop, Reason :: term(), NewState :: term()}.
handle_call(get_lsocket, _From, State) ->
    Reply = State#estado.listen,
    {reply, Reply, State};
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

%%--------------------------------------------------------------------
%% @doc
%% Handling cast messages
%% @end
%%--------------------------------------------------------------------
%% -spec handle_cast(Request :: term(), State :: term()) ->
%%           {noreply, NewState :: term()} |
%%           {noreply, NewState :: term(), Timeout :: timeout()} |
%%           {noreply, NewState :: term(), hibernate} |
%%           {stop, Reason :: term(), NewState :: term()}.
handle_cast({add_cliente, Datos}, State) ->
    ets:insert(sockets, Datos),
    {noreply, State};
handle_cast({listen_socket, LSocket}, State) ->
    {noreply, State#estado{listen=LSocket}};
handle_cast({del_cliente, UUID_PJ}, State) ->
    %% Se avisa de la desaparición del personaje.
    PJ = hd(ets:lookup(pjs, UUID_PJ)),
    #entidad{parent=UUID_Parent} = hd(ets:lookup(entidades,UUID_PJ)),
    Parent = hd(ets:lookup(entidades, UUID_Parent)),
    New_Children = lists:delete(UUID_PJ, Parent#entidad.children),
    gen_server:cast(erlmud_datos,{actualizar_entidad, Parent#entidad{children=New_Children}}),
    Mensaje_Salida = PJ#pjs.nombre_propio ++ " se desvanece en la nada.",
    Objetos = lists:delete(UUID_PJ, utiles:objetos_accesibles(Parent#entidad.uuid)),
    enviar_mensaje_evento(Objetos, Mensaje_Salida),
    {noreply, State};
handle_cast({evento_decir, UUID_PJ, Mensaje}, State) ->
    Pj = hd(ets:lookup(pjs, UUID_PJ)),
    Nombre = Pj#pjs.nombre_propio,
    #entidad{parent=Posicion} = hd(ets:lookup(entidades,Pj#pjs.uuid)),
    Entidad_Parent = hd(ets:lookup(entidades,Posicion)),
    case Entidad_Parent#entidad.tipo of
        ?OBJETO_TABLA ->
            %% Coger el Preter-Parent, el Pj está sentado o tumbado en un objeto-tabla
            Preter_Parent = hd(ets:lookup(entidades,Entidad_Parent#entidad.parent)),
            UUID_Parent = Preter_Parent#entidad.uuid;
        _ ->
            UUID_Parent = Entidad_Parent#entidad.uuid
    end,
    Mensaje_Evento = Nombre ++ " dice: «" ++ Mensaje ++ "»",
    %% Eliminamos el UUID_PJ para evitar el eco.
    Children = lists:delete(UUID_PJ, utiles:objetos_accesibles(UUID_Parent)),
    enviar_mensaje_evento(Children, Mensaje_Evento),
    {noreply, State};
handle_cast({evento_gritar, UUID_PJ, Mensaje}, State) ->
    Pj = hd(ets:lookup(pjs, UUID_PJ)),
    Nombre = Pj#pjs.nombre_propio,
    #entidad{parent=Posicion} = hd(ets:lookup(entidades,Pj#pjs.uuid)),
    Entidad_Parent = hd(ets:lookup(entidades,Posicion)),
    case Entidad_Parent#entidad.tipo of
        ?OBJETO_TABLA ->
            %% Coger el Preter-Parent, el Pj está sentado o tumbado en un objeto-tabla
            Preter_Parent = hd(ets:lookup(entidades,Entidad_Parent#entidad.parent)),
            UUID_Parent = Preter_Parent#entidad.uuid;
        _ ->
            UUID_Parent = Entidad_Parent#entidad.uuid
    end,
    UUID_Zona = utiles:obtener_id_zona(UUID_Parent),
    Mensaje_Evento =
        case Mensaje of
            "" ->
                Nombre ++ " grita desde " ++ utiles:nombre_con_articulo(determinado,Posicion);
            _ ->
                Nombre ++ " grita desde " ++ utiles:nombre_con_articulo(determinado,Posicion)
                    ++ ": «" ++ Mensaje ++ "»."
        end,
    %% Eliminar el UUID_PJ de la lista para evitar el eco.
    Children = lists:delete(UUID_PJ, utiles:obtener_objetos_contenidos(UUID_Zona)),
    enviar_mensaje_evento(Children, Mensaje_Evento),
    {noreply, State};
handle_cast({evento_ir, UUID_PJ, UUID_Origen, UUID_Destino}, State) ->
    PJ = hd(ets:lookup(pjs, UUID_PJ)),
    Origen = hd(ets:lookup(entidades, UUID_Origen)),
    Destino = hd(ets:lookup(entidades, UUID_Destino)),
    if
        UUID_Origen /= UUID_Destino ->
            Mensaje_Salida = PJ#pjs.nombre_propio ++ " se dirige hacia " ++ Destino#entidad.nombre,
            Objetos_Salida = lists:delete(UUID_PJ, utiles:objetos_accesibles(UUID_Origen)),
            enviar_mensaje_evento(Objetos_Salida, Mensaje_Salida),
            Mensaje_Entrada = PJ#pjs.nombre_propio ++ " llega desde " ++ Origen#entidad.nombre,
            Objetos_Entrada = lists:delete(UUID_PJ, utiles:objetos_accesibles(UUID_Destino)),
            enviar_mensaje_evento(Objetos_Entrada, Mensaje_Entrada);
        true ->
            Mensaje = "Una intensa niebla se solidifica cerca de ti y " ++ PJ#pjs.nombre_propio ++ " se materializa de la nada.",
            Objetos = lists:delete(UUID_PJ, utiles:objetos_accesibles(UUID_Origen)),
            enviar_mensaje_evento(Objetos, Mensaje)
    end,
    {noreply, State};
handle_cast({evento_pj, UUID, Mensaje}, State) ->
    #entidad{parent=Parent} = hd(ets:lookup(entidades, UUID)),
    Objetos = lists:delete(UUID, utiles:objetos_accesibles(Parent)),
    enviar_mensaje_evento(Objetos, Mensaje),
    {noreply, State};
handle_cast({evento_dar, UUID_Origen, UUID_Destino, UUID_Objeto}, State) ->
    Da = hd(ets:lookup(pjs,UUID_Origen)),
    Recibe = hd(ets:lookup(pjs,UUID_Destino)),
    Mensaje = Da#pjs.nombre_propio ++ " da "
        ++ utiles:nombre_con_articulo(indeterminado,UUID_Objeto)
        ++ " a " ++ Recibe#pjs.nombre_propio,
    #entidad{parent=UUID_Parent} = hd(ets:lookup(entidades,UUID_Origen)),
    Avisados = lists:delete(UUID_Origen, utiles:objetos_accesibles(UUID_Parent)),
    enviar_mensaje_evento(lists:delete(UUID_Destino,Avisados), Mensaje),
    enviar_mensaje_evento([UUID_Destino], Da#pjs.nombre_propio ++ " te da " ++
                              utiles:nombre_con_articulo(indeterminado, UUID_Objeto)),
    {noreply, State};
handle_cast({evento_global, Mensaje}, State) ->
    enviar_mensaje_evento(Mensaje),
    {noreply, State};
handle_cast(_Request, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @doc
%% Handling all non call/cast messages
%% @end
%%--------------------------------------------------------------------
%% -spec handle_info(Info :: timeout() | term(), State :: term()) ->
%%           {noreply, NewState :: term()} |
%%           {noreply, NewState :: term(), Timeout :: timeout()} |
%%           {noreply, NewState :: term(), hibernate} |
%%           {stop, Reason :: normal | term(), NewState :: term()}.
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%% @end
%%--------------------------------------------------------------------
%% -spec terminate(Reason :: normal | shutdown | {shutdown, term()} | term(),
%%                 State :: term()) -> any().
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @doc
%% Convert process state when code is changed
%% @end
%%--------------------------------------------------------------------
%% -spec code_change(OldVsn :: term() | {down, term()},
%%                   State :: term(),
%%                   Extra :: term()) -> {ok, NewState :: term()} |
%%           {error, Reason :: term()}.
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%--------------------------------------------------------------------
%% @doc
%% This function is called for changing the form and appearance
%% of gen_server status when it is returned from sys:get_status/1,2
%% or when it appears in termination error logs.
%% @end
%%--------------------------------------------------------------------
%% -spec format_status(Opt :: normal | terminate,
%%                     Status :: list()) -> Status :: term().
format_status(_Opt, Status) ->
    Status.

%%%===================================================================
%%% Internal functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Envía un mensaje de evento a los todos los objetos que lleguen en
%% la lista de uuids que viene en Objetos.
%%
%% Si sólo recibe un mensaje, lo envía a todos los sockets.
%% @end
%%--------------------------------------------------------------------
enviar_mensaje_evento(Objetos, Mensaje) ->
    lists:map(fun(UUID) ->
        Clientes = ets:lookup(sockets, UUID),
        case Clientes of
            [] -> ok;
            [H] ->
                gen_server:cast(H#socket.pid, {informar_evento, Mensaje})
        end
    end,
    Objetos).
enviar_mensaje_evento(Mensaje) ->
    Conexiones = ets:tab2list(sockets),
    lists:map(
        fun(X) ->
            if
                X#socket.uuid /= nil ->
                    %% El cliente que está a la espera de conexión guarda el PID de la última
                    %% conexión establecida. Por eso comprobamos que tenga asignado un UUID
                    gen_server:cast(X#socket.pid, {informar_evento, Mensaje});
                true -> ok
            end
        end,
    Conexiones).
