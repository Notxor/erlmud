%%%-------------------------------------------------------------------
%%% @author Notxor <notxor@nueva-actitud.org>
%%% @copyright (C) 2020, Notxor
%%% @doc
%%% Pequeña aplicación MUD hecha en erlang
%%% @end
%%% Created : 28 Jun 2020 by Notxor <notxor@nueva-actitud.org>
%%%-------------------------------------------------------------------
-module(erlmud_app).

-behaviour(application).

%% Application callbacks
-export([start/2, start_phase/3, stop/1, prep_stop/1,
         config_change/3]).

%%%===================================================================
%%% Callbacks de application
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Esta función se llama cuando una aplicación se inicia utilizando
%% application:start/[1,2] y debe comenzar el proceso de la aplicación.
%% Si la aplicación tiene una estructura acorde con los principios de
%% diseño de OTP como un árbol de supervisión, ésto significa iniciar
%% el supervisor «top» del árbol.
%% @end
%%--------------------------------------------------------------------
start(_StartType, _StartArgs) ->
    case erlmud_supersup:start_link() of
        {ok, Pid} ->
            {ok, Pid};
        Error ->
            Error
    end.

%%--------------------------------------------------------------------
%% @doc
%% supervisor top del árbol.
%% Inicia una aplicación con las aplicaciones incluidas, cuando se
%% necesita sincronización entre procesos en las diferentes
%% aplicaciones durante el inicio.
%% @end
%%--------------------------------------------------------------------
start_phase(_Phase, _StartType, _PhaseArgs) ->
    ok.

%%--------------------------------------------------------------------
%% @doc
%% Esta función se llama cuando una aplicación se para. Es el opuesto
%% intencionadamente a Module:start/2 y debe hacer cualquier limpieza
%% necesaria. El valor de retorno es ignorado.
%% @end
%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%--------------------------------------------------------------------
%% @doc
%% Esta función se llama cuando una aplicación va a ser detenida,
%% antes de salir del proceso de la aplicación.
%% @end
%%--------------------------------------------------------------------
prep_stop(State) ->
    State.

%%--------------------------------------------------------------------
%% @doc
%% Esta función se llama por una aplicación después de reemplazar el
%% código, si la los parámetros de configuración deben cambiar.
%% @end
%%--------------------------------------------------------------------
config_change(_Changed, _New, _Removed) ->
    ok.

%%%===================================================================
%%% Funciones internas
%%%===================================================================
