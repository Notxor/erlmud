%%%-------------------------------------------------------------------
%%% @author Notxor <notxor@nueva-actitud.org>
%%% @copyright (C) 2020, Notxor
%%% @doc
%%% Supervisor básico.
%%% @end
%%% Created : 07 Jul 2020 by Notxor <notxor@nueva-actitud.org>
%%%-------------------------------------------------------------------
-module(erlmud_sup).

-behaviour(supervisor).

%% API
-export([start_link/0,start_cliente/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

-include("include/registros.hrl").

%%%===================================================================
%%% Funciones API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Lanza el supervisor
%% @end
%%--------------------------------------------------------------------
start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%--------------------------------------------------------------------
%% @doc
%% Inicia un proceso cliente.
%% @end
%%--------------------------------------------------------------------
start_cliente() ->
    supervisor:start_child(?MODULE, []).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Cuando se inicia un supervisor usando supervisor:start_link/[2,3],
%% esta función la llama el nuevo proceso buscando la estrategia de
%% reinicio, la intensidad máxima de reinicio y las especificaciones
%% el proceso «hijo»
%% @end
%%--------------------------------------------------------------------
init([]) ->
    {ok, LSocket} = gen_tcp:listen(?DEFAULT_PORT, [list,
                                                   {packet,line},
                                                   {reuseaddr,true},
                                                   {active,true}]),
    gen_server:cast(erlmud_servidor, {listen_socket, LSocket}),
    unlink(LSocket),
    SupFlags = #{strategy => simple_one_for_one,
                 intensity => 1,
                 period => 5},
    Child    = #{id => erlmud_cliente,
                 start => {erlmud_cliente, start_link, [LSocket]},
                 restart => temporary,
                 shutdown => 5000,
                 type => worker,
                 modules => [erlmud_cliente]},
    spawn_link(fun start_cliente/0),
    {ok, {SupFlags, [Child]}}.

%%%===================================================================
%%% Funciones internas
%%%===================================================================

