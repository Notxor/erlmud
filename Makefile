.PHONY: clean compilar run solorun all

DIR_SRC = ./src
DIR_BIN = ./ebin
DIR_DOC = ./doc

APP_NAME = erlmud
SOURCES = erlmud_servidor.erl \
	  erlmud_supersup.erl \
	  erlmud_app.erl \
	  erlmud_sup.erl \
	  erlmud_cliente.erl \
	  erlmud_datos.erl \
	  erlmud_cosmosup.erl \
	  erlmud_sol.erl \
	  erlmud_blua.erl \
	  erlmud_ruya.erl \
	  erlmud_blanka.erl \
	  erlmud_bruna.erl \
	  erlmud_meteo.erl \
	  utiles.erl \
	  erlmud.app

TARGETS = $(DIR_BIN)/erlmud_servidor.beam \
	  $(DIR_BIN)/erlmud_supersup.beam \
          $(DIR_BIN)/erlmud_app.beam \
          $(DIR_BIN)/erlmud_sup.beam \
	  $(DIR_BIN)/erlmud_cliente.beam \
	  $(DIR_BIN)/erlmud_datos.beam \
	  $(DIR_BIN)/erlmud_cosmosup.beam \
	  $(DIR_BIN)/erlmud_sol.beam \
	  $(DIR_BIN)/erlmud_blua.beam \
	  $(DIR_BIN)/erlmud_ruya.beam \
	  $(DIR_BIN)/erlmud_blanka.beam \
	  $(DIR_BIN)/erlmud_bruna.beam \
	  $(DIR_BIN)/erlmud_meteo.beam \
	  $(DIR_BIN)/utiles.beam \
          $(DIR_BIN)/erlmud.app

compilar: $(DIR_BIN) $(TARGETS)

clean:
	rm -rf $(DIR_BIN)
	rm -rf $(DIR_DOC)/*.html

$(DIR_BIN)/%.beam: $(DIR_SRC)/%.erl
	erlc -o $(DIR_BIN) $<

$(DIR_BIN)/erlmud_datos.beam: ./include/data_mundi.hrl ./include/registros.hrl

$(DIR_BIN)/erlmud_cliente.beam: ./include/sinonimos.hrl ./include/registros.hrl

$(DIR_BIN)/erlmud_servidor.beam: ./include/registros.hrl

$(DIR_BIN)/$(APP_NAME).app: $(DIR_SRC)/$(APP_NAME).app
	cp $(DIR_SRC)/$(APP_NAME).app $(DIR_BIN)/$(APP_NAME).app 

$(DIR_BIN):
	mkdir $(DIR_BIN)

solorun:
	erl -pa $(DIR_BIN) -eval 'application:start(erlmud).'

run: compilar solorun

docs:
	erl -noshell -run edoc_run application "'$(APP_NAME)'" \
	'"."' '[{def,{vsn,"$(VSN)"}}]'

all: compilar docs
