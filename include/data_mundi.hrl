-include("registros.hrl").

-define(
   DATA_MUNDI,
   [ #entidad{
        uuid = ?UUID_LOCALIDAD_MUNDO
      , tipo = ?LUGAR
      , nombre = "explanada"
      , children = [2,5,11,3,13]
      , parent = nil
      , estado = ?EXTERIOR
       }
   , #entidad{
        uuid = 1
      , tipo = ?PERSONAJE
      , nombre = "heroina"
      , children = []
      , parent = ?UUID_LOCALIDAD_MUNDO
       }
   , #entidad{
        uuid = 2
      , tipo = ?OBJETO_LUGAR
      , nombre = "monolito"
      , parent = ?UUID_LOCALIDAD_MUNDO
      , children = [10]
      , estado = ?VISIBLE
       }
  , #entidad{
       uuid = 3
     , tipo = ?OBJETO
     , nombre = "femur"
     , sinonimos = ["hueso"]
     , estado = ?VISIBLE
     , parent = ?UUID_LOCALIDAD_MUNDO
      }
   , #entidad{
        uuid = 4
      , tipo = ?PERSONAJE
      , nombre = "manekeno"
      , children = [14,15,16]
      , parent = ?UUID_LOCALIDAD_MUNDO
       }
   , #entidad{
        uuid = 5
      , tipo = ?OBJETO_LUGAR
      , nombre = "templo"
      , children = [6,12]
      , parent = ?UUID_LOCALIDAD_MUNDO
       }
   , #entidad{
        uuid = 6
      , tipo = ?OBJETO_CONTENEDOR
      , nombre = "armario"
      , sinonimos = ["botiquin", "armarito"]
      , children = [7]
      , parent = 5
       }
   , #entidad{
        uuid = 7
      , tipo = ?OBJETO
      , nombre = "llave"
      , parent = 6
       }
   , #entidad{
        uuid = 8
      , tipo = ?PERSONAJE
      , nombre = "olegario"
      , children = []
      , parent = 5
       }
   , #entidad{
        uuid = 9
      , tipo = ?PERSONAJE
      , nombre = "silvana"
      , children = []
      , parent = 2
       }
   , #entidad{
        uuid = 10
      , tipo = ?OBJETO_TABLA
      , nombre = "sitial"
      , sinonimos = ["asiento","sillon"]
      , children = []
      , parent = 2
       }
   , #entidad{
        uuid = 11
      , tipo = ?OBJETO_RELOJ
      , nombre = "reloj"
      , sinonimos = ["carrillon"]
      , children = []
      , parent = ?UUID_LOCALIDAD_MUNDO
       }
   , #entidad{
        uuid = 12
      , tipo = ?OBJETO_CALENDARIO
      , nombre = "calendario"
      , sinonimos = ["almanaque"]
      , children = []
      , parent = 5
       }
   , #entidad{
        uuid = 13
      , tipo = ?OBJETO_LUZ
      , nombre = "fanal"
      , sinonimos = ["farol", "candil"]
      , children = []
      , parent = ?UUID_LOCALIDAD_MUNDO
       }
   , #entidad{
        uuid = 14
      , tipo = ?OBJETO_VESTIBLE
      , nombre = "tunica"
      , sinonimos = ["camisa"]
      , children = []
      , parent = 4
       }
   , #entidad{
        uuid = 15
      , tipo = ?OBJETO_VESTIBLE
      , nombre = "pantalon"
      , sinonimos = ["pantalón","pantalones"]
      , children = []
      , parent = 4
       }
   , #entidad {
        uuid = 16
      , tipo = ?OBJETO_VESTIBLE
      , nombre = "botas"
      , sinonimos = []
      , children = []
      , parent = 4
       }
   ]
).

-define(PERSONAJES,
        [ #pjs{
              uuid = 1
            , nombre = "heroina"
            , nombre_propio = "Heroína"
            , gg = ?FEMENINO
            , altura = 154
            , peso = 35
            , raza = [{?DUENDE,52},{?HOMBRE,48}]
            , desc = "Encargada de evitar la catástrofe."
            , estado = ?DE_PIE
            , en_juego = ?LIBRE
            }
        , #pjs{
              uuid = 4
            , nombre = "manekeno"
            , nombre_propio = "Manekeno"
            , gg = ?MASCULINO
            , altura = 164
            , peso = 75
            , raza = [{?INMORTAL,100},{?HOMBRE,62},{?DUENDE,38}]
            , desc = "El «amo del calabozo» en persona."
            , estado = ?DE_PIE
            , en_juego = ?LIBRE
            }
        , #pjs{
              uuid = 8
            , nombre = "olegario"
            , nombre_propio = "Olegario"
            , gg = ?MASCULINO
            , altura = 172
            , peso = 60
            , raza = [{?HOMBRE,68},{?ORCO,32}]
            , desc = "Otro personaje de pega para hacer pruebas con él."
            , estado = ?DE_PIE
            , en_juego = ?LIBRE
            }
        , #pjs{
              uuid = 9
            , nombre = "silvana"
            , nombre_propio = "Silvana"
            , gg = ?FEMENINO
            , altura = 156
            , peso = 58
            , raza = [{?INMORTAL,100},{?ELFO,58},{?HOMBRE,42}]
            , desc = "Otro personaje femenino para hacer pruebas con él."
            , estado = ?DE_PIE
            , en_juego = ?LIBRE
            }
        ]
).

-define(OBJETOS,
        [  #obj{
              uuid = 2
            , nombre = "monolito"
            , nombre_propio = "Monolito"
            , gg = ?MASCULINO
            , tipo = ?OBJETO_LUGAR
            , movil = "No puedes levantar el monolito."
            , peso = 30000
            , volumen = 250000
            , desc =
                  [ {fuera,
                     "Negro y mate. De dimensiones precisas y cortado sin defecto
                      alguno, tiene tres metros de alto, dos metros de largo y un metro
                      de ancho. Sin embargo, parece que se puede entrar en él."}
                  , {dentro,
                     "Oscuro y estrecho, tienes que moverte casi de lado para no rozar
                      las paredes laterales, pero te hace sentirte en el centro del
                      Universo."}
                  ]
            , enlaces = [
                         {0, ["explanada", "salir", "sur"], ?VISIBLE, "Pasas por una angosta salida", 1}
                        ]
            }
        ,  #obj{
              uuid = 3
            , nombre = "femur"
            , nombre_propio = "fémur"
            , gg = ?MASCULINO
            , tipo = ?OBJETO
            , movil = ok
            , peso = 0.25
            , volumen = 120
            , desc = "Un hueso largo, amarillo y duro."
            , enlaces = []
            }
        ,  #obj{
              uuid = 5
            , nombre = "templo"
            , nombre_propio = "Templo de la Diosa Madre Era"
            , gg = ?MASCULINO
            , tipo = ?OBJETO_LUGAR
            , movil = "No puedes levantar el templo."
            , peso = 3000
            , volumen = 2500000
            , desc =
                  [ {fuera,
                     "Un templo donde se realizan los más secretos rituales de este
                      mundo. El templo de la Diosa Madre es muy sencillo parece una
                      caseta de piedra y madera."}
                  , {dentro,
                     "No hay nada que reseñar en la estancia, salvo mucho polvo y suciedad.
                      Apenas queda algún vestigio de haber sido usada alguna vez."}
                  ]
            , enlaces = [
                         {0, ["explanada", "salir", "este"], ?VISIBLE, "Sales pasando bajo el arco de las cuatro lunas.", 2}
                        ]
            }
        ,  #obj{
              uuid = 6
            , nombre = "armario"
            , nombre_propio = "armario"
            , gg = ?MASCULINO
            , movil = "El armario está fijado a la pared."
            , tipo = ?OBJETO_CONTENEDOR
            , peso = 1
            , volumen = 250
            , desc =
                  "Un armarito de pared del tamaño de un buzón. En su puerta metálica
                   puedes adivinar un relieve de la Diosa Madre elevando a sus cuatro
                   hijas convertidas en lunas hacia el cielo.

                   Tiene un orificio extraño a modo de cerradura y al lado un bajorrelieve
                   en forma de hueso señala hacia él."
            , estado = ?LLAVE_CERRADO
            , enlaces = [{llave,3}] % El fémur es la llave del armario
            }
        ,  #obj{
              uuid = 7
            , nombre = "llave"
            , nombre_propio = "llave"
            , gg = ?FEMENINO
            , movil = ok
            , tipo = ?OBJETO
            , peso = 0.10
            , volumen = 1
            , desc = "Una llave de seguridad marcada con la etiqueta «MNT-237»"
            , enlaces = []
            }
        , #obj{
             uuid = 10
           , nombre = "sitial"
           , nombre_propio = "sitial"
           , gg = ?MASCULINO
           , movil = "El sitial está tallado en la piedra."
           , tipo = ?OBJETO_TABLA
           , peso = 3000
           , volumen = 10000
           , desc = "Tallado en la piedra tras dos escalones un sitial te permite
                     sentarte en medio de interior del Monolito."
           , enlaces = []
           }
        , #obj{
             uuid = 11
           , nombre = "reloj"
           , nombre_propio = "reloj de pared"
           , gg = ?MASCULINO
           , movil = "El reloj está fijado al suelo."
           , tipo = ?OBJETO_RELOJ
           , peso = 125
           , volumen = 1000
           , desc = "Un reloj de pared un tanto barroco."
           , estado = {adelanta, 0, 1}
           , enlaces = []
           }
        , #obj{
             uuid = 12
           , nombre = "calendario"
           , nombre_propio = "calendario"
           , gg = ?MASCULINO
           , movil = "El calendario está fijado en la pared."
           , tipo = ?OBJETO_CALENDARIO
           , peso = 1
           , volumen = 1
           , desc = "Un calendario de publicidad del taller mecánico de la esquina."
           , enlaces = []
           }
         , #obj{
             uuid = 13
           , nombre = "fanal"
           , nombre_propio = "fanal"
           , gg = ?MASCULINO
           , movil = ok
           , tipo = ?OBJETO_LUZ
           , peso = 1
           , volumen = 1
           , desc = "Un candil protegido del viento por unas pantallas de cristal."
           , estado = ?OFF
           , enlaces = []
           }
         , #obj{
             uuid = 14
           , nombre = "tunica"
           , nombre_propio = "tunica"
           , gg = ?FEMENINO
           , movil = ok
           , tipo = ?OBJETO_VESTIBLE
           , peso = 0
           , volumen = 0
           , desc = [
                      {?VESTIDO,
                       "Una holgada túnica corta de pelo de unicornio blanco que
                        cubre el torso y los brazos."}
                    , {?NO_VESTIDO,
                       "Una túnica corta de pelo de unicornio blanco."}
                    ]
           , estado = ?VESTIDO
           , enlaces = []
           }
         , #obj{
              uuid = 15
            , nombre = "pantalon"
            , nombre_propio = "pantalón"
            , gg = ?MASCULINO
            , movil = ok
            , tipo = ?OBJETO_VESTIBLE
            , peso = 0
            , volumen = 0
            , desc = [
                       {?VESTIDO,
                        "Un pantalón desgastado de cuero negro."}
                     , {?NO_VESTIDO,
                        "Un pantalón de cuero desgastado, cuidadosamente doblado."}
                     ]
            , estado = ?VESTIDO
            , enlaces = []
           }
         , #obj {
              uuid = 16
            , nombre = "botas"
            , nombre_propio = "botas"
            , gg = ?FEMENINO
            , numero = 2
            , movil = ok
            , tipo = ?OBJETO_VESTIBLE
            , peso = 0
            , volumen = 0
            , desc = [
                       {?VESTIDO,
                        "Unas botas de media caña de flexible cuero negro calzan los pies."}
                     , {?NO_VESTIDO,
                        "Unas botas de media caña de flexible cuero negro."}
                     ]
            , estado = ?VESTIDO
            , enlaces = []
           }
        ]
).

-define(LUGARES,
        [ #lugar{
              uuid = ?UUID_LOCALIDAD_MUNDO
            , nombre = "explanada"
            , nombre_propio = "Explanada"
            , coordenadas = {0,0,0}
            , gg = ?FEMENINO
            , desc =
                 "Todo parece flotar en la quietud de la nada del Universo, sin
                  embargo, todo está en movimiento: si se parara todo, caería
                  hacia un solo punto donde explotaría y la rueda comenzaría de
                  nuevo a rodar. ¿Se parará alguna vez?

                  Quizá necesitas un poco de aventura en este universo loco."
            , estado = ?EXTERIOR
            , enlaces = [
                         {2, ["monolito", "entrar", "norte"], ?OCULTO_NOCHE, "Atraviesas una angosta entrada en forma de ese.", 1}
                       , {5, ["templo", "oeste"], ?VISIBLE, "Atraviesas un pórtico adornado con la alegoría de la conjunción de los cinco mundos.", 2}
                        ]
            }
        ]
).

-define(CIERRES,
        [ #cierre{
              uuid = 1
            , estado = ?LLAVE_CERRADO
            , llave = 7
            , clave = ""
            }
         , #cierre{
              uuid = 2
            , estado = ?CERRADO
            , llave = nil
            , clave = ""
            }
        ]
).
