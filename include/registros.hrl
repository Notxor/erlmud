-define(DEFAULT_PORT, 6969).

-define(UUID_LOCALIDAD_MUNDO, 0).

%% Estados de los personajes durante el juego
-define(DE_PIE, 0).
-define(SENTADO, 1).
-define(TUMBADO, 2).
-define(DORMIDO, 3).
-define(INCONSCIENTE, 4).

%% Estados de los pj disponibles
-define(LIBRE, 0).
-define(OCUPADO, 1).

%% Estados de los objetos vestibles
-define(NO_VESTIDO, 0).               % No se está vistiendo la prenda.
-define(VESTIDO, 1).                  % La prenda está siendo vestida.

%% Estados de los los cierres entre localidades
-define(ABIERTO, 0).
-define(CERRADO, 1).
-define(LLAVE_ABIERTO, 2).
-define(LLAVE_CERRADO, 3).
-define(FORZADO, 4).

%% Tipos de entidades
-define(LUGAR, 0).
-define(PERSONAJE, 1).
-define(OBJETO, 2).
-define(OBJETO_CONTENEDOR, 3).
-define(OBJETO_LUGAR, 4).
-define(OBJETO_TABLA, 5).
-define(OBJETO_RELOJ, 6).
-define(OBJETO_CALENDARIO, 7).
-define(OBJETO_LUZ, 8).
-define(OBJETO_VESTIBLE, 9).

%% Estado de luces y otros dispositivos binarios
-define(OFF, 0).
-define(ON, 1).

%% Estados de las entidades
-define(OCULTO, 0).
-define(VISIBLE, 1).
-define(OCULTO_NOCHE, 2).

%% Tipo de lugar para descripciones iluminado
-define(INTERIOR,3).   % Sin vistas al exterior
-define(EXTERIOR,4).   % Al aire libre
-define(TECHADO,5).    % Con vistas al exterior

%% Género gramatical
-define(MASCULINO, 0).
-define(FEMENINO, 1).
-define(NEUTRO, 2).

-define(GENERO,
        #{
           ?MASCULINO => "masculino"
         , ?FEMENINO => "femenino"
         }).

%% Valores para razas
-define(ELFO, 1).
-define(ORCO, 2).
-define(HOMBRE, 3).
-define(DUENDE, 4).
-define(NUM_RAZAS_PJ, 4).  % Número de razas asignable a los Personajes Jugadores
-define(INMORTAL, 1001).

%% Características de los Personajes
-define(FUE, 0).      % Fuerza
-define(AGI, 1).      % Agilidad
-define(INT, 2).      % Inteligencia
-define(CON, 3).      % Constitución
-define(NUM, 4).      % Aptitud numérica y cálculo
-define(MEC, 5).      % Aptitud mecánica
-define(ESP, 6).      % Aptitud espacial
-define(SOC, 7).      % Aptitudes sociales y relacionales
-define(COO, 8).      % Coordinación visomotora

-define(RAZAS,      % Tabla de valores de las razas
        #{
           ?ELFO => "elfo"
         , ?ORCO => "orco"
         , ?HOMBRE => "hombre"
         , ?DUENDE => "duende"
         , ?INMORTAL => "inmortal"
         }).

-define(RAZA_VALUE, % Devolver valor numérico dada la cadena.
        #{
           "elfo" => ?ELFO
         , "orco" => ?ORCO
         , "hombre" => ?HOMBRE
         , "duende" => ?DUENDE
         , "inmortal" => ?INMORTAL
         }).

-define(TABLA_RAZA, % Establece los valores min-máx para la generación de personajes
        #{   %     Ed.m, Ed.M,   C1,   C2, Alt.m, Alt.M, P.m,  P.M
           "EM" => {30,   180, ?CON, ?INT,   155,   170,  50,  80}
         , "EF" => {30,   180, ?CON, ?INT,   155,   170,  45,  75}
         , "OM" => {25,    50, ?CON, ?AGI,   155,   170,  60,  85}
         , "OF" => {25,    50, ?CON, ?AGI,   155,   170,  55,  80}
         , "HM" => {16,    25, ?FUE, ?INT,   160,   195,  50, 200}
         , "HF" => {16,    25, ?FUE, ?INT,   145,   190,  35,  85}
         , "DM" => {16,    25, ?AGI, ?INT,   100,   155,  25,  65}
         , "DF" => {16,    25, ?AGI, ?INT,    90,   145,  20,  55}
         }).

%% Valores para profesiones básicas
-define(GUERRERO, 1).
-define(ARTESANO, 2).
-define(AVENTURERO, 3).
-define(ESTUDIOSO, 4).
-define(VIAJERO, 5).
-define(MAGO, 6).

-define(PROFESIONES,   % Cambiar el valor numérico por una cadena
        #{
           ?GUERRERO => "guerrero"
         , ?ARTESANO => "artesano"
         , ?AVENTURERO => "aventurero"
         , ?ESTUDIOSO => "estudioso"
         , ?VIAJERO => "viajero"
         , ?MAGO => "mago"
         }).

-define(PROFESION_VALOR, % Devolver el valor numérico dada la cadena.
        #{
           "guerrero" => ?GUERRERO
         , "artesano" => ?ARTESANO
         , "aventurero" => ?AVENTURERO
         , "estudioso" => ?ESTUDIOSO
         , "viajero" => ?VIAJERO
         , "mago" => ?MAGO
         }).

-define(TABLA_PROFESIONES, % Establece los valores para la generación de personajes
        #{  %              Prin. Secun.
           ?GUERRERO   => {?AGI, ?FUE}
         , ?ARTESANO   => {?COO, ?NUM}
         , ?AVENTURERO => {?CON, ?INT}
         , ?ESTUDIOSO  => {?INT, ?NUM}
         , ?VIAJERO    => {?ESP, ?AGI}
         , ?MAGO       => {?INT, ?CON}
         }).

%% Registro para la lista de clientes.
-record(socket, { uuid = nil         % Identificador único del PJ
                , socket             % Socket de comunicación
                , pid                % Pid del proceso cliente
                , nombre = ""        % Nombre del jugador
                }).

%% Registro para construir el árbol del mundo
-record(entidad, {
                   uuid              % Identificador único
                 , tipo              % Tipo de entidad
                 , nombre            % Nombre de la entidad
                 , sinonimos = []    % Lista de nombres por los que puede ser llamada
                 , estado = ?VISIBLE % Estado de una entidad
                 , children = []     % Lista con los uuids «children»
                 , parent            % uuid de la entidad «parent»
                 }).

%% Registro de personajes jugadores
-record(pjs,     {
                   uuid   = nil      % Identificador único
                 , nombre = ""       % nombre de uso del jugador
                 , nombre_propio     % nombre propio del jugador
                 , gg                % Género gramatical
                 , altura            % Estatura del personaje en cms
                 , peso              % Peso del personaje en kg
                 , raza = []         % Raza del personaje
                 , nacimiento = {}   % Tupla {año,mes,día}
                 , profesion         % profesion del personaje
                 , pvR = 100         % Puntos de vida como Rasgo (son los pv máximos del PJ).
                 , pvE = 100         % Puntos de Vida como Estado
                 , emR = 0           % Energía mágica como Rasgo (la em máxima del PJ).
                 , emE = 0           % Energía mágica como Estado
                 , exp = 150         % Puntos de Experiencia
                 , energia = 100     % Simulación de metabolismo (funciona en porcentaje).
                 , fue = 50          % Característica de FUErza
                 , agi = 50          % Característica de AGIlidad del personaje
                 , int = 50          % Característica de INTeligencia del personaje (es una media de las aptitudes)
                 , con = 50          % Característica de CONstitución
                 , num = 50          % Aptitud numérica
                 , mec = 50          % Aptitud mecánica
                 , esp = 50          % Aptitud espacial
                 , soc = 50          % Aptitud social
                 , coo = 50          % Coordinación visomotora
                 , desc              % Descripción del jugador
                 , estado            % Estado del personaje jugador
                 , miembros = []     % Una cabeza, un cuerpo, dos brazos y dos piernas
                 , en_juego = ?LIBRE % Marca si un jugador está ocupado o libre
                 , formato = true    % Muestra los textos con colores o sin ellos
                 }).

%% Registro de lugares del mundo
-record(lugar,   {
                   uuid              % Identificador único
                 , nombre            % Nombre del lugar
                 , nombre_propio     % Nombre principal del objeto
                 , coordenadas       % Coordenadas {X,Y,Z} del lugar.
                 , gg                % Género gramatical
                 , desc              % Descripción del lugar
                 , estado            % Estado del lugar (abierto, cerrado)
                 , enlaces = []      % Lista de salidas del lugar
                 }).

%% Registro de objetos del mundo
-record(obj,     {
                   uuid              % Identificador único
                 , nombre            % Nombre del objeto
                 , nombre_propio     % Nombre principal del objeto
                 , gg                % Género gramatical
                 , numero = 1        % Expresa el número gramatical, >1 para utilizar el plural
                 , movil             % ok si se puede mover, si no, frase con mensaje
                 , tipo              % Tipo de objeto
                 , peso = 1          % Peso del objeto en kg
                 , volumen = 250     % Volumen que ocupa el objeto en cm3
                 , pv = 100          % Puntos de vida del objeto
                 , parada = 0        % Puntos de protección (evita perder PV del personajes)
                 , alcance_min = 0   % Distancia mínima a la que afecta si es un arma
                 , alcance_max = 0   % Distancia máxima a la que afecta si es un arma
                 , alcance_opt = 0   % Distancia óptima del arma
                 , pd = 0            % Puntos de daño que produce el arma
                 , desc              % Descripción del objeto
                 , estado            % Estado del objeto
                 , enlaces = []      % Relaciones de un objeto con otro, por ejemplo una llave
                 }).

%% Registro de cerraduras
-record(cierre,  {
                   uuid              % Identificador del cierre, cerradura o cancela
                 , estado            % Cerrado, Abierto, Roto
                 , llave             % UUID del objeto que lo abre y cierra
                 , clave = ""        % Cadena que representa una clave de apertura
                 }).
